﻿using EVP;
using UnityEngine;
using UnityEditor;

namespace CarsBattleRoyal
{
    [CustomEditor(typeof(InputSettings))]
    public class InputSettingsInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            InspectorTools.BeginContent();
            serializedObject.Update();
            
            InspectorTools.SetMinLabelWidth(210);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("continuousForwardAndReverse"));
            InspectorTools.SetMinLabelWidth(160);

            SerializedProperty propThrottleAndBrakeInput = serializedObject.FindProperty("throttleAndBrakeInput");
            EditorGUILayout.PropertyField(propThrottleAndBrakeInput);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("steerAxis"));

            ThrottleAndBrakeInput throttleAndBrakeInput =
                (ThrottleAndBrakeInput) propThrottleAndBrakeInput.enumValueIndex;

            if (throttleAndBrakeInput == ThrottleAndBrakeInput.SeparateAxes)
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("throttleAxis"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("brakeAxis"));
            }
            else
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("throttleAndBrakeAxis"));
            }

            EditorGUILayout.PropertyField(serializedObject.FindProperty("handbrakeAxis"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("resetVehicleKey"));

            serializedObject.ApplyModifiedProperties();
            InspectorTools.EndContent();
        }
    }
}