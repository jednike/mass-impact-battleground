using System;
using UnityEngine;

namespace CarsBattleRoyal
{
    public enum ThrottleAndBrakeInput
    {
        SingleAxis,
        SeparateAxes
    };
    [CreateAssetMenu(menuName = "Mass Impact/Input Settings", fileName = "InputSettings")]
    public class InputSettings: ScriptableObject
    {
        public bool continuousForwardAndReverse = true;

        public ThrottleAndBrakeInput throttleAndBrakeInput = ThrottleAndBrakeInput.SingleAxis;

        public string steerAxis = "Horizontal";
        public string throttleAndBrakeAxis = "Vertical";
        public string throttleAxis = "Fire2";
        public string brakeAxis = "Fire3";
        public string handbrakeAxis = "Jump";
    }
}