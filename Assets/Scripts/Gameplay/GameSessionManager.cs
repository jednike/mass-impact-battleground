﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class GameSessionManager : MonoBehaviour
    {
        private GameSession _gameSession;
        
        private void OnEnable()
        {
            GameControllers.GameEndEvent += OnGameEndEvent;
        }

        private void OnGameEndEvent(int position, int kills, int earnedMoney)
        {
            StartCoroutine(EndSession(kills));
        }

        private void OnDisable()
        {
            GameControllers.GameEndEvent -= OnGameEndEvent;
        }

        private void Start()
        {
            StartSession();
        }

        private void StartSession()
        {
            if(string.IsNullOrEmpty(Scores.Instance.UniqueId))
                return;
			
            var form = new WWWForm();
            form.AddField("UniqueId", Scores.Instance.UniqueId);
			
            StartCoroutine(NetworkRequests.SendRequest("start_session", form, (success, response) =>
            {
                if (!success)
                {
                    Debug.LogError(response);
                    return;
                }

                _gameSession = JsonUtility.FromJson<GameSession>(response);
                if (!_gameSession.Success)
                    Debug.LogError(_gameSession.Message);
            }));
        }
        
        private IEnumerator EndSession(int kills)
        {
            if(_gameSession == null || string.IsNullOrEmpty(Scores.Instance.UniqueId) || !_gameSession.Success)
                yield break;
			
            var form = new WWWForm();
            form.AddField("UniqueId", Scores.Instance.UniqueId);
            form.AddField("SessionId", _gameSession.SessionId);
            form.AddField("Kills", kills);
			
            StartCoroutine(NetworkRequests.SendRequest("end_session", form, (success, response) =>
            {
                if (!success)
                {
                    Debug.LogError(response);
                    return;
                }

                var message = JsonUtility.FromJson<DefaultMessage>(response);
                if (message.Success) return;
                Debug.LogError(message.Message);
            }));
        }
    }
}