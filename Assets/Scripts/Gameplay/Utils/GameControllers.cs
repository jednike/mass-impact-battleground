﻿using System;
using UnityEngine;

namespace CarsBattleRoyal
{
	public enum GameState
	{
		None,
		MainMenu,
		ConnectingToServer,
		ConnectingToRoom,
		WaitingPlayers,
		WaitingStartGame,
		BeforeStart,
		ConnectError,
		GamePlay,
		LoadGame,
		LoadMenu,
		PreDesant,
		Desant,
		Pause,
		Settings,
		EndGame,
		Garage,
		Safe,
		Leaderboard,
	}
	
	public class GameControllers : GlobalSingletonBehaviour<GameControllers>
	{
		public static Action<int, int, int> GameEndEvent = delegate {  };
		public static event Action<GameState, GameState> OnGameStateChanged = delegate { };

		public static GameState LastState;
		public static GameState GameState
		{
			get { return _gameState; }
			set
			{
				if (_gameState == value)
				{
					return;
				}

				LastState = _gameState;
				_gameState = value;
				OnGameStateChanged(LastState, _gameState);
			}
		}
		private static GameState _gameState;
	}
}
