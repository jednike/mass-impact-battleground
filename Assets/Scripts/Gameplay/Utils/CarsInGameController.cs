﻿using System;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CarsBattleRoyal
{
	public partial class CarsInGameController : LocalSingletonBehaviour<CarsInGameController>
	{
		private static readonly Dictionary<int, CarController> Players = new Dictionary<int, CarController>();
		
		public static void AddPlayerToPlayersList(CarController player)
		{
			if (Players.ContainsKey(player.OwnerId))
			{
				Debug.Log("This player already exist");
				return;
			}
			Players.Add(player.OwnerId, player);
		}
		public static void RemovePlayerFromPlayersList(CarController player)
		{
			if (!Players.ContainsKey(player.OwnerId))
			{
				Debug.Log("This player does not exist");
				return;
			}
			Players.Remove(player.OwnerId);
		}
		
		private void Update()
		{
			if (!PhotonNetwork.IsMasterClient)
				return;

			var needBots = AIBehaviour.MinActiveBots - AIBehaviour.ActiveBots;
			if (needBots <= 0) return;
			var closestCars = Players.Values.Where(go => go.Active == ActiveState.NotActive && go.CurrentCarType == ActiveCarType.LocalAi).ToList();
			foreach (var car in closestCars)
			{
				if (needBots <= 0)
					return;

				needBots--;
				car.Active = ActiveState.AlwaysActive;
			}
		}

		public static Transform[] FindAllClosest(CarController localCar, float maxFindDistance, Vector3 position)
		{
			return FindAllClosest(localCar, maxFindDistance, position, new []{ActiveCarType.LocalAi, ActiveCarType.LocalPlayer, ActiveCarType.NetworkAi, ActiveCarType.NetworkPlayer});
		}
		private static Transform[] FindAllClosest(CarController localCar, float maxFindDistance, Vector3 position, ActiveCarType[] needTypes)
		{			
			var target = new List<Transform>();
			foreach (var go in Players)
			{
				if (go.Value == localCar || go.Value.TeamId == localCar.TeamId || go.Value.Destroyed || !needTypes.Contains(go.Value.CurrentCarType))
					continue;
				
				var curDistance = Vector3.Distance(go.Value.transform.position, position);
				if (curDistance > maxFindDistance) continue;
				target.Add(go.Value.transform);
			}
			return target.ToArray();
		}
		
		public static Transform FindClosest(CarController localCar, float maxFindDistance, Vector3 position)
		{
			return FindClosest(localCar, maxFindDistance, position, new []{ActiveCarType.LocalAi, ActiveCarType.LocalPlayer, ActiveCarType.NetworkAi, ActiveCarType.NetworkPlayer});
		}
		public static Transform FindClosest(CarController localCar, float maxFindDistance, Vector3 position, ActiveCarType[] needTypes)
		{
			Transform target = null;
			var distance = Mathf.Infinity;
			
			var allClosest = FindAllClosest(localCar, maxFindDistance, position, needTypes);
			foreach (var closest in allClosest)
			{
				var curDistance = Vector3.Distance(closest.position, position);
				if (curDistance >= distance || curDistance > maxFindDistance) continue;
				
				target = closest.transform;
				distance = curDistance;
			}

			return target;
		}
		public static Transform FindClosest(CarController localCar, float maxFindDistance, Vector3 position, float angle)
		{
			Transform target = null;
			var distance = Mathf.Infinity;
			
			var allClosest = FindAllClosest(localCar, maxFindDistance, position, new []{ActiveCarType.LocalAi, ActiveCarType.NetworkAi, ActiveCarType.NetworkPlayer});

			var localCarTransform = localCar.transform;
			var localCarPosition = localCarTransform.position;
			var forward = localCarTransform.forward;
			
			var carPos = new Vector2(localCarPosition.x, localCarPosition.z);
			var carForward = new Vector2(forward.x, forward.z);

			foreach (var closest in allClosest)
			{
				var closestPosition = closest.position;
				var curDistance = Vector3.Distance(closestPosition, position);
				if (curDistance >= distance) continue;

				var closestPos = new Vector2(closestPosition.x, closestPosition.z);
								
				if (Vector2.Angle(closestPos - carPos, carForward) >= angle)
					continue;
				
				target = closest.transform;
				distance = curDistance;
			}

			return target;
		}
		
		public static Transform FindClosestWeapon(Vector3 position, float maxFindDistance)
		{
			var distance = Mathf.Infinity;
			
			Transform target = null;
			var weapons = GameObject.FindGameObjectsWithTag("Weapon");

			foreach (var go in weapons)
			{
				var curDistance = Vector3.Distance(go.transform.position, position);
				if (curDistance >= distance || curDistance > maxFindDistance) continue;
				target = go.transform;
				distance = curDistance;
			}

			return target;
		}

		public static bool GetCarControllerById(int ownerId, out CarController carController)
		{
			carController = null;
			if (!Players.ContainsKey(ownerId))
				return false;
			carController = Players[ownerId];
			return true;
		}
		
		public static IEnumerable<CarController> GetCarsInMyZone(Point myZone)
		{
			var carList = new List<CarController>();
			foreach (var car in Players.Values)
			{
				var carZone = car.GetComponent<CarZoneManager>().Zone;
				if (carZone.x >= myZone.x - GameConstants.ActiveDistance && carZone.x <= myZone.x + GameConstants.ActiveDistance &&
				    carZone.y >= myZone.y - GameConstants.ActiveDistance && carZone.y <= myZone.y + GameConstants.ActiveDistance)
				{
					carList.Add(car);
				}
			}
			return carList.ToArray();
		}

		public static int GetRealAliveCount()
		{
			return Players.Values.Count(car => !car.Destroyed);
		}
		
		public override void DoDestroy()
		{
			Players.Clear();
		}
	}
	//For team games
	public partial class CarsInGameController
	{
		public static CarController[] GetCarsWithTeamId(int teamId)
		{
			return Players.Values.Where(car => car.TeamId == teamId).ToArray();
		}
		public static Dictionary<int, List<CarController>> GetAllCarsWithTeamId()
		{
			var allCars = new Dictionary<int, List<CarController>>();
			foreach (var car in Players.Values)
			{
				if(!allCars.ContainsKey(car.TeamId))
					allCars.Add(car.TeamId, new List<CarController>{car});
				else
					allCars[car.TeamId].Add(car);
			}

			return allCars;
		}
	}
}