﻿using System;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	[Serializable]
	public struct LanguageFont
	{
		public SystemLanguage Language;
		public Font Font;
	}
	public class MultiLanguageManager : MonoBehaviour {
		private static readonly Dictionary<string, string> Dictionary = new Dictionary<string, string>();
		private static readonly List<string> NeedKeys = new List<string>();
		private static string _needLanguages;
		
		[SerializeField] private LanguageFont[] _languageFonts;
		public static Font CurrentLanguageFont;

		private static MultiLanguageManager Instance
		{
			get
			{
				if (!_instance)
					Instantiate(Resources.Load<GameObject>("LanguageManager"));

				return _instance;
			}
		}
		private static MultiLanguageManager _instance;

		private void Awake()
		{
			if (_instance)
			{
				Destroy(gameObject);
				return;
			}
			_instance = this;
			DontDestroyOnLoad(gameObject);
			
			var language = Scores.Instance.CurrentLanguage;
			if (string.IsNullOrEmpty(language))
			{
				switch (Application.systemLanguage)
				{
					case SystemLanguage.Russian:
						language = "Russian";
						break;
					default:
						language = "English";
						break;
				}

				Scores.Instance.CurrentLanguage = language;
			}
			LoadNewLanguage(language);
		}

		private void OnEnable()
		{
			Scores.OnLanguageChange += OnOnLanguageChange;
		}

		private void OnOnLanguageChange()
		{
			LoadNewLanguage(Scores.Instance.Language);
		}

		public static string GetTranslateString(string stringCode, string currentValue, Text text)
		{
			if (!Instance)
				return currentValue;
			text.font = CurrentLanguageFont;
			
			if (Dictionary.ContainsKey(stringCode)) return Dictionary[stringCode];

			if (!NeedKeys.Contains(stringCode))
			{
				NeedKeys.Add(stringCode);
				_needLanguages += "<string name=\"" + stringCode + "\">" + currentValue + "</string> ";
			}
			return currentValue;

		}
		
		public static void LoadNewLanguage(string language)
		{
			CurrentLanguageFont = Instance._languageFonts[0].Font;
			foreach (var font in Instance._languageFonts)
			{
				if (font.Language == Application.systemLanguage)
					CurrentLanguageFont = font.Font;
			}
			var textAsset = (TextAsset) Resources.Load("Languages/" + language, typeof(TextAsset));
			var currentLanguageXml = new XmlDocument();
			currentLanguageXml.LoadXml(textAsset.text);

			Dictionary.Clear();
			foreach (XmlNode node in currentLanguageXml.GetElementsByTagName("string"))
			{
				if (node.Attributes != null && !Dictionary.ContainsKey(node.Attributes["name"].Value))
				{
					Dictionary.Add(node.Attributes["name"].Value, node.InnerText);
				}
			}
		}

		public void OnDisable()
		{
			Scores.OnLanguageChange -= OnOnLanguageChange;
			Debug.Log(_needLanguages);
		}
	}
}
