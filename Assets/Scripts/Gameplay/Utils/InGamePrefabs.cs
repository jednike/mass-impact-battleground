﻿﻿﻿using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using UnityEngine;
using UnityEngine.Audio;
  using UnityEngine.SceneManagement;
  using UnityEngine.Serialization;
  using Random = UnityEngine.Random;

namespace CarsBattleRoyal
{
	public enum AvailableType
	{
		Random,
		Simple,
		Epic,
		OnlyVIP,
		OnlyBox,
		OnlySeason
	}
	[Serializable]
	public struct SpawnParticles
	{
		public WeaponLevel WeaponLevel;
		public GameObject Particle;
	}
	[Serializable]
	public struct WeaponLevelColors
	{
		public WeaponLevel WeaponLevel;
		public Color Color;
	}
	[Serializable]
	public struct AwardFromLootBox
	{
		public AwardType AwardType;
		public string ResourceName;
		public string CarName;
		public string SkinName;
	}

	public class InGamePrefabs : MonoBehaviour
	{
		public SpawnParticles[] spawnParticles;
		public WeaponLevelColors[] weaponLevelColors;
		public AwardFromLootBox[] loot;
		public GameObject miniMapPrefab;
		public GameObject nicknamePrefab;

		public CarsInfo cars;
		public SoundsInfo sounds;
		public ParticlesInfo particles;

		public static InGamePrefabs Instance
		{
			get
			{
				if (!_instance)
					Instantiate(Resources.Load<GameObject>("InGamePrefabs"));

				return _instance;
			}
		}
		private static InGamePrefabs _instance;
		public InputSettings inputSettings;
		public FlipSettings flipSettings;

		private void Awake()
		{
			if (_instance != null)
			{
				Destroy(gameObject);
				return;
			}

			_instance = this;
			DontDestroyOnLoad(gameObject);

			SceneManager.sceneLoaded += (scene, mode) => { Scores.Save(); };
			SetVolume();
		}

		public static void SetVolume(int value = -1)
		{
			Instance.sounds.musicMixerGroup.audioMixer.SetFloat("MusicVolume", value == -1 ? (Scores.Instance.Music ? 0 : -80) : value);
			Instance.sounds.soundMixerGroup.audioMixer.SetFloat("SoundsVolume", value == -1 ? (Scores.Instance.Sounds ? 0 : -80) : value);
		}

		public GameObject SpawnParticleByLevel(WeaponLevel level, Transform parent)
		{
			var particle =
				(from spawnParticle in spawnParticles where spawnParticle.WeaponLevel == level select spawnParticle.Particle)
				.FirstOrDefault();
			return particle == null ? null : Instantiate(particle, parent);
		}

		public Color GetWeaponLevelColor(WeaponLevel level)
		{
			return (from spawnParticle in weaponLevelColors where spawnParticle.WeaponLevel == level select spawnParticle.Color)
				.FirstOrDefault();
		}

		private void OnApplicationFocus(bool hasFocus)
		{
			if (!hasFocus)
			{
				Scores.Save();
			}
		}
	}
}
