﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public enum MessageTypes
	{
		EnemyDestroy,
		NowYouLeader,
		NowNotLeader
	}
	public class InfoMessages : MonoBehaviour
	{
		private static InfoMessages _instance;
		
		private Text _text;
		private Animator _animation;
		[SerializeField] private float _timeToChange = 2f;
		private float _currentTime = 0f;
		private readonly List<string> _messagesStack = new List<string>();
		
		private void Awake()
		{
			_text = GetComponent<Text>();
			_animation = GetComponent<Animator>();
			_instance = this;
		}

		private void OnDestroy()
		{
			_instance = null;
		}

		private void Update()
		{
			if (_currentTime <= 0)
			{
				if (_messagesStack.Count <= 0) return;
				_currentTime = _timeToChange;
				_text.text = _messagesStack[0];
				_messagesStack.RemoveAt(0);
				_animation.Play("Show");
				return;
			}
			_currentTime -= Time.deltaTime;
		}
		
		public static void ShowMessage(MessageTypes messageTypes, object[] dopArguments)
		{
			if(!_instance)
				return;
			_instance.ShowLocalMessage(messageTypes, dopArguments);
		}
		private void ShowLocalMessage(MessageTypes messageTypes, object[] dopArguments)
		{
			switch (messageTypes)
			{
				case MessageTypes.EnemyDestroy:
					_messagesStack.Add(MultiLanguageManager.GetTranslateString("Enemy Destroyed", "Enemy destroyed!", _text));
					break;
				case MessageTypes.NowYouLeader:
					_messagesStack.Add(MultiLanguageManager.GetTranslateString("Now You Leader", "Now you are the leader!", _text));
					break;
				case MessageTypes.NowNotLeader:
					_messagesStack.Add(MultiLanguageManager.GetTranslateString("Now Not Leader", "Leadership is lost!", _text));
					break;
			}
		}
	}
}
