﻿using System;
using System.Linq;
using CarsBattleRoyal;
using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class MultiLanguageText : MonoBehaviour
	{
		[SerializeField] private string _specialName;

		private void OnEnable()
		{
			OnLanguageChange();
		}

		private void OnLanguageChange()
		{
			var text = GetComponent<Text>();
			if(!text)
				return;
			text.text = MultiLanguageManager.GetTranslateString(name, text.text, text);
			text.font = MultiLanguageManager.CurrentLanguageFont;
		}

		private void Awake()
		{
			Scores.OnLanguageChange += OnLanguageChange;
		}
		private void OnDestroy()
		{
			Scores.OnLanguageChange -= OnLanguageChange;
		}
	}
}
