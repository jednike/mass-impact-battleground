using System;
using ExitGames.Client.Photon;

namespace CarsBattleRoyal
{
    public struct Point: IEquatable<Point>
    {
        public int x;
        public int y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public bool Equals(Point other)
        {
            return x == other.x && y == other.y;
        }
        public override bool Equals(object obj)
        {
            return obj is Point other && Equals(other);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return (x * 397) ^ y;
            }
        }

        public static bool operator ==(Point left, Point right)
        {
            return left.x == right.x && left.y == right.y;
        }
        public static bool operator !=(Point left, Point right)
        {
            return !(left == right);
        }
        
        public static readonly byte[] memPoint = new byte[2 * 4];
        public static short Serialize(StreamBuffer outStream, object customobject)
        {
            var vo = (Point) customobject;
            lock (memPoint)
            {
                var bytes = memPoint;
                var index = 0;
                Protocol.Serialize(vo.x, bytes, ref index);
                Protocol.Serialize(vo.y, bytes, ref index);
                outStream.Write(bytes, 0, 2 * 4);
            }
            return 2 * 4;
        }

        public static object Deserialize(StreamBuffer inStream, short length)
        {
            var vo = new Point();
            lock (memPoint)
            {
                inStream.Read(memPoint, 0, 2 * 4);
                int index = 0;
                Protocol.Deserialize(out vo.x, memPoint, ref index);
                Protocol.Deserialize(out vo.y, memPoint, ref index);
            }

            return vo;
        }
    }
}