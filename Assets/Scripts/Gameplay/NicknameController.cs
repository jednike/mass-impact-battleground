﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class NicknameController : MonoBehaviour
	{
		[SerializeField] private Image bestImage;
		[SerializeField] private Image killImage;
		[SerializeField] private Text nicknameText;

		private CarController _carController;
		private BestPlayerManager _bestPlayerManager;
		private const int MaxDistance = 200;
		private Camera _camera;

		private void Start()
		{
			_camera = Camera.main;
			_carController = GetComponentInParent<CarController>();
			_bestPlayerManager = _carController.GetComponent<BestPlayerManager>();
			nicknameText.text = _carController.NickName;
		}
	
		private void Update ()
		{
			var camTransform = _camera.transform;
			if (Vector3.Distance(camTransform.position, transform.position) > MaxDistance)
			{
				bestImage.enabled = false;
				nicknameText.enabled = false;
				return;
			}
			
			transform.LookAt(camTransform);
			transform.rotation *= Quaternion.Euler(0, 180, 0);
			
			if (_carController.Destroyed)
			{
				killImage.enabled = true;
				bestImage.enabled = false;
				nicknameText.enabled = true;
				enabled = false;
			}
			else
			{
				nicknameText.enabled = true;
				bestImage.enabled = _bestPlayerManager.IsBest;
			}
		}
	}
}
