using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class SearchByTagPointGetter: IPointGetter
    {
        private readonly List<Transform> _spawnPoints;

        public SearchByTagPointGetter()
        {
            _spawnPoints  = new List<Transform>();
        }

        public void Init()
        {
            var objects = GameObject.FindGameObjectsWithTag("SpawnPoint");
            foreach (var ob in objects)
            {
                _spawnPoints.Add(ob.transform);
            }
        }

        public Transform[] GetPoints()
        {
            return _spawnPoints.ToArray();
        }
    }
}