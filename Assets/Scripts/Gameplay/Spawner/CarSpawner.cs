using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class CarSpawner
    {
        private static int _ownerScene = -2;
        private static int _teamId;
		
        public static void SpawnCar(bool bot = false)
        {
            var carName = Scores.Instance.CurrentCar;
            var carSkin = Scores.Instance.CurrentSkin;
			
            if (bot || carName == "Random" || carSkin == "Random")
            {
                InGamePrefabs.Instance.cars.GetRandomCarAndSkin(out carName, out carSkin, AvailableType.Simple);
            }

            _teamId++;
            var spawnPos = GameModeManager.Instance.GetSpawnPosition(out var spawnRot, _teamId);
            var carInfo = new SpawnCarInfo()
            {
                carName = carName,
                carSkin = carSkin,
                isBot = bot,
                teamId = _teamId
            };
            if (!bot)
            {
                carInfo.ownerId = PhotonNetwork.LocalPlayer.ActorNumber;
                carInfo.focusOnTargetMode = Scores.Instance.SelectedTarget;
            }
            else
            {
                carInfo.ownerId = --_ownerScene;
                carInfo.focusOnTargetMode = true;
            }
            PhotonNetwork.Instantiate("Cars/" + carName, spawnPos, spawnRot, 0, (object[]) carInfo);
        }
    }
}