using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    public interface IPointGetter
    {
        void Init();
        Transform[] GetPoints();
    }
}