﻿using System;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace CarsBattleRoyal
{
	[Serializable]
	public struct SpawnObject
	{
		public string objectName;
		public bool active;
		public float chance;
	}
	[Serializable]
	public struct SpawnCarInfo
	{
		public string carName;
		public string carSkin;
		public bool isBot;
		public int ownerId;
		public bool focusOnTargetMode;
		public int teamId;

		public static implicit operator SpawnCarInfo(object[] objects)
		{
			return new SpawnCarInfo()
			{
				carName = (string) objects[0],
				carSkin = (string) objects[1],
				isBot = (bool) objects[2],
				ownerId = (int) objects[3],
				focusOnTargetMode = (bool) objects[4],
				teamId = (int) objects[5],
			};
		}
		
		public static explicit operator object[](SpawnCarInfo spawnCarInfo)
		{
			return new object[]
			{
				spawnCarInfo.carName,
				spawnCarInfo.carSkin,
				spawnCarInfo.isBot,
				spawnCarInfo.ownerId,
				spawnCarInfo.focusOnTargetMode,
				spawnCarInfo.teamId
			};
		}
	}
	public class SpawnElementsController : MonoBehaviour
	{
		[SerializeField] private SpawnObject[] weapons;
		[SerializeField] private SpawnObject[] objects;
		[SerializeField] private float weaponLuck = 0.8f;

		private IPointGetter _pointGetter;

		public void Awake()
		{
			_pointGetter = new SearchByTagPointGetter();
			_pointGetter.Init();
		}

		private void Start()
		{
			if(!PhotonNetwork.IsMasterClient)
				return;

			var points = _pointGetter.GetPoints();
			for (var i = 0; i < points.Length; i++)
			{
				var type = GetSpawnObjectType();
				var spawnObjectName = type == ObjectType.Object ? "Objects/" + GetRandomObject(objects): "Weapons/" + GetRandomObject(weapons);
				var spawnObjectLevel = GetWeaponLevel();
				PhotonNetwork.InstantiateSceneObject(spawnObjectName, points[i].position, points[i].rotation,
					0, new object[] {spawnObjectLevel, i});
			}
		}

		private static string GetRandomObject(SpawnObject[] objectArray)
		{
			var random = Random.Range(0f, 1f);
			var available = objectArray.Where(weapon => weapon.active && weapon.chance >= random).ToList();
			return available[Random.Range(0, available.Count)].objectName;
		}
		private ObjectType GetSpawnObjectType()
		{
			var range = Random.Range(0f, 1f);
			return range > weaponLuck ? ObjectType.Object : ObjectType.Weapon;
		}
		private static WeaponLevel GetWeaponLevel()
		{
			var range = Random.Range(0f, 1f);
			if (range > 0.9f)
				return WeaponLevel.Gold;
			if (range > 0.7f)
				return WeaponLevel.Purple;
			if (range > 0.4f)
				return WeaponLevel.Blue;
			
			return WeaponLevel.Green;
		}
	}
}
