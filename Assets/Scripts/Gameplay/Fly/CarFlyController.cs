﻿﻿using System;
 using System.Collections;
 using EVP;
using UnityEngine;
 using Random = UnityEngine.Random;

namespace CarsBattleRoyal
{
	public class CarFlyController : MonoBehaviour
	{
		[SerializeField] private Transform _leftBlade;
		[SerializeField] private Transform _rightBlade;
		
		private Rigidbody _rigidbody;
		private Transform _carTransform;
		
		private const float RotationSpeed = 100.0f;

		private VehicleController _vehicleController;
		private CarController _carController;
		private CarLandingManager _landingManager;
		private FlipOver _flipOver;

		private float _currentSteer;
		private float _currentBrake;
		private float _needTimeStep = 2f;
		private float _lifeTimeStep = 2f;
		private float _lifeTime;

		private void Start()
		{
			_carController = GetComponentInParent<CarController>();
			_landingManager = GetComponentInParent<CarLandingManager>();
			_vehicleController = GetComponentInParent<VehicleController>();
			
			_rigidbody = GetComponentInParent<Rigidbody>();
			_flipOver = GetComponentInParent<FlipOver>();
			_flipOver.enabled = false;
			
			_carTransform = transform.parent;
		}

		private void Update ()
		{
			var hits = Physics.RaycastAll(transform.position, Vector3.down, 4);
			foreach (var hit in hits)
			{
				if (!hit.collider.CompareTag(GameConstants.TerrainTag)) continue;
				SoftLanding();
			}
		}

		public void SoftLanding()
		{
			if (_carController.CurrentCarType != ActiveCarType.LocalPlayer || Scores.Instance.SoftLanding)
				_landingManager.SoftLanding();
			else
				_landingManager.SelfLanding();
			
			gameObject.SetActive(false);
		}

		private void FixedUpdate()
		{
			if (_carController.CurrentCarType != ActiveCarType.LocalPlayer && _carController.CurrentCarType != ActiveCarType.LocalAi)
				return;

			if (_carController.CurrentCarType == ActiveCarType.LocalAi)
			{
				_lifeTime += Time.deltaTime;
				if (_lifeTime < 5)
				{
					_lifeTimeStep += Time.deltaTime;
					if (_lifeTimeStep > _needTimeStep)
					{
						_lifeTimeStep = Random.Range(0, _needTimeStep/2f);
						_currentSteer = Random.Range(-1f, 1f);
						_currentBrake = Random.Range(0.2f, 1f);
					}

					_vehicleController.steerInput = _currentSteer;
					_vehicleController.brakeInput = _currentBrake;
				}
				else
				{
					_vehicleController.steerInput = 0;
					_vehicleController.brakeInput = 0;
					_vehicleController.throttleInput = 1f;
				}
			}
			
			var horisontal = Mathf.Clamp(_vehicleController.steerInput, -1.0f, 1.0f) *
			                 (Time.deltaTime * RotationSpeed);
			var vertival = Mathf.Clamp(Math.Abs(_vehicleController.throttleInput) > 0.01f ? _vehicleController.throttleInput: -_vehicleController.brakeInput, -1.0f, 1.0f) * (Time.deltaTime * RotationSpeed);

			_carTransform.Rotate(vertival, 0, -horisontal);
			var euler = _carTransform.eulerAngles;
			euler.x = Utils.ClampAngle(euler.x, -3, 70);
			euler.z = Utils.ClampAngle(euler.z, -45, 45);
			_carTransform.eulerAngles = euler;

			var forward = _carTransform.forward;
			forward.y = Mathf.Clamp(forward.y, -1f, -0.17f);
			_rigidbody.velocity = forward * 100;
			_rigidbody.angularVelocity = Vector3.zero;

			_rightBlade.Rotate(new Vector3(0, 3000 * Time.deltaTime, 0));
			_leftBlade.Rotate(new Vector3(0, 3000 * Time.deltaTime, 0));
		}
	}
}
