﻿using System.Collections;
using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
	public class FlyController : MonoBehaviourPun, IPunObservable
	{
		[SerializeField] private GameObject _minimapIcon;
		[SerializeField] private float _spawnRadius = 20f;

		[SerializeField] private Vector3 _startPoint;
		[SerializeField] private Vector3 _endPoint;

		private float _lastLifeTime;
		[SerializeField] private float _lifeTime;

		[SerializeField] private float _minTimeToDesant = 15f;
		[SerializeField] private float _maxTimeToDesant = 65f;
		[SerializeField] private float _destroyTime = 70f;

		private bool _landingMessageSended;

		[SerializeField] private int _needBots = 40;
		private int _startNeedBots;
		
		private float _spawnTime = 1f;
		private Transform _transform;

		private const int Radius = 1100;
		public static Vector3 GetStartPoint()
		{
			var angle = Random.Range(0, 360f);
			return new Vector3(0 + Radius*Mathf.Cos(angle), 480, 0 + Radius*Mathf.Sin(angle));
		}

		private void OnSetLocalPlayer(GameObject player)
		{
			_landingMessageSended = true;
		}

		private void Awake()
		{
			_transform = transform;
			CarController.OnSetLocalPlayer += OnSetLocalPlayer;
			
			if (PhotonNetwork.OfflineMode || PhotonNetwork.CurrentRoom == null)
			{
				_startPoint = GetStartPoint();
				_needBots = Mathf.Clamp(GameConstants.MaxRealPlayers - 1, 0, GameConstants.MaxAiPlayers);
				FakeCarsManager.NeedFakePlayers = GameConstants.MaxRealPlayers - _needBots - 1;
			} else if (PhotonNetwork.CurrentRoom.CustomProperties != null &&
			           PhotonNetwork.CurrentRoom.CustomProperties["StartFlyPos"] != null)
			{
				_startPoint = (Vector3) PhotonNetwork.CurrentRoom.CustomProperties["StartFlyPos"];
				_needBots = Mathf.Clamp(GameConstants.MaxRealPlayers - PhotonNetwork.CurrentRoom.PlayerCount, 0, GameConstants.MaxAiPlayers);
				FakeCarsManager.NeedFakePlayers = GameConstants.MaxRealPlayers - _needBots - PhotonNetwork.CurrentRoom.PlayerCount;
			}
			_startNeedBots = _needBots;
			
			_endPoint = new Vector3(-_startPoint.x, _startPoint.y, -_startPoint.z);
			transform.position = _startPoint;
			transform.LookAt(_endPoint);
		}

		private void Update()
		{
			if (photonView.IsMine)
			{
				_lifeTime += Time.deltaTime;
				if (_lifeTime > _destroyTime)
				{
					PhotonNetwork.Destroy(gameObject);
					return;
				}
				transform.position = Vector3.Lerp(_startPoint, _endPoint, _lifeTime / _destroyTime);
			}
			else
			{
				_lastLifeTime = Mathf.Lerp(_lastLifeTime, _lifeTime, 4 * Time.deltaTime);
				transform.position = Vector3.Lerp(_startPoint, _endPoint, _lastLifeTime / _destroyTime);
			}

			if (GameControllers.GameState == GameState.PreDesant && MayLandMe())
			{
				GameControllers.GameState = GameState.Desant;
			}

			if (PhotonNetwork.IsMasterClient && MayLandMe() && _needBots > 0)
			{
				_spawnTime -= Time.deltaTime;
				if (_spawnTime <= 0)
				{
					_needBots--;
					var maxSpawnTime = (_maxTimeToDesant - _minTimeToDesant) / _startNeedBots;
					_spawnTime = Random.Range(maxSpawnTime/2f, 1.1f * maxSpawnTime);
					CarSpawner.SpawnCar(true);
				}
			}

			if (!_landingMessageSended && _lifeTime > _maxTimeToDesant)
			{
				_landingMessageSended = true;
				GameControllers.GameState = GameState.GamePlay;
				CarSpawner.SpawnCar();
			}
		}

		public bool MayLandMe()
		{
			return _lifeTime >= _minTimeToDesant;
		}

		public Vector3 GetSpawnPointByIndex(out Quaternion spawnRot)
		{
			var randomPoint = _spawnRadius * Random.insideUnitCircle;
			var position = _transform.position;
			var newPos = new Vector3
			{
				x = position.x + randomPoint.x,
				y = position.y - 10,
				z = position.z + randomPoint.y
			};
			spawnRot = _transform.rotation;
			return newPos;
		}

		public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
		{
			if (stream.IsWriting)
			{
				_lastLifeTime = _lifeTime;
				stream.SendNext(_lifeTime);
			}
			else
			{
				_lastLifeTime = _lifeTime;
				_lifeTime = (float) stream.ReceiveNext();
			}
		}

		private void OnDestroy()
		{
			if(_lifeTime > _destroyTime)
				Destroy(Instantiate(InGamePrefabs.Instance.particles.GetRandomExplosion(), transform.position, Quaternion.identity), 1f);
			
			Destroy(_minimapIcon);
			CarController.OnSetLocalPlayer -= OnSetLocalPlayer;
		}
	}
}
