﻿using UnityEngine;
using UnityEngine.Serialization;

namespace CarsBattleRoyal
{
	public class BattleRoyaleManager : GameModeManager {
		[SerializeField] private GameObject plane;
		private FlyController _flyController;
		
		private void Start()
		{
			plane.SetActive(true);
			_flyController = plane.GetComponent<FlyController>();
		}

		protected override void OnAliveChanged(int lastValue, int newValue)
		{
			if (GameControllers.GameState == GameState.EndGame || newValue > 1) return;			
			if (CarController && !CarController.Destroyed)
			{
				GameControllers.GameState = GameState.EndGame;
			}
		}

		public override Vector3 GetSpawnPosition(out Quaternion quaternion, int teamId)
		{
			return _flyController.GetSpawnPointByIndex(out quaternion);
		}
	}
}
