﻿using System;
using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CarsBattleRoyal
{
	public enum ZoneType
	{
		Circle,
		Line
	}

	[Serializable]
	public struct Zones
	{
		public ZoneType ZoneType;
		public GameObject ZoneObject;
		public bool Available;
	}
	public class ZonesController : MonoBehaviourPun, IPunObservable {
		[SerializeField] private Zones[] _zones;
		[SerializeField] private ZoneTypeAnimation _typeAnimation;
		private Transform _zoneAnchor;

		private void Start ()
		{
			Zones zone;
			if (Utils.NowInRoom())
			{
				var zoneType = (ZoneType) PhotonNetwork.CurrentRoom.CustomProperties["ZoneType"];
				zone = GetZoneByType(zoneType);
			}
			else
			{
				zone = _zones[Random.Range(0, _zones.Length)];
				while (!zone.Available)
				{
					zone = _zones[Random.Range(0, _zones.Length)];
				}
			}
			zone.ZoneObject.SetActive(true);
			_zoneAnchor = zone.ZoneObject.GetComponent<DangerZoneController>().ZoneAnchor;
			_typeAnimation.ZoneType = zone.ZoneType;
		}

		private Zones GetZoneByType(ZoneType zoneType)
		{
			foreach (var zone in _zones)
			{
				if (zone.ZoneType == zoneType)
					return zone;
			}
			return _zones[0];
		}

		public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
		{
			if(!_zoneAnchor)
				return;
			if (stream.IsWriting)
			{
				stream.SendNext(_zoneAnchor.position);
				stream.SendNext(_zoneAnchor.rotation);
				stream.SendNext(_zoneAnchor.localScale);
			}
			else
			{
				_zoneAnchor.position = (Vector3) stream.ReceiveNext();
				_zoneAnchor.rotation = (Quaternion) stream.ReceiveNext();
				_zoneAnchor.localScale = (Vector3) stream.ReceiveNext();
			}
		}
	}
}
