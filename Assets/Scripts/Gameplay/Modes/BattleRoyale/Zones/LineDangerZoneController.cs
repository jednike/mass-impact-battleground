﻿using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CarsBattleRoyal
{
	public class LineDangerZoneController : DangerZoneController
	{
		[SerializeField] private float _speed = 0.3f;
		
		protected override void Init()
		{
			if (Utils.NowInRoom())
			{
				AnchorSafety.localEulerAngles = new Vector3(0, (float) PhotonNetwork.CurrentRoom.CustomProperties["ZoneAngle"], 0);
			}
			else
			{
				AnchorSafety.localEulerAngles = new Vector3(0, Random.Range(0, 360), 0);
			}
		}
		
		protected override void StartResize()
		{
			CurrentTime = 0;
						
			ZoneState = ZoneState.Resize;
		}

		protected override void Resize()
		{
			AnchorSafety.localPosition += AnchorSafety.forward * _speed;
		}
	}
}
