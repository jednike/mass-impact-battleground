﻿using UnityEngine;

namespace CarsBattleRoyal
{
	public class ZoneTypeAnimation : MonoBehaviour {
		private Animator _mapAnimator;
		public ZoneType ZoneType;
		
		private void Start ()
		{
			_mapAnimator = GetComponent<Animator>();
			_mapAnimator.Play(ZoneType.ToString());
		}
	}
}
