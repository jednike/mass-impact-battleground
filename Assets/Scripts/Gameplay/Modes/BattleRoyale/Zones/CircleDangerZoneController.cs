﻿using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CarsBattleRoyal
{
	public class CircleDangerZoneController : DangerZoneController
	{		
		private float _startSize;

		protected override void Init()
		{
			var newPosition = AnchorSafety.localPosition;
			if (Utils.NowInRoom())
			{
				newPosition.x = (float) PhotonNetwork.CurrentRoom.CustomProperties["ZonePositionX"];
				newPosition.z = (float) PhotonNetwork.CurrentRoom.CustomProperties["ZonePositionY"];
			}
			else
			{
				var radius = (GameConstants.CircleRadius * _startSize) / GameConstants.CircleRisizeKoef;
				radius = Random.Range(0, radius);
				var randomPointInCircle = radius * Random.insideUnitCircle;
				
				newPosition.x = randomPointInCircle.x;
				newPosition.z = randomPointInCircle.y;
			}

			AnchorSafety.localPosition = newPosition;
			Safety.localPosition = -AnchorSafety.localPosition;
		}

		protected override void StartResize()
		{
			CurrentTime = 0;
			_startSize = AnchorSafety.localScale.x;
						
			ZoneState = ZoneState.Resize;
		}

		protected override void Resize()
		{
			var scale = Mathf.Lerp(_startSize, _startSize / GameConstants.CircleRisizeKoef, CurrentTime / TimeToResize);
			AnchorSafety.localScale = new Vector3(scale, 1, scale);
		}
	}
}
