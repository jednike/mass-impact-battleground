﻿﻿using System.Collections;
 using Photon.Pun;
 using UnityEngine;
using Random = UnityEngine.Random;

namespace CarsBattleRoyal
{
	public enum ZoneState
	{
		WaitStart,
		Resize,
		WaitResize
	}
	public abstract class DangerZoneController : MonoBehaviour
	{
		[SerializeField] private float _firstDelay = 30f;
		[SerializeField] private float _delay = 15f;
		[SerializeField] protected float TimeToResize = 15f;
				
		[SerializeField] protected ZoneState ZoneState;
		[SerializeField] protected Transform AnchorSafety;

		public Vector3 DangerZonePosition => AnchorSafety.position;
		public Transform ZoneAnchor => AnchorSafety;

		[SerializeField] protected Transform Safety;

		protected float CurrentTime;

		private void Awake()
		{
			Init();
			if (!PhotonNetwork.IsMasterClient) enabled = false;
		}

		private IEnumerator Start()
		{
			var startPosY = transform.position.y;
			const int endPosY = 0;
			var time = 0f;
			var needTime = Mathf.Clamp(_firstDelay - 10, 0.1f, _firstDelay + 10);
			while (time < needTime)
			{
				time += Time.deltaTime;
				var position = transform.position;
				position = new Vector3(position.x, Mathf.Lerp(startPosY, endPosY, time/needTime), position.z);
				transform.position = position;
				yield return new WaitForSeconds(Time.deltaTime);
			}
		}

		private void Update()
		{
			if (AnchorSafety.localScale.x <= 0.01f)
			{
				AnchorSafety.localScale = Vector3.zero;
				return;
			}

			CurrentTime += Time.deltaTime;
			switch (ZoneState)
			{
				case ZoneState.WaitStart:
					if (CurrentTime > _firstDelay)
						StartResize();
					break;
				case ZoneState.Resize:
					Resize();
					if (CurrentTime > TimeToResize)
					{
						CurrentTime = 0;
						ZoneState = ZoneState.WaitResize;
					}
					break;
				case ZoneState.WaitResize:
					if (CurrentTime > _delay)
						StartResize();
					break;
			}
		}

		protected abstract void Init();
		protected abstract void StartResize();
		protected abstract void Resize();
	}
}
