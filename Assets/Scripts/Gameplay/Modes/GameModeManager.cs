﻿using UnityEngine;

namespace CarsBattleRoyal
{
	public class GameModeManager : LocalSingletonBehaviour<GameModeManager> {
		protected CarController CarController;
		
		public override void DoAwake()
		{
			CarController.OnSetLocalPlayer += SetLocalPlayer;
			CarCount.OnAliveCarsChanged += OnAliveChanged;
		}
		public override void DoDestroy()
		{
			CarController.OnSetLocalPlayer -= SetLocalPlayer;
			CarCount.OnAliveCarsChanged -= OnAliveChanged;
		}

		private void SetLocalPlayer(GameObject localPlayer)
		{
			CarController = localPlayer.GetComponent<CarController>();
		}
		
		protected virtual void OnAliveChanged(int lastValue, int newValue){}

		public virtual Vector3 GetSpawnPosition(out Quaternion quaternion, int teamId)
		{
			quaternion = Quaternion.identity;
			return Vector3.zero;
		}
	}
}
