﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CarsBattleRoyal
{
	[Serializable]
	public class TeamSpawnPoints
	{
		public List<Transform> Points = new List<Transform>();
		public readonly List<int> Used = new List<int>();
	}
	[Serializable]
	public class TeamMembers
	{
		public List<CarController> Cars = new List<CarController>();
	}
	public class SmallBattleManager : GameModeManager
	{
		private List<TeamSpawnPoints> _teamSpawnPoints = new List<TeamSpawnPoints>();

		[SerializeField] private int _maxRounds = 3;
		private int _round;

		public override void DoAwake()
		{
			base.DoAwake();
			var spawnPoint = GameObject.FindGameObjectWithTag("SpawnTeamPoint").transform;
			foreach (Transform team in spawnPoint)
			{
				var teamPoints = new TeamSpawnPoints();
				foreach (Transform point in team)
				{
					teamPoints.Points.Add(point);
				}
				_teamSpawnPoints.Add(teamPoints);
			}
		}

		private void Update()
		{
			var allCars = CarsInGameController.GetAllCarsWithTeamId();
		}

		private void ClearPoints()
		{
			foreach (var points in _teamSpawnPoints)
			{
				points.Used.Clear();
			}
		}

		public override Vector3 GetSpawnPosition(out Quaternion quaternion, int teamId)
		{
			var curTeam = (int) Mathf.Repeat(teamId + _round, _teamSpawnPoints.Count);
			if (teamId > _teamSpawnPoints.Count)
				return base.GetSpawnPosition(out quaternion, teamId);
			var teamPoints = _teamSpawnPoints[curTeam];
			if (teamPoints.Points.Count <= teamPoints.Used.Count)
				return base.GetSpawnPosition(out quaternion, teamId);

			int index;
			do
			{
				index = Random.Range(0, teamPoints.Points.Count);
			} while (teamPoints.Used.Contains(index));
			teamPoints.Used.Add(index);
			quaternion = teamPoints.Points[index].rotation;
			return teamPoints.Points[index].position;
		}
	}
}
