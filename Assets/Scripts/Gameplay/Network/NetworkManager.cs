﻿using System;
using System.Collections;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Random = UnityEngine.Random;

namespace CarsBattleRoyal
{
	[RequireComponent(typeof(PhotonView))]
	public class NetworkManager : MonoBehaviourPunCallbacks
	{
		public static Text CountdownText;
		public static Image TotalTimeImage;

		private const float MaxWaitingTime = 23f;
		private float _waitingTime = 23f;

		private const float MaxTotalWaitingTime = 60f;
		private float _totalWaitingTime = 60f;

		public static NetworkManager Instance
		{
			get
			{
				if (!_instance)
				{
					new GameObject("NetworkManager").AddComponent<NetworkManager>();
				}
				return _instance;
			}
		}
		private static NetworkManager _instance;

		private void Awake()
		{
			PhotonPeer.RegisterType(typeof(Point), (byte) 'Z', Point.Serialize, Point.Deserialize);
			
			PhotonNetwork.BackgroundTimeout = 20f;
			PhotonNetwork.AddCallbackTarget(this);

			#if UNITY_STANDALONE
			PhotonNetwork.NickName = "User" + Random.Range(0, 99999);
			#else
			PhotonNetwork.NickName = Scores.Instance.Nickname;
			#endif
			
			_instance = this;
			
			DontDestroyOnLoad(gameObject);
		}

		private void Update()
		{
			if(GameControllers.GameState != GameState.WaitingPlayers)
				return;
			_waitingTime -= Time.deltaTime;
			_totalWaitingTime -= Time.deltaTime;

			if (TotalTimeImage)
				TotalTimeImage.fillAmount = _totalWaitingTime / MaxTotalWaitingTime;
			
			if(!PhotonNetwork.IsMasterClient)
				return;
			if (_waitingTime <= 0 || _totalWaitingTime <= 0)
			{
				if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
				{
					NotAvailableConnect();
				}
				else
				{
					PhotonNetwork.CurrentRoom.IsOpen = false;
					photonView.RPC("WaitingStartGame", RpcTarget.All);
				}
			}
		}

		private void OnEnable()
		{
			GameControllers.OnGameStateChanged += OnGameStateChanged;
		}

		private void OnDisable()
		{
			GameControllers.OnGameStateChanged -= OnGameStateChanged;
		}

		[PunRPC]
		public void RunCountdownBeforeStart()
		{
			StartCoroutine(CountdownBeforeStart());
		}
		[PunRPC]
		public void WaitingStartGame()
		{
			GameControllers.GameState = GameState.WaitingStartGame;
		}

		private static IEnumerator CountdownBeforeStart()
		{
			float time = 3;
			while (time > 0)
			{
				switch (GameControllers.GameState)
				{
					case GameState.WaitingStartGame:
						if(CountdownText)
							CountdownText.text = 
								MultiLanguageManager.GetTranslateString("Waiting start game", "Game will start after %", CountdownText).Replace("%", ((int) time).ToString());
						break;
				}
				time -= 1f;
				yield return new WaitForSecondsRealtime(1f);
			}

			GameControllers.GameState = GameState.BeforeStart;
		}

		private void OnGameStateChanged(GameState lastState, GameState newState)
		{
			switch (newState)
			{
				case GameState.MainMenu:
					Disconnect();
					break;
				case GameState.ConnectingToServer:
					if (TotalTimeImage)
						TotalTimeImage.transform.parent.gameObject.SetActive(false);

					if (!PhotonNetwork.IsConnected)
					{
						PhotonNetwork.ConnectUsingSettings();
						PhotonNetwork.GameVersion = GameConstants.GameVersion;
					}
					else
					{
						GameControllers.GameState = GameState.ConnectingToRoom;
					}
					break;
				case GameState.WaitingStartGame:
					
					if (TotalTimeImage)
						TotalTimeImage.transform.parent.gameObject.SetActive(false);
					
					StartCoroutine(CountdownBeforeStart());
					break;
				case GameState.ConnectingToRoom:
					if (TotalTimeImage)
						TotalTimeImage.transform.parent.gameObject.SetActive(false);
					PhotonNetwork.JoinRandomRoom(null, 0);	
					break;
				case GameState.WaitingPlayers:
					if (TotalTimeImage)
						TotalTimeImage.transform.parent.gameObject.SetActive(true);
					_waitingTime = MaxWaitingTime;
					_totalWaitingTime = MaxTotalWaitingTime;
					break;
				case GameState.BeforeStart:
					if (TotalTimeImage)
						TotalTimeImage.transform.parent.gameObject.SetActive(false);
					if (PhotonNetwork.IsMasterClient)
					{
						photonView.RPC("StartLoadScene", RpcTarget.All);
					}
					break;
				case GameState.LoadMenu:
					Disconnect();
					break;
			}
		}

		public static void Disconnect()
		{
			if(PhotonNetwork.IsConnected)
				PhotonNetwork.Disconnect();
		}
		
		#region Photon

		private void OnConnectedToServer()
		{
			Debug.Log("Connected to server");
		}

		public override void OnConnectedToMaster()
		{
			GameControllers.GameState = GameState.ConnectingToRoom;
		}
		public override void OnJoinedLobby()
		{
			GameControllers.GameState = GameState.ConnectingToRoom;
		}
		public override void OnJoinedRoom()
		{
			GameControllers.GameState = GameState.WaitingPlayers;
		}

		public override void OnJoinRandomFailed(short returnCode, string message)
		{
			PhotonNetwork.CreateRoom(null, new RoomOptions {MaxPlayers = GameConstants.MaxRealPlayers, CustomRoomProperties = new Hashtable{{"StartFlyPos", FlyController.GetStartPoint()}}}, null);
		}

		public override void OnCreatedRoom()
		{
			var roomProperties = PhotonNetwork.CurrentRoom.CustomProperties;
			var zoneTypes = new[] {ZoneType.Line, ZoneType.Circle};
			var zoneType = zoneTypes[Random.Range(0, zoneTypes.Length)];
			roomProperties["ZoneType"] = zoneType;
			switch (zoneType)
			{
				case ZoneType.Circle:
					var radius = GameConstants.CircleRadius / GameConstants.CircleRisizeKoef;
					radius = Random.Range(0, radius);
					var randomPointInCircle = radius * Random.insideUnitCircle;
					roomProperties["ZonePositionX"] = randomPointInCircle.x;
					roomProperties["ZonePositionY"] = randomPointInCircle.y;
					break;
				case ZoneType.Line:
					roomProperties["ZoneAngle"] = Random.Range(0f, 360f);
					break;
			}
			PhotonNetwork.CurrentRoom.SetCustomProperties(roomProperties);
		}
        public override void OnLeftRoom()
        {
        }
		public override void OnLeftLobby()
		{
		}

        public override void OnMasterClientSwitched(Player newMasterClient)
        {
	        if (newMasterClient.IsLocal && GameControllers.GameState == GameState.BeforeStart)
	        {
		        photonView.RPC("StartLoadScene", RpcTarget.All);
	        }
        }

		#region Fails

		public override void OnCreateRoomFailed(short returnCode, string message)
		{
			NotAvailableConnect();
		}

		public override void OnJoinRoomFailed(short returnCode, string message)
		{
			NotAvailableConnect();
		}

		private void NotAvailableConnect()
		{
			StartCoroutine(UnAvailableConnectCoroutine());
		}

		private IEnumerator UnAvailableConnectCoroutine()
		{
			
			while (PhotonNetwork.IsConnected)
			{
				PhotonNetwork.Disconnect();
				yield return new WaitForSeconds(0.1f);
			}
			
			if (GameControllers.GameState == GameState.ConnectingToRoom ||
			    GameControllers.GameState == GameState.WaitingPlayers ||
			    GameControllers.GameState == GameState.WaitingStartGame ||
			    GameControllers.GameState == GameState.ConnectingToServer)
			{
				PhotonNetwork.OfflineMode = true;
				GameControllers.GameState = GameState.WaitingStartGame;
			}
			else if (GameControllers.GameState == GameState.GamePlay ||
			         GameControllers.GameState == GameState.Pause)
			{
				GameControllers.GameState = GameState.LoadMenu;
			}
		}

		#endregion

		#region Player Connected

		public override void OnPlayerEnteredRoom(Player newPlayer)
		{
			if(!PhotonNetwork.IsMasterClient)
				return;
			
			_waitingTime = MaxWaitingTime;
			if (PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers)
			{
				PhotonNetwork.CurrentRoom.IsOpen = false;
				photonView.RPC("WaitingStartGame", RpcTarget.All);
			}
		}

		public override void OnPlayerLeftRoom(Player otherPlayer)
		{
			base.OnPlayerLeftRoom(otherPlayer);
		}
		
		#endregion

		#region Changed Properties


		#endregion

		#endregion

		#region RPC

		[PunRPC]
		public void StartLoadScene()
		{
			PhotonNetwork.IsMessageQueueRunning = false;
			SceneManager.LoadScene("LoadScene");
		}

		public void OnApplicationFocus(bool hasFocus)
		{
			if (!hasFocus)
			{
				ChangeMaster();
			}
		}

		public static void ChangeMaster()
		{
			if (!PhotonNetwork.IsConnected || !PhotonNetwork.IsMasterClient || !PhotonNetwork.InRoom || PhotonNetwork.CurrentRoom.PlayerCount <= 1)
				return;
			PhotonNetwork.SetMasterClient(PhotonNetwork.MasterClient.GetNext());
		}

		#endregion
	}
}
