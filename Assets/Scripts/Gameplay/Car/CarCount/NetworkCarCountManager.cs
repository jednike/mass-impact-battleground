using System;
using Photon.Pun;

namespace CarsBattleRoyal
{
    public class NetworkCarCountManager: MonoBehaviourPun, IPunObservable
    {
        private void Start()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                gameObject.AddComponent<LocalCarCountManager>();
            }
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(CarCount.AliveCars);
            }
            else
            {
                CarCount.AliveCars = (int) stream.ReceiveNext();
            }
        }
    }
}