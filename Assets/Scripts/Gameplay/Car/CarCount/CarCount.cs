using System;

namespace CarsBattleRoyal
{
    public static class CarCount
    {
        public static event Action<int, int> OnAliveCarsChanged = delegate {  };
        public static int AliveCars
        {
            get => _aliveCars;
            set
            {
                if(value == _aliveCars)
                    return;
                var lastValue = _aliveCars;
                _aliveCars = value;
                OnAliveCarsChanged(lastValue, value);
            }
        }
        private static int _aliveCars;
    }
}