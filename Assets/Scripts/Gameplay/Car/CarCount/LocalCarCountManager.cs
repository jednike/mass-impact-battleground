using System;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class LocalCarCountManager: MonoBehaviour
    {
        private FakeCarsManager _fakeCarsManager;
        private void Awake()
        {
            _fakeCarsManager = new FakeCarsManager();
            CarCount.AliveCars = GameConstants.MaxRealPlayers;
            CarDeathHandler.CarKilled += CarKilled;
        }

        private void Update()
        {
            _fakeCarsManager.Update(Time.deltaTime);
        }

        private void CarKilled(CarController carController)
        {
            CarCount.AliveCars = CarsInGameController.GetRealAliveCount() + _fakeCarsManager.CurrentFakePlayers;
        }
        
        private void OnDestroy()
        {
            CarDeathHandler.CarKilled -= CarKilled;
            CarCount.AliveCars = 0;
        }
    }
}