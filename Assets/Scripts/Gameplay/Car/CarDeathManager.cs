﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
    [RequireComponent(typeof(HealthManager))]
    [RequireComponent(typeof(DamageCarManager))]
    [RequireComponent(typeof(CarController))]
    [DisallowMultipleComponent]
    public class CarDeathManager : MonoBehaviourPunCallbacks
    {
        private HealthManager _healthManager;
        private DamageCarManager _damageManager;
        private CarController _car;

        private void Awake()
        {
            _healthManager = GetComponent<HealthManager>();
            _damageManager = GetComponent<DamageCarManager>();
            _car = GetComponent<CarController>();
            
            _healthManager.HpChanged += OnHpChanged;
        }

        private void OnHpChanged(float oldHp, float newHp)
        {
            if((_car.CurrentCarType != ActiveCarType.LocalPlayer && _car.CurrentCarType != ActiveCarType.LocalAi) || _car.Destroyed)
                return;
            if (Math.Abs(newHp) < 0.01f)
            {
                photonView.RPC("DestroyCar", RpcTarget.All, _damageManager.KillerId);
            }
        }

        private void OnDestroy()
        {
            _healthManager.HpChanged -= OnHpChanged;
        }
    }
}
