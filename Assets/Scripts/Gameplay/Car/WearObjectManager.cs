using System;
using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class WearObjectManager: DefaultWeaponManager
    {
        public event Action<ObjectsType> OnChangeWeapon = delegate {  };

        public WearObjectController CurrentWeapon => _currentWeapon;
        private WearObjectController _currentWeapon;
        protected override string EventName => "ThrowWear";

        public override void InstallWeapon(PickUpObject weaponManager)
        {
            _currentWeapon = (WearObjectController) weaponManager;
            WeaponTransform = _currentWeapon.transform;
			
            WeaponTransform.SetParent(GunTransformManager.GetWeaponPoint());
            WeaponTransform.localPosition = Vector3.zero;
            WeaponTransform.localRotation = Quaternion.identity;

            foreach (var particle in _currentWeapon.GetComponentsInChildren<ParticleSystem>())
            {
                Destroy(particle);
            }
            foreach (var particle in _currentWeapon.GetComponentsInChildren<Collider>())
            {
                particle.enabled = false;
            }
			
            OnChangeWeapon(_currentWeapon.WeaponType);
        }
        public override bool ExistWeapon()
        {
            return _currentWeapon;
        }
        public override void RemoveWeapon(PickUpObject pickUpObject)
        {
            
        }
        public void UseWeapon()
        {
            if (!ExistWeapon())
                return;
            _currentWeapon.UseObject();
        }
        
        [PunRPC]
        public void ThrowWear()
        {
            if(!_currentWeapon)
                return;
			
            Destroy(_currentWeapon);
            _currentWeapon = null;
            OnChangeWeapon(ObjectsType.None);
        }
    }
}