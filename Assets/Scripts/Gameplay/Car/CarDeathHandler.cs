﻿using System;
using System.Collections;
using EVP;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Serialization;

namespace CarsBattleRoyal
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(DamageSoundManager))]
    [RequireComponent(typeof(CarController))]
    [RequireComponent(typeof(GunManager))]
    public class CarDeathHandler : MonoBehaviour
    {
        public static event Action<CarController> CarKilled = delegate {  };
        
        [SerializeField] private GameObject deathEffect;
        
        private DamageSoundManager _damageSoundManager;
        private CarController _car;
        private GunManager _gunManager;
        private Transform _transform;

        private void Awake()
        {
            _damageSoundManager = GetComponent<DamageSoundManager>();
            _car = GetComponent<CarController>();
            _gunManager = GetComponent<GunManager>();
            _transform = transform;
        }
        
        private IEnumerator DestroyCarObjectByTime()
        {
            yield return new WaitForSeconds(5f);
            PhotonNetwork.Destroy(gameObject);
        }
        
        [PunRPC]
        public void DestroyCar(int killerId)
        {
            if(_car.Destroyed)
                return;
            
            _car.Destroyed = true;
            tag = "Finish";
            CarKilled(_car);
			
            Destroy(GetComponent<CarInput>());
            var carType = _car.CurrentCarType;
            if (carType == ActiveCarType.LocalAi || carType == ActiveCarType.LocalPlayer)
            {
                _gunManager.ThrowWeapon();
            }

            switch (carType)
            {
                case ActiveCarType.LocalAi:
                    StartCoroutine(DestroyCarObjectByTime());
                    break;
                case ActiveCarType.LocalPlayer:
                    GameControllers.GameState = GameState.EndGame;
                    break;
            }

            if(killerId != 999)
                ApplyVisualEffects();

            AddKillsToKiller(killerId);
        }

        private void ApplyVisualEffects()
        {
            deathEffect.SetActive(true);
            _damageSoundManager.PlayDamageSound(InGamePrefabs.Instance.sounds.destroyCarSound);
            Instantiate(InGamePrefabs.Instance.particles.GetRandomExplosion(), _transform.position, _transform.rotation);
        }

        private static void AddKillsToKiller(int killerId)
        {
            if (!CarsInGameController.GetCarControllerById(killerId, out var killerCar)) return;
            if (killerCar.CurrentCarType == ActiveCarType.LocalPlayer)
            {
                InfoMessages.ShowMessage(MessageTypes.EnemyDestroy, new object[] {killerCar.NickName});
            }

            killerCar.GetComponent<KilledCarsManager>().KilledCars++;
        }
    }
}
