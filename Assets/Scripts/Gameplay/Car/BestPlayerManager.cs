﻿using System;
using UnityEngine;

namespace CarsBattleRoyal
{
    [RequireComponent(typeof(CarController))]
    [DisallowMultipleComponent]
    public class BestPlayerManager : MonoBehaviour
    {
        public static int maxKills;
        public bool IsBest
        {
            get => _isBest;
            set
            {
                if (value == _isBest)
                    return;
                _isBest = value;
                if (_car.CurrentCarType == ActiveCarType.LocalPlayer)
                {
                    InfoMessages.ShowMessage(_isBest ? MessageTypes.NowYouLeader : MessageTypes.NowNotLeader,
                        new object[] {""});
                }
            }
        }
        private bool _isBest;

        private CarController _car;

        private void Awake()
        {
            _car = GetComponent<CarController>();
            KilledCarsManager.KillsChanged += OnKillsChanged;
        }

        private void OnKillsChanged(CarController car, int kills)
        {
            maxKills = Mathf.Max(kills, maxKills);
            if (car == _car)
            {
                IsBest = kills == maxKills;
            }
        }

        private void OnDestroy()
        {
            KilledCarsManager.KillsChanged -= OnKillsChanged;
        }
    }
}