﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class SmokeCarManager : MonoBehaviour
    {
        [SerializeField] private GameObject smokeEffect;
        
        private HealthManager _healthManager;

        private void Awake()
        {
            _healthManager = GetComponent<HealthManager>();
            _healthManager.HpChanged += OnHpChanged;
        }

        private void OnHpChanged(float oldHp, float newHp)
        {
            smokeEffect.SetActive(newHp <= 0.4f * _healthManager.MaxHp);
        }

        private void OnDestroy()
        {
            _healthManager.HpChanged -= OnHpChanged;
        }
    }
}
