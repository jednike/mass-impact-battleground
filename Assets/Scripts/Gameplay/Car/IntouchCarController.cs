﻿using UnityEngine;

namespace CarsBattleRoyal
{
	public class IntouchCarController : MonoBehaviour {
		private const float MaxLifeTime = 3f;
		private float _lifeTime;

		private void Awake()
		{
			gameObject.ChangeLayer(GameConstants.NotTouchCarLayer);
		}
		
		public void Update()
		{
			_lifeTime += Time.deltaTime;
			if (_lifeTime <= MaxLifeTime) return;
			gameObject.ChangeLayer(GameConstants.CarLayer);
			Destroy(this);
		}
	}
}
