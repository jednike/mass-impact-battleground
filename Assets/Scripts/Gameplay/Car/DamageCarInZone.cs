﻿using System;
using UnityEngine;

namespace CarsBattleRoyal
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(CarController))]
    [RequireComponent(typeof(HealthManager))]
    public class DamageCarInZone : MonoBehaviour
    {
        public static event Action<bool> PlayerInZone = delegate {  };
        
        private CarController _car;
        private HealthManager _healthManager;
        private float _outSafetyZoneTime;

        public bool InZone => _outSafetyZoneTime < -0.2f;
        private bool _inZone;

        private void Awake()
        {
            _healthManager = GetComponent<HealthManager>();
            _car = GetComponent<CarController>();
        }

        private void Update()
        {
            if (InZone)
            {
                _healthManager.Hp -= GameConstants.DamageInDangerZone * Time.deltaTime;
                if (!_inZone)
                {
                    _inZone = true;
                    if (_car.CurrentCarType == ActiveCarType.LocalPlayer)
                        PlayerInZone(true);
                }
            }
            else
            {
                if (_inZone)
                {
                    _inZone = false;
                    if (_car.CurrentCarType == ActiveCarType.LocalPlayer)
                        PlayerInZone(false);
                }
            }

            _outSafetyZoneTime -= Time.deltaTime;
        }
		
        private void OnTriggerStay(Collider other)
        {
            if(other.name == "Safety")
                _outSafetyZoneTime = 0;
        }
    }
}
