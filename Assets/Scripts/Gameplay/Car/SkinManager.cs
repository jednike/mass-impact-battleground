using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class SkinManager: LocalSingletonBehaviour<SkinManager>
    {
        private List<AssetBundle> _bundles = new List<AssetBundle>();

        public static Texture GetSkin(string carName, string skinName)
        {
            var bundle = Instance.GetBundle(carName.ToLower());
            return bundle.LoadAsset<Texture>($"{skinName}");
        }

        private AssetBundle GetBundle(string carName)
        {
            foreach (var bundle in _bundles)
            {
                if (bundle.name == $"skins/{carName}")
                    return bundle;
            }

            var newBundle = AssetBundle.LoadFromFile($"{Application.streamingAssetsPath}/skins/{carName}");
            _bundles.Add(newBundle);
            return newBundle;
        }

        public override void DoDestroy()
        {
            foreach (var bundle in _bundles)
            {
                bundle.Unload(true);
            }
        }
    }
}