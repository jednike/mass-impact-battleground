﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    public interface ISkinSwitcher
    {
        IEnumerable<Transform> GetTransforms(Transform parent);
    }
    public class GarageSkinSwitcher: ISkinSwitcher
    {
        public IEnumerable<Transform> GetTransforms(Transform parent)
        {
            return new[] {parent};
        }
    }
    public class GamePlaySkinSwitcher: ISkinSwitcher
    {
        public IEnumerable<Transform> GetTransforms(Transform parent)
        {
            return new[] {parent.Find("Body"), parent.Find("Wheels")};
        }
    }
    public class CarSkinManager : MonoBehaviour
    {
        public void ApplySkin(string carName, string skinName, ISkinSwitcher skinSwitcher)
        {
            var texture = SkinManager.GetSkin(carName, skinName);
            foreach (var child in skinSwitcher.GetTransforms(transform))
            {
                foreach (var lRenderer in child.GetComponentsInChildren<Renderer>())
                {
                    lRenderer.material.mainTexture = texture;
                }
            }
        }
    }
}
