﻿using System;
using System.Linq;
using UnityEngine;
using EVP;

namespace CarsBattleRoyal
{
    [Serializable]
    public class FlipSettings
    {
        /// <summary>
        /// Time after detecting flip over after which vehicle will be flipped back.
        /// </summary>
        [Tooltip("Time after detecting flip over after which vehicle will be flipped back.")]
        public float timeout = 1f;

        /// <summary>
        /// Minimum angle that the vehicle needs to be at for it to be detected as flipped over.
        /// </summary>
        [Tooltip("Minimum angle that the vehicle needs to be at for it to be detected as flipped over.")]
        public float allowedAngle = 70f;

        /// <summary>
        /// Flip over detection will be disabled if velocity is above this value [m/s].
        /// </summary>
        [Tooltip("Flip over detection will be disabled if velocity is above this value [m/s].")]
        public float maxDetectionSpeed = 1f;

        /// <summary>
        /// Rotation speed of the vehicle while being flipped back.
        /// </summary>
        [Tooltip("Rotation speed of the vehicle while being flipped back.")]
        public float rotationSpeed = 240f;
    }
    public class FlipOver: MonoBehaviour
    {
        private FlipSettings _flipSettings;
        
        /// <summary>
        /// Is the vehicle flipped over?
        /// </summary>
        [HideInInspector]
        public bool FlippedOver;

        private float _timeSinceFlip;
        private float _timeAfterRecovery;
        private VehicleController _vc;
        private Rigidbody _rigidbody;
        private float direction;

        private void Awake()
        {
            _vc = GetComponent<VehicleController>();
            _rigidbody = GetComponent<Rigidbody>();

            _flipSettings = InGamePrefabs.Instance.flipSettings;
        }
        public void Update()
        {
            DetectFlipOver();

            if (FlippedOver)
            {
                if (Math.Abs(direction) < 0.01f)
                {
                    direction = Mathf.Sign(Vector3.SignedAngle(_vc.transform.up, -Physics.gravity.normalized, _vc.transform.forward) - 180f);
                }

                _rigidbody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
                var yRotation = Quaternion.AngleAxis(_flipSettings.rotationSpeed * Time.fixedDeltaTime, _vc.transform.InverseTransformDirection(_vc.transform.forward));
                _rigidbody.MoveRotation(_vc.transform.rotation * yRotation);
            }
            else
            {
                _rigidbody.constraints = RigidbodyConstraints.None;
            }
        }

        private void DetectFlipOver()
        {
            var wheelsOnGround = _vc.wheelData.Count(wheel => wheel.grounded);

            if(_vc.speed < _flipSettings.maxDetectionSpeed && VehicleAngle() > _flipSettings.allowedAngle && wheelsOnGround <= _vc.wheelData.Length / 2f)
            {
                _timeSinceFlip += Time.fixedDeltaTime;

                if(_timeSinceFlip > _flipSettings.timeout)
                {
                    FlippedOver = true;
                }
            }
            else
            {
                _timeAfterRecovery += Time.fixedDeltaTime;

                if (!(_timeAfterRecovery > 1f) && !(VehicleAngle() < 45f)) return;
                FlippedOver = false;
                _timeSinceFlip = 0f;
                _timeAfterRecovery = 0f;
                direction = 0;
            }
        }

        private float VehicleAngle()
        {
            return Vector3.Angle(_vc.transform.up, -Physics.gravity.normalized);
        }
    }
}

