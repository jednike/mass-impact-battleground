﻿using UltimateSurvival.StandardAssets;
using UnityEngine;

namespace CarsBattleRoyal
{
	public class PlayerInZoneEffect : MonoBehaviour
	{
		private ColorCorrection _colorCorrection;

		private void Awake()
		{
			DamageCarInZone.PlayerInZone += PlayerInZone;
			_colorCorrection = GetComponent<ColorCorrection>();
		}

		private void PlayerInZone(bool state)
		{
			if (_colorCorrection)
				_colorCorrection.enabled = state;
		}

		private void OnDestroy()
		{
			DamageCarInZone.PlayerInZone -= PlayerInZone;
		}
	}
}
