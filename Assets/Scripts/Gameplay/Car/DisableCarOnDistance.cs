﻿using EVP;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class DisableCarOnDistance : MonoBehaviour
    {
        private CarController _carController;

        private void Awake()
        {
            _carController = GetComponent<CarController>();
            _carController.ActiveStateChanged += OnActiveStateChanged;
        }

        private void OnDestroy()
        {
            _carController.ActiveStateChanged -= OnActiveStateChanged;
        }

        private void OnActiveStateChanged(ActiveState newState)
        {
            var active = newState != ActiveState.NotActive;

            var vehicleController = GetComponent<VehicleController>();
            var vehicleAudio = GetComponent<VehicleAudio>();
            var aiController = GetComponent<AIInput>();
            switch (_carController.CurrentCarType)
            {
                case ActiveCarType.LocalAi:
                    vehicleController.enabled = active;
                    vehicleAudio.enabled = active;
                    var flipOver = GetComponent<FlipOver>();
                    if(aiController)
                        aiController.enabled = active;
                    flipOver.enabled = active;
                    break;
                
                case ActiveCarType.NetworkAi:
                    vehicleController.enabled = active;
                    vehicleAudio.enabled = active;
                    break;
                case ActiveCarType.NetworkPlayer:
                    vehicleController.enabled = active;
                    vehicleAudio.enabled = active;
                    break;
            }
        }
    }
}
