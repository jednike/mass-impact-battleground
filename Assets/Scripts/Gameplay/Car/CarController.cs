﻿using System;
using System.Collections;
using EVP;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Serialization;

namespace CarsBattleRoyal
{
	public enum ActiveCarType
	{
		None,
		LocalPlayer,
		LocalAi,
		NetworkAi,
		NetworkPlayer
	}

	public enum ActiveState
	{
		Active,
		NotActive,
		AlwaysActive
	}

	[DisallowMultipleComponent]
	public class CarController : MonoBehaviourPunCallbacks
	{
		public static CarController Local;
		
		public static event Action<GameObject> OnSetLocalPlayer = delegate {  };

		public ActiveState Active
		{
			get => _activeState; 
			set
			{
				if (_activeState == value || _activeState == ActiveState.AlwaysActive)
					return;
				_activeState = value;
				ActiveStateChanged(_activeState);
			}
		}
		private ActiveState _activeState;
		public event Action<ActiveState> ActiveStateChanged = delegate { };

		private VehicleController _vehicleController;
		private FlipOver _flipOver;
		
		public ActiveCarType CurrentCarType
		{
			get => currentCarType;
			private set
			{
				if (currentCarType == value)
					return;
				var lastCarType = currentCarType;
				currentCarType = value;
				CarStateChanged(lastCarType, currentCarType);
				switch (currentCarType)
				{
					case ActiveCarType.LocalPlayer:
					case ActiveCarType.LocalAi:
						_flipOver.enabled = true;
						gameObject.AddComponent<LocalPhotonCarSynchronizer>();
						break;
					case ActiveCarType.NetworkAi:
					case ActiveCarType.NetworkPlayer:
						_flipOver.enabled = false;
						gameObject.AddComponent<NetworkPhotonCarSynchronizer>();
						break;
				}
			}
		}
		[SerializeField] private ActiveCarType currentCarType;
		public event Action<ActiveCarType, ActiveCarType> CarStateChanged = delegate {  };

		public bool Destroyed { get; set; }

		public string NickName { get; private set; }
		public bool SelectedTarget { get; private set; }
		public int TeamId { get; private set; }
		public int OwnerId { get; private set; }

		private bool _isBot;
	
		private void Awake()
		{
			_vehicleController = GetComponent<VehicleController>();
			_flipOver = GetComponent<FlipOver>();

			if(photonView.InstantiationData == null)
				throw new Exception("No data for car");
			
			var carInfo = (SpawnCarInfo) photonView.InstantiationData;
			GetComponent<CarSkinManager>().ApplySkin(carInfo.carName, carInfo.carSkin, new GamePlaySkinSwitcher());
			
			_isBot = carInfo.isBot;
			OwnerId = carInfo.ownerId;
			SelectedTarget = carInfo.focusOnTargetMode;
			TeamId = carInfo.teamId;

			DetectType(_isBot);

			gameObject.AddComponent<IntouchCarController>();

			switch (CurrentCarType)
			{
				case ActiveCarType.LocalPlayer:
					name = PhotonNetwork.LocalPlayer.NickName;
					break;
				case ActiveCarType.LocalAi:
				case ActiveCarType.NetworkAi:
					Active = ActiveState.NotActive;
					name = "AI" + OwnerId;
					_vehicleController.wheelPositionMode = VehicleController.PositionMode.Fast;
					break;
				case ActiveCarType.NetworkPlayer:
					Active = ActiveState.NotActive;
					_vehicleController.wheelPositionMode = VehicleController.PositionMode.Fast;
					name = photonView.Owner.NickName;
					break;
			}
			NickName = name;

			CarsInGameController.AddPlayerToPlayersList(this);
		}

		private void DetectType(bool bot)
		{
			if (bot)
				CurrentCarType = photonView.IsMine ? ActiveCarType.LocalAi : ActiveCarType.NetworkAi;
			else
				CurrentCarType = photonView.IsMine ? ActiveCarType.LocalPlayer : ActiveCarType.NetworkPlayer;

			if (CurrentCarType == ActiveCarType.LocalPlayer)
			{
				Local = this;
				OnSetLocalPlayer(gameObject);
			}
		}

		public override void OnMasterClientSwitched(Player newMasterClient)
		{
			DetectType(_isBot);
		}

		private void OnDestroy()
		{
			CarsInGameController.RemovePlayerFromPlayersList(this);
		}

		public static bool operator ==(CarController left, CarController right)
		{
			if (!left || !right)
				return false;
			return left.OwnerId == right.OwnerId;
		}
		public static bool operator !=(CarController left, CarController right)
		{
			return !(left == right);
		}
	}
}