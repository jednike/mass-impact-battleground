﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    [RequireComponent(typeof(AudioSource))]
    public class DamageSoundManager : MonoBehaviour
    {
        private AudioSource _damageAudioSource;
        private Coroutine _stopDamageSoundCoroutine;

        private void Awake()
        {
            _damageAudioSource = GetComponent<AudioSource>();
        }

        public void PlayDamageSound(AudioClip clip)
        {
            if(!_damageAudioSource.isPlaying)
                _damageAudioSource.PlayOneShot(clip);
			
            if(_stopDamageSoundCoroutine != null)
                StopCoroutine(_stopDamageSoundCoroutine);
            _stopDamageSoundCoroutine = StartCoroutine(StopDamageSound());
        }
        
        private IEnumerator StopDamageSound()
        {
            yield return new WaitForSeconds(0.3f);
            _damageAudioSource.Stop();
        }
    }
}
