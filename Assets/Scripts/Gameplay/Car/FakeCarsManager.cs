using UnityEngine;

namespace CarsBattleRoyal
{
    public class FakeCarsManager
    {
        public static int NeedFakePlayers;

        public int CurrentFakePlayers => _currentFakePlayers;
        private int _currentFakePlayers;
        
        private float _minTimeToSpawnFake;
        private float _curTimeToSpawnFake;
        private bool _mayDecreaseFake;

        public FakeCarsManager()
        {
            _currentFakePlayers = NeedFakePlayers;
            _minTimeToSpawnFake = Random.Range(23f, 40f);
        }
        
        public void Update(float deltaTime)
        {
            if (_currentFakePlayers <= 0) return;
            
            _curTimeToSpawnFake += deltaTime;
            if (_curTimeToSpawnFake < _minTimeToSpawnFake) return;
            _curTimeToSpawnFake = 0;
            _minTimeToSpawnFake = Random.Range(1.5f, 6f);
            _currentFakePlayers--;
            CarCount.AliveCars--;
        }
    }
}