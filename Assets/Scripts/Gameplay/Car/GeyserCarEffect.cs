using UnityEngine;

namespace CarsBattleRoyal
{
    public class GeyserCarEffect: MonoBehaviour
    {
        private DamageCarManager _damageCarManager;
        private DamageSoundManager _damageSoundManager;
        private Rigidbody _rigidBody;

        private float _damage;
        private int _ownerId;
        private AudioClip _explosionSound;
        private float _power;

        private void Awake()
        {
            _damageCarManager = GetComponent<DamageCarManager>();
            _damageSoundManager = GetComponent<DamageSoundManager>();
            _rigidBody = GetComponent<Rigidbody>();
        }

        public void UpdateEffect()
        {
            _damageCarManager.DamageHp(_damage * Time.deltaTime, _ownerId);
            _damageSoundManager.PlayDamageSound(_explosionSound);
            _rigidBody.AddForce(Vector3.up * _power, ForceMode.Force);
        }

        public void SetInfo(float damage, int ownerId, AudioClip explosionSound, float power)
        {
            _damage = damage;
            _ownerId = ownerId;
            _power = power;
            _explosionSound = explosionSound;
        }
    }
}