﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    [RequireComponent(typeof(FlipOver))]

    [RequireComponent(typeof(CarController))]
    public class CarLandingManager : MonoBehaviour
    {
        public bool Desanted;

        private Rigidbody _rigidbody;
        private FlipOver _flipOver;
        private CarController _car;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _flipOver = GetComponent<FlipOver>();
            _car = GetComponent<CarController>();
        }

        public void SoftLanding()
        {
            StartCoroutine(SoftLandingCoroutine());
        }

        public void SelfLanding()
        {
            Desanted = true;
            if (_car.CurrentCarType == ActiveCarType.LocalPlayer || _car.CurrentCarType == ActiveCarType.LocalAi)
            {
                _flipOver.enabled = true;
            }
        }

        private IEnumerator SoftLandingCoroutine()
        {
            var time = 1f;
            _rigidbody.velocity *= 0.5f;
            Destroy(Instantiate(InGamePrefabs.Instance.particles.stopFlyParticle, transform.position, Quaternion.identity), 2f);
            while (time > 0)
            {
                time -= Time.deltaTime;
                _rigidbody.velocity *= 0.9f;
                yield return new WaitForSeconds(Time.deltaTime);
            }

            Desanted = true;
            if (_car.CurrentCarType == ActiveCarType.LocalPlayer || _car.CurrentCarType == ActiveCarType.LocalAi)
            {
                _flipOver.enabled = true;
            }
        }

        private void OnCollisionEnter(Collision other)
        {
            if(Desanted) return;
            if (!other.collider.CompareTag(GameConstants.TerrainTag)) return;
            var carFlyController = GetComponentInChildren<CarFlyController>();
            if(carFlyController)
                carFlyController.SoftLanding();
        }
    }
}
