using System;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class CarJumper: MonoBehaviour
    {
        private Rigidbody _rigidbody;
        private ICarInput _carInput;
        
        public const float JumpCoolDown = 5f;
        public float LastJumpTime { get; private set; }

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _carInput = GetComponent<ICarInput>();
        }

        private void FixedUpdate()
        {
            if(LastJumpTime > 0)
                LastJumpTime -= Time.deltaTime;
            
            if (LastJumpTime <= 0 && _carInput.JumpInput)
            {
                LastJumpTime = JumpCoolDown;
                _rigidbody.AddForce(transform.up * 500, ForceMode.Acceleration);
            }
        }
    }
}