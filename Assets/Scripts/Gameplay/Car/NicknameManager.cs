using System;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class NicknameManager: MonoBehaviour
    {
        private void Awake()
        {
            if(GetComponent<CarController>().CurrentCarType == ActiveCarType.LocalPlayer)
                return;
            var gunManager = GetComponent<GunTransformManager>();
            Instantiate(InGamePrefabs.Instance.nicknamePrefab, transform).transform.position = gunManager.GetWeaponPoint().position + Vector3.up * 3;
        }
    }
}