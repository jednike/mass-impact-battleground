﻿using System;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class MiniMapManager : MonoBehaviour
    {
        private GameObject _miniMap;
        private CarController _carController;

        private void Awake()
        {
            _carController = GetComponent<CarController>();
            _carController.CarStateChanged += OnCarStateChanged;
            OnCarStateChanged(ActiveCarType.None, _carController.CurrentCarType);
        }
        private void OnCarStateChanged(ActiveCarType lastState, ActiveCarType newState)
        {
            if (newState != ActiveCarType.LocalPlayer) return;
            if(_miniMap)
                Destroy(_miniMap);
            
            _miniMap = Instantiate(InGamePrefabs.Instance.miniMapPrefab, null);
            _miniMap.GetComponent<FollowForTarget>().SetTarget(transform);
        }

        private void OnDestroy()
        {
            _carController.CarStateChanged -= OnCarStateChanged;
            Destroy(_miniMap);
        }
    }
}