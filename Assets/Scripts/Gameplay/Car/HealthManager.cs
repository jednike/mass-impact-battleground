﻿using System;
using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class HealthManager : MonoBehaviourPunCallbacks
    {
        public float MaxHp => _maxHp;
        [SerializeField] private float _maxHp = 100;
        public float Hp
        {
            get { return _hp; }
            set
            {
                var lastHp = _hp;
                _hp = Mathf.Clamp(value, 0, MaxHp);
                HpChanged(lastHp, _hp);
            }
        }
        private float _hp;
        public event Action<float, float> HpChanged = delegate(float oldHp, float newHp) {  };

        private void Start()
        {
            Hp = MaxHp;
        }
		
        public void RestoreHp(float restoreKoef)
        {
            Hp += restoreKoef * MaxHp;
        }
    }
}
