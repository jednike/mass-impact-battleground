﻿using System.Collections;
using EVP;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class FreezeCarEffect : MonoBehaviour
    {
        [SerializeField] private GameObject freezeParticle;

        private VehicleController _vehicleController;
        private Coroutine _freezeEffectCoroutine;
        private float _maxSpeed;

        private void Awake()
        {
            _vehicleController = GetComponent<VehicleController>();
            _maxSpeed = _vehicleController.maxDriveForce;
        }

        public void FreezeEffect()
        {
            freezeParticle.SetActive(true);
            _vehicleController.maxDriveForce = _maxSpeed / 2f;
            if (_freezeEffectCoroutine != null)
                StopCoroutine(_freezeEffectCoroutine);
            _freezeEffectCoroutine = StartCoroutine(FreezeCar());
        }

        private IEnumerator FreezeCar()
        {
            yield return new WaitForSeconds(3f);
            freezeParticle.SetActive(false);
            _vehicleController.maxDriveForce = _maxSpeed;
        }
    }
}