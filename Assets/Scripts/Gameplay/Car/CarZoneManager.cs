﻿using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
    [RequireComponent(typeof(CarController))]
    [DisallowMultipleComponent]
    public class CarZoneManager : MonoBehaviourPunCallbacks
    {
        public Point Zone
        {
            get => zone;
            set
            {
                if (zone == value)
                    return;
                zone = value;

                var localCar = CarController.Local;
                var inZone = false;
                if (localCar)
                {
                    var local = localCar.GetComponent<CarZoneManager>();
                    inZone = zone.x >= local.zone.x - GameConstants.ActiveDistance && zone.x <= local.zone.x + GameConstants.ActiveDistance && 
                             zone.y >= local.zone.y - GameConstants.ActiveDistance && zone.y <= local.zone.y + GameConstants.ActiveDistance;
                }
                _car.Active = inZone ? ActiveState.Active : ActiveState.NotActive;

                switch (_car.CurrentCarType)
                {
                    case ActiveCarType.LocalPlayer:
                        OnRealPlayerZoneChanged(zone, true);
                        photonView.RPC("ZoneChanged", RpcTarget.Others, zone);
                        break;
                    case ActiveCarType.LocalAi:
                        photonView.RPC("ZoneChanged", RpcTarget.Others, zone);
                        break;
                    case ActiveCarType.NetworkAi:
                        break;
                    case ActiveCarType.NetworkPlayer:
                        OnRealPlayerZoneChanged(zone, false);
                        break;
                }
            }
        }
        [SerializeField]private Point zone;

        private CarController _car;
        
        private void Awake()
        {
            _car = GetComponent<CarController>();
        }

        private void Update()
        {
            if (_car.CurrentCarType != ActiveCarType.LocalAi && _car.CurrentCarType != ActiveCarType.LocalPlayer) return;
            DetectZone();
        }

        private void DetectZone()
        {
            var position = transform.position;
            var indexByX = ((int) position.x + 1100) / 55;
            var indexByZ = ((int) position.z + 1100) / 55;

            Zone = new Point(indexByZ, indexByX);
        }

        private void OnRealPlayerZoneChanged(Point newZone, bool localPlayer)
        {
            if(_car.CurrentCarType == ActiveCarType.LocalPlayer)
                return;
            var inZone = newZone.x >= zone.x - GameConstants.ActiveDistance && newZone.x <= zone.x + GameConstants.ActiveDistance && newZone.y >= zone.y - GameConstants.ActiveDistance &&
                         newZone.y <= zone.y + GameConstants.ActiveDistance;
			
            _car.Active = inZone ? ActiveState.Active : ActiveState.NotActive;
        }
        
        [PunRPC]
        public void ZoneChanged(Point newZone)
        {
            Zone = newZone;
        }
    }
}
