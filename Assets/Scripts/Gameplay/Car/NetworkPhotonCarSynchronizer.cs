﻿using System.Collections;
using EVP;
using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class NetworkPhotonCarSynchronizer : MonoBehaviourPunCallbacks
    {
        private CarController _car;
        private HealthManager _healthManager;
        private Rigidbody _rigidBody;

        private Vector3 _correctPlayerPos;
        private Quaternion _correctPlayerRot;
        private Vector3 _currentVelocity;
        private float _updateTime;

        private Transform _transform;
        
        private void Awake()
        {
            _car = GetComponent<CarController>();
            _healthManager = GetComponent<HealthManager>();
            _rigidBody = GetComponent<Rigidbody>();
            
            _transform = transform;
        }

        private void Start()
        {
            _correctPlayerPos = _transform.position;
            _correctPlayerRot = _transform.rotation;
            _currentVelocity = _rigidBody.velocity;
        }

        private void FixedUpdate()
        {
            if (_car.Destroyed)
                return;

            var projectedPosition = _correctPlayerPos + _currentVelocity * (Time.time - _updateTime);
            _rigidBody.velocity = _currentVelocity;
            _transform.position = Vector3.Lerp(_transform.position, projectedPosition, Time.deltaTime * 4);
            _transform.rotation = Quaternion.Lerp(_transform.rotation, _correctPlayerRot, Time.deltaTime * 4);
        }

        [PunRPC]
        public void Synchronize(float hp, Vector3 playerPos, Quaternion playerRot, Vector3 velocity)
        {
            _healthManager.Hp = hp;
            _correctPlayerPos = playerPos;
            _correctPlayerRot = playerRot;
            _currentVelocity = velocity;

            _updateTime = Time.time;
        }
    }
}
