using UnityEngine;

namespace CarsBattleRoyal
{
    public interface IPathFinder
    {
        Vector3 ClosestPosition { get; set; }
        void GetPath();
        Vector3 GetCurrentTargetPosition();
    }
}