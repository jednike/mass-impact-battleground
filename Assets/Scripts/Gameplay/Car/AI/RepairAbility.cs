namespace CarsBattleRoyal
{
    public class RepairAbility: AIAbility
    {
        private readonly HealthManager _healthManager;
        private readonly WearObjectManager _wearObjectManager;

        public RepairAbility(HealthManager healthManager, WearObjectManager wearObjectManager)
        {
            _healthManager = healthManager;
            _wearObjectManager = wearObjectManager;
        }

        public void UseAbility()
        {
            if (!(_healthManager.Hp <= _healthManager.MaxHp * 0.6f)) return;
			
            if(_wearObjectManager.ExistWeapon() && _wearObjectManager.CurrentWeapon.WeaponType == ObjectsType.Armor)
                _wearObjectManager.CurrentWeapon.UseObject();
        }
    }
}