﻿using EVP;
using UnityEngine;
using System.Collections.Generic;

namespace CarsBattleRoyal
{
	public enum IntelligenceState
	{
		None,
		FindWeapon,
		MoveToWeapon,
		FindCar,
		MoveToCar
	}

	public class AIBehaviour : MonoBehaviour
	{
		public static int TotalBots = 0;
		public static int ActiveBots = 0;
		public const int MinActiveBots = 5;
		
		private readonly List<AIAbility> _abilities = new List<AIAbility>();

		public void Awake()
		{
			var healthManager = GetComponent<HealthManager>();
			var vehicleController = GetComponent<VehicleController>();
			var wearObjectManager = GetComponent<WearObjectManager>();
			_abilities.Add(new MinesAbility(vehicleController, wearObjectManager));
			_abilities.Add(new RepairAbility(healthManager, wearObjectManager));

			TotalBots++;
		}

		private void OnEnable()
		{
			ActiveBots++;
		}
		private void OnDisable()
		{
			ActiveBots--;
		}

		private void OnDestroy()
		{
			TotalBots--;
		}

		private void Update()
		{
			foreach (var ability in _abilities)
			{
				ability.UseAbility();
			}
		}

	}
}