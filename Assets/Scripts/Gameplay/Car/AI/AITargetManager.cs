using UnityEngine;

namespace CarsBattleRoyal
{
    public class AITargetManager: ITargetManager
    {
        private IntelligenceState State
        {
            get => _state;
            set
            {
                if(_state == value)
                    return;
                _state = value;
				
                switch (_state)
                {
                    case IntelligenceState.FindWeapon:
                        _curTimeToChangeTarget = 0;
                        ClosestObject = CarsInGameController.FindClosestWeapon(_transform.position, Mathf.Infinity);
                        State = IntelligenceState.MoveToWeapon;
                        break;
                    case IntelligenceState.FindCar:
                        _curTimeToChangeTarget = 0;
                        ClosestObject = CarsInGameController.FindClosest(_carController, Mathf.Infinity, _dangerZone.DangerZonePosition, new []{ActiveCarType.LocalPlayer, ActiveCarType.NetworkPlayer});
                        State = IntelligenceState.MoveToCar;
                        break;
                }
            }
        }
        private IntelligenceState _state;

        private const float TimeToChangeTarget = 10f;
        private float _curTimeToChangeTarget;

        public Transform ClosestObject { get; private set; }
        public Vector3 ClosestPosition { get; private set; }

        private readonly DangerZoneController _dangerZone;
        private readonly CarController _carController;
        private readonly Transform _transform;
        private readonly GunManager _gunManager;

        public AITargetManager(GunManager gunManager, Transform transform, CarController carController, DangerZoneController dangerZone)
        {
            _gunManager = gunManager;
            _transform = transform;
            _carController = carController;
            _dangerZone = dangerZone;
            _gunManager.OnChangeWeapon += OnChangeWeapon;
            
            State = IntelligenceState.FindWeapon;
        }

        public void ChangeTarget()
        {
            switch (State)
            {
                case IntelligenceState.MoveToWeapon:
                    if (!ClosestObject || !ClosestObject.CompareTag(GameConstants.WeaponTag))
                        State = IntelligenceState.FindWeapon;
                    else
                        ClosestPosition = ClosestObject.position;
                    break;
                case IntelligenceState.MoveToCar:
                    if (!ClosestObject || !ClosestObject.CompareTag(GameConstants.CarTag))
                        State = IntelligenceState.FindCar;
                    else
                    {
                        var distance = Vector3.Distance(ClosestObject.position, _transform.position); ;
                        if(distance < _gunManager.CurrentWeapon.MaxFindDistance / 2 && _gunManager.CurrentWeapon.InAttack)
                            ClosestPosition = _transform.position;
                        else
                            ClosestPosition = ClosestObject.position;
                    }
                    break;
            }
			
            _curTimeToChangeTarget += Time.deltaTime;
            if (_curTimeToChangeTarget < TimeToChangeTarget) return;
            _curTimeToChangeTarget = 0;
            ClosestObject = null;
        }

        private void OnChangeWeapon(WeaponType weaponType)
        {
            State = weaponType != WeaponType.None ? IntelligenceState.FindCar : IntelligenceState.FindWeapon;
        }

        public void Destroy()
        {
            if(_gunManager)
                _gunManager.OnChangeWeapon -= OnChangeWeapon;
        }
    }
}