using EVP;

namespace CarsBattleRoyal
{
    public class MinesAbility: AIAbility
    {
        private readonly VehicleController _vehicleController;
        private readonly WearObjectManager _wearObjectManager;
        public MinesAbility(VehicleController vehicleController, WearObjectManager wearObjectManager)
        {
            _vehicleController = vehicleController;
            _wearObjectManager = wearObjectManager;
        }

        public void UseAbility()
        {
            if (_vehicleController.speed < 10) return;
			
            if(_wearObjectManager.ExistWeapon() && _wearObjectManager.CurrentWeapon.WeaponType == ObjectsType.Mines)
                _wearObjectManager.CurrentWeapon.UseObject();
        }
    }
}