using UnityEngine;
using UnityEngine.AI;

namespace CarsBattleRoyal
{
    public class PathFinder: IPathFinder
    {
        private const float TimeToUpdatePath = 3f;
        private float _curTimeToUpdatePath = 3f;
        private int _currentIndexPath;
        private NavMeshPath _path;
        
        private readonly Transform _transform;

        public Vector3 ClosestPosition { get; set; }

        public PathFinder(Transform transform)
        {
            _transform = transform;
        }

        public void GetPath()
        {
            _curTimeToUpdatePath += Time.deltaTime;
            if (_curTimeToUpdatePath < TimeToUpdatePath) return;
            _curTimeToUpdatePath = 0;
            _currentIndexPath = 0;

            if (Vector3.Distance(_transform.position, ClosestPosition) > 20)
            {
                _path = new NavMeshPath();
                NavMesh.CalculatePath(_transform.position, ClosestPosition, NavMesh.AllAreas, _path);
            }
            else
            {
                _path = null;
            }
        }

        public Vector3 GetCurrentTargetPosition()
        {
            if (_path?.corners == null || _path.corners.Length == 0 || _path.corners.Length <= _currentIndexPath)
                return ClosestPosition;
			
            if (_currentIndexPath + 1 < _path.corners.Length &&
                Vector3.Distance(_transform.position, _path.corners[_currentIndexPath]) < 10)
            {
                _currentIndexPath++;
            }

            return _path.corners[_currentIndexPath];
        }
    }
}