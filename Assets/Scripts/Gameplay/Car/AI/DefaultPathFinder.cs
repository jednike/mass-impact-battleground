using UnityEngine;

namespace CarsBattleRoyal
{
    public class DefaultPathFinder: IPathFinder
    {
        public Vector3 ClosestPosition { get; set; }
        
        public void GetPath()
        {
        }
        public Vector3 GetCurrentTargetPosition()
        {
            return ClosestPosition;
        }
    }
}