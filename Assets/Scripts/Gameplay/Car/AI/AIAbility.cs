namespace CarsBattleRoyal
{
    public interface AIAbility
    {
        void UseAbility();
    }
}