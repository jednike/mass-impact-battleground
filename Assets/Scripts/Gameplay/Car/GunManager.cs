using System;
using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class GunManager: DefaultWeaponManager
    {
        public event Action<WeaponType> OnChangeWeapon = delegate {  };
        
        public WeaponManager CurrentWeapon => _currentWeapon;
        private WeaponManager _currentWeapon;

        protected override string EventName => "ThrowGun";

        public override void InstallWeapon(PickUpObject weaponManager)
        {
            _currentWeapon = (WeaponManager) weaponManager;

            WeaponTransform = weaponManager.transform;
            WeaponTransform.SetParent(GunTransformManager.GetWeaponPoint());
            WeaponTransform.localPosition = GunTransformManager.GetWeaponPoint(_currentWeapon.WeaponType);
            WeaponTransform.localRotation = Quaternion.identity;
			
            OnChangeWeapon(_currentWeapon.WeaponType);
        }

        public override bool ExistWeapon()
        {
            return _currentWeapon != null;
        }
        public override void RemoveWeapon(PickUpObject weaponManager)
        {
            if (_currentWeapon != weaponManager)
            {
                Debug.LogError("Try destroy not current weapon");
                return;
            }
            _currentWeapon = null;
            OnChangeWeapon(WeaponType.None);
        }

        [PunRPC]
        public void ThrowGun()
        {
            if(_currentWeapon)
                _currentWeapon.ThrowWeapon();
        }
    }
}