﻿using System;
using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class DamageCarManager : MonoBehaviour
    {
        public static event Action<int> OnDamageLocalPlayer = delegate {  };
        
        private HealthManager _healthManager;
        private CarController _car;
		
        public int KillerId => _killerId;
        private int _killerId;
        
        private float _accumulatedDamage;

        private float _timeToShowDamage = 0.5f;
        private float _currentTime;
        
        private void Awake()
        {
            _healthManager = GetComponent<HealthManager>();
            _car = GetComponent<CarController>();
        }
        
        public void DamageHp(float damage, int killerId)
        {
            _healthManager.Hp -= damage;
            _killerId = killerId;
            if (_car.CurrentCarType == ActiveCarType.LocalPlayer)
                OnDamageLocalPlayer(killerId);
            else if (PhotonNetwork.LocalPlayer.ActorNumber == killerId)
            {
                _accumulatedDamage += damage;
                _currentTime += Time.deltaTime;
                if (_currentTime > _timeToShowDamage || _accumulatedDamage > 5)
                {
                    DamagePointsController.CreateFloatingText(((int)_accumulatedDamage).ToString(), transform);
                    _currentTime = 0;
                    _accumulatedDamage = 0;
                }
            }
        }
    }
}
