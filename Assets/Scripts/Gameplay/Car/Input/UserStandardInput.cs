﻿using System;
using EVP;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace CarsBattleRoyal
{
    public class UserStandardInput: CarInput
    {
        private InputSettings _inputSettings;
        public override void Init()
        {
            _inputSettings = InGamePrefabs.Instance.inputSettings;
        }

        public override void DoUpdate()
        {
            
        }

        public override void DoFixedUpdate(float speed)
        {
            var steerInput = Mathf.Clamp(GetAxis(_inputSettings.steerAxis), -1.0f, 1.0f);
            var handbrakeInput = Mathf.Clamp01(GetAxis(_inputSettings.handbrakeAxis));

            float forwardInput;
            float reverseInput;

            if (_inputSettings.throttleAndBrakeInput == ThrottleAndBrakeInput.SeparateAxes)
            {
                forwardInput = Mathf.Clamp01(GetAxis(_inputSettings.throttleAxis));
                reverseInput = Mathf.Clamp01(GetAxis(_inputSettings.brakeAxis));
            }
            else
            {
                if (!Scores.Instance.AutoAccelerate)
                {
                    forwardInput = Mathf.Clamp01(GetAxis(_inputSettings.throttleAndBrakeAxis));
                    reverseInput = Mathf.Clamp01(-GetAxis(_inputSettings.throttleAndBrakeAxis));
                }
                else
                {
                    reverseInput = Mathf.Clamp01(-GetAxis(_inputSettings.throttleAndBrakeAxis));
                    forwardInput = Math.Abs(reverseInput) < 0.03f ? 1 : 0;
                }
            }
            
            var throttleInput = 0.0f;
            var brakeInput = 0.0f;

            if (_inputSettings.continuousForwardAndReverse)
            {
                const float minSpeed = 0.1f;
                const float minInput = 0.1f;

                if (speed > minSpeed)
                {
                    throttleInput = forwardInput;
                    brakeInput = reverseInput;
                }
                else
                {
                    if (reverseInput > minInput)
                    {
                        throttleInput = -reverseInput;
                        brakeInput = 0.0f;
                    }
                    else if (forwardInput > minInput)
                    {
                        if (speed < -minSpeed)
                        {
                            throttleInput = 0.0f;
                            brakeInput = forwardInput;
                        }
                        else
                        {
                            throttleInput = forwardInput;
                            brakeInput = 0;
                        }
                    }
                }
            }
            else
            {
                var reverse = GetKey(KeyCode.LeftControl) || GetKey(KeyCode.RightControl);
                if (!reverse)
                {
                    throttleInput = forwardInput;
                    brakeInput = reverseInput;
                }
                else
                {
                    throttleInput = -reverseInput;
                    brakeInput = 0;
                }
            }

            JumpInput = GetKeyDown(KeyCode.LeftShift);
            SteerInput = steerInput;
            ThrottleInput = throttleInput;
            BrakeInput = brakeInput;
            HandbrakeInput = handbrakeInput;
            HandbrakeInput = handbrakeInput;
        }

        private static float GetAxis(string axisName)
        {
            var input = Input.GetAxis(axisName);
            return Math.Abs(input) < 0.01f ? CrossPlatformInputManager.GetAxis(axisName) : input;
        }

        private static bool GetKey(KeyCode keyName)
        {
            var input = Input.GetKey(keyName);
            return input || CrossPlatformInputManager.GetButton(keyName.ToString());
        }

        private static bool GetKeyDown(KeyCode keyName)
        {
            var input = Input.GetKeyDown(keyName);
            return input || CrossPlatformInputManager.GetButtonDown(keyName.ToString());
        }
    }
}