using System;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class AIInput: CarInput
    {
        private float _throttleAndBrakeAxis = 1;
        private float _handbrakeAxis;

        private float _steerInput;
        private float _forwardInput;
        private float _rvInput;
        private float _bInput;
        private Vector3 _steerVector;

        private float _flag;
		
        private bool _reversing;
        private float _reversCounter;
        private const float WaitToReverse = 1.0f;
        private const float ReversTime = 3.5f;

        private const float SensorLength = 60;
        private const float FrontSensorStartPoint = 1;
        private const float FrontSensorSideDist = 8;
        private const float FrontSensorsAngle = 30;
        private const float SideWaySensorLength = 8;
        
        private Transform _point;
        private Transform _transform;
        private Rigidbody _rigidBody;
        private IPathFinder _pathFinder;
        private ITargetManager _targetManager;
        
        public override void Init()
        {
	        _transform = transform;
	        var carController = GetComponent<CarController>();
	        var dangerZone = FindObjectOfType<DangerZoneController>();
	        var gunManager = GetComponent<GunManager>();
	        _rigidBody = GetComponent<Rigidbody>();
	        _targetManager = new AITargetManager(gunManager, _transform, carController, dangerZone);
	        
	        _point = GetComponent<GunTransformManager>().GetWeaponPoint();

	        _pathFinder = new PathFinder(transform);
        }

        public override void DoUpdate()
        {
	        Sensors();
	        _targetManager.ChangeTarget();
	        _pathFinder.ClosestPosition = _targetManager.ClosestPosition;
	        _pathFinder.GetPath();
        }

        public override void DoFixedUpdate(float speed)
        {
            if (_targetManager.ClosestObject)
            {
                _steerVector = _transform.InverseTransformPoint(_pathFinder.GetCurrentTargetPosition());
            }
            else
            {
                _handbrakeAxis = 0;
            }

            if (Math.Abs(_flag) < 0.01f)
            {
                _steerInput = 2f * (_steerVector.x / (Math.Abs(_steerVector.magnitude) < 0.01 ? 0.01f : _steerVector.magnitude));
            }
            else
            {
                _steerInput = -_flag;
            }


            var fwInput = Mathf.Clamp01(_throttleAndBrakeAxis);
            var reverseInput = Mathf.Clamp01(-_throttleAndBrakeAxis);
            var handbrakeInput = Mathf.Clamp01(_handbrakeAxis);
            var throttleInput = 0.0f;
            var brakeInput = 0.0f;

            const float minSpeed = 0.1f;
            const float minInput = 0.1f;

            if (speed > minSpeed)
            {
                throttleInput = fwInput;
                brakeInput = reverseInput;
            }
            else
            {
                if (reverseInput > minInput)
                {
                    throttleInput = -reverseInput;
                    brakeInput = 0.0f;
                }
                else if (fwInput > minInput)
                {
                    if (speed < -minSpeed)
                    {
                        throttleInput = 0.0f;
                        brakeInput = fwInput;
                    }
                    else
                    {
                        throttleInput = fwInput;
                        brakeInput = 0;
                    }
                }
            }

            SteerInput = _steerInput;
            ThrottleInput = throttleInput;
            BrakeInput = brakeInput;
            HandbrakeInput = handbrakeInput;
        }

		private void Sensors()
		{
			_flag = 0;
			RaycastHit hit;
			var up = _transform.up;
			var forward = _transform.forward;
			
			var rightAngle = Quaternion.AngleAxis(FrontSensorsAngle, up) * forward;
			var leftAngle = Quaternion.AngleAxis(-FrontSensorsAngle, up) * forward;

			var pos = _point.position;
			pos += _point.forward * FrontSensorStartPoint;

			//BRAKING SENSOR 
			if (Physics.Raycast(pos, forward, out hit, SensorLength - 5))
			{

				if (!hit.transform.CompareTag(GameConstants.ColliderTag) && !hit.transform.CompareTag(GameConstants.TerrainTag) &&
				    !hit.transform.CompareTag(GameConstants.CarTag))
				{
					_throttleAndBrakeAxis = 1f;
					Debug.DrawLine(pos, hit.point, Color.red);
				}
				else
				{
					_throttleAndBrakeAxis = 1f;
				}
			}

			if (Physics.Raycast(pos, forward, out hit, SensorLength * 0.2f))
			{
				if (!hit.transform.CompareTag(GameConstants.ColliderTag) && !hit.transform.CompareTag(GameConstants.TerrainTag) &&
				    !hit.transform.CompareTag(GameConstants.CarTag))
				{
					_throttleAndBrakeAxis = 0.2f;
					Debug.DrawLine(pos, hit.point, Color.black);
				}
				else
				{
					_throttleAndBrakeAxis = 1f;
				}

			}

			//Front Straight Right Sensor 
			pos += _point.right * FrontSensorSideDist;

			if (Physics.Raycast(pos, forward, out hit, SensorLength * 0.7f))
			{
				if (!hit.transform.CompareTag(GameConstants.ColliderTag) && !hit.transform.CompareTag(GameConstants.TerrainTag) &&
				    !hit.transform.CompareTag(GameConstants.CarTag))
				{
					_flag += 0.5f;

					Debug.DrawLine(pos, hit.point, Color.yellow);
				}

			}

			if (Physics.Raycast(pos, rightAngle, out hit, SensorLength * 0.7f))
			{
				if (!hit.transform.CompareTag(GameConstants.ColliderTag) && !hit.transform.CompareTag(GameConstants.TerrainTag) &&
				    !hit.transform.CompareTag(GameConstants.CarTag))
				{
					_flag += 0.5f;
					Debug.DrawLine(pos, hit.point, Color.white);
				}
			}

			//Front Straight left Sensor 
			pos = _point.position;
			pos += _point.forward * FrontSensorStartPoint;
			pos -= _point.right * FrontSensorSideDist;

			if (Physics.Raycast(pos, forward, out hit, SensorLength * 0.8f))
			{
				if (!hit.transform.CompareTag(GameConstants.ColliderTag) && !hit.transform.CompareTag(GameConstants.TerrainTag) &&
				    !hit.transform.CompareTag(GameConstants.CarTag))
				{
					_flag -= 0.5f;

					Debug.DrawLine(pos, hit.point, Color.white);
				}

			}

			if (Physics.Raycast(pos, leftAngle, out hit, SensorLength * 0.8f))
			{
				if (!hit.transform.CompareTag(GameConstants.ColliderTag) && !hit.transform.CompareTag(GameConstants.TerrainTag) &&
				    !hit.transform.CompareTag(GameConstants.CarTag))
				{
					_flag -= 0.5f;
					Debug.DrawLine(pos, hit.point, Color.white);
				}

			}

			if (_rigidBody.velocity.magnitude < 1 && !_reversing)
			{
				_reversCounter += Time.deltaTime;
				if (_reversCounter >= WaitToReverse)
				{
					_reversCounter = 0;
					_reversing = true;

				}
			}
			else if (!_reversing)
			{
				_reversCounter = 0;
				_throttleAndBrakeAxis = 1;
			}

			if (!_reversing) return;
			_throttleAndBrakeAxis = -1;
			_reversCounter += Time.deltaTime;
			
			if (_reversCounter < ReversTime) return;
			_reversCounter = 0;
			_reversing = false;
		}

		private void OnDestroy()
		{
			_targetManager.Destroy();
		}
    }
}