using UnityEngine;

namespace CarsBattleRoyal
{
    public interface ITargetManager
    {
        Transform ClosestObject { get; }
        Vector3 ClosestPosition { get; }
        void ChangeTarget();
        void Destroy();
    }
}