namespace CarsBattleRoyal
{
    public interface ICarInput
    {
        float SteerInput { get; }
        float BrakeInput { get; }
        float HandbrakeInput { get; }
        float ThrottleInput { get; }
        bool JumpInput { get; }

        void Init();
        void DoUpdate();
        void DoFixedUpdate(float speed);
    }
}