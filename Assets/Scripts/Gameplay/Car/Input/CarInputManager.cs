using System;
using EVP;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class CarInputManager: MonoBehaviour
    {
        private VehicleController _target;
        private CarInput _input;
        private CarController _carController;

        private void Awake()
        {
            _target = GetComponent<VehicleController>();
            
            _carController = GetComponent<CarController>();
            _carController.CarStateChanged += OnCarTypeChanged;
            OnCarTypeChanged(ActiveCarType.None, _carController.CurrentCarType);

            gameObject.AddComponent<CarJumper>();
        }

        private void OnCarTypeChanged(ActiveCarType oldType, ActiveCarType newType)
        {
            if(oldType == newType)
                return;

            Destroy(_input);
            
            switch (newType)
            {
                case ActiveCarType.None:
                    break;
                case ActiveCarType.LocalPlayer:
                    _input = gameObject.AddComponent<UserStandardInput>();
                    break;
                case ActiveCarType.LocalAi:
                    _input = gameObject.AddComponent<AIInput>();
                    break;
                case ActiveCarType.NetworkAi:
                case ActiveCarType.NetworkPlayer:
                    _input = gameObject.AddComponent<NetworkInput>();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            _input.Init();
        }

        private void Update()
        {
            _input.DoUpdate();
        }

        private void FixedUpdate()
        {
            _input.DoFixedUpdate(_target.speed);
            
            _target.steerInput = _input.SteerInput;
            _target.throttleInput = _input.ThrottleInput;
            _target.brakeInput = _input.BrakeInput;
            _target.handbrakeInput = _input.HandbrakeInput;
        }

        private void OnDestroy()
        {
            _carController.CarStateChanged -= OnCarTypeChanged;
        }
    }
}