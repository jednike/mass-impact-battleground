using Photon.Pun;

namespace CarsBattleRoyal
{
    public class NetworkInput: CarInput
    {
        public override void Init()
        {
            
        }

        public override void DoUpdate()
        {
        }

        public override void DoFixedUpdate(float speed)
        {
        }
        
        [PunRPC]
        public void SynchronizeInput(float throttle, float brake, float steer, float handbrake)
        {
            ThrottleInput = throttle;
            BrakeInput = brake;
            SteerInput = steer;
            HandbrakeInput = handbrake;
        }
    }
}