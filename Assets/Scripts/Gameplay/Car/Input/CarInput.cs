using UnityEngine;

namespace CarsBattleRoyal
{
    public abstract class CarInput: MonoBehaviour, ICarInput
    {
        public float SteerInput { get; protected set; }
        public float BrakeInput { get; protected set; }
        public float HandbrakeInput { get; protected set; }
        public float ThrottleInput { get; protected set; }
        public bool JumpInput { get; protected set; }

        public abstract void Init();
        public abstract void DoUpdate();
        public abstract void DoFixedUpdate(float speed);
    }
}