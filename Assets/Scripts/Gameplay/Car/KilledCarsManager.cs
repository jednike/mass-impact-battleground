﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class KilledCarsManager : MonoBehaviour
    {
        public int KilledCars
        {
            get => _killedCars;
            set
            {
                if(_killedCars == value)
                    return;
                _killedCars = value;
                KillsChanged(_car, _killedCars);
                if (_car.CurrentCarType == ActiveCarType.LocalPlayer)
                {
                    Scores.Instance.Killed++;
                }
            }
        }
        private int _killedCars;
        public static event Action<CarController, int> KillsChanged = delegate {  };

        private CarController _car;

        private void Awake()
        {
            _car = GetComponent<CarController>();
        }
    }
}
