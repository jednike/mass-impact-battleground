﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Serialization;

namespace CarsBattleRoyal
{
	[Serializable]
	public struct WeaponPoint
	{
		public WeaponType WeaponName;
		public Vector3 OffsetVector;
	}
	public class GunTransformManager : MonoBehaviour
	{
		[SerializeField] private Transform baseWeaponPoint;
		[SerializeField] private WeaponPoint[] weaponPoints;
		
		public Transform GetWeaponPoint()
		{
			return baseWeaponPoint;
		}
		public Vector3 GetWeaponPoint(WeaponType weaponName)
		{
			foreach (var weaponPoint in weaponPoints)
			{
				if (weaponPoint.WeaponName == weaponName)
				{
					return weaponPoint.OffsetVector;
				}
			}
			return Vector3.zero;
		}
	}
}