﻿using System.Collections;
using EVP;
using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class LocalPhotonCarSynchronizer : MonoBehaviourPunCallbacks
    {
        private CarController _carController;
        private HealthManager _healthManager;
        private VehicleController _vehicleController;
        private Rigidbody _rigidBody;
        private CarZoneManager _zone;

        private void Awake()
        {
            _carController = GetComponent<CarController>();
            _vehicleController = GetComponent<VehicleController>();
            _healthManager = GetComponent<HealthManager>();
            _rigidBody = GetComponent<Rigidbody>();
            _zone = GetComponent<CarZoneManager>();

            StartCoroutine(Synchronize());
        }

        private IEnumerator Synchronize()
        {
            while(!_carController.Destroyed && PhotonNetwork.IsConnected)
            {
                var clients = CarsInGameController.GetCarsInMyZone(_zone.Zone);
                foreach (var client in clients)
                {
                    if(client.photonView.IsMine)
                        continue;
                    var localTransform = transform;
                    photonView.RPC("Synchronize", client.photonView.Owner, 
                        _healthManager.Hp, 
                        localTransform.position,
                        localTransform.rotation,
                        _rigidBody.velocity
                    );
                    photonView.RPC("SynchronizeInput", client.photonView.Owner, 
                        _vehicleController.throttleInput,
                        _vehicleController.brakeInput,
                        _vehicleController.steerInput,
                        _vehicleController.handbrakeInput
                    );
                }
                yield return new WaitForSecondsRealtime(GameConstants.SynchronizeTime);
            }			
        }
    }
}