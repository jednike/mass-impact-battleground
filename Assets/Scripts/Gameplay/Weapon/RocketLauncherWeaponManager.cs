﻿using UnityEngine;

namespace CarsBattleRoyal
{
    public class RocketLauncherWeaponManager : RotatingWeaponManager {
        
        [SerializeField] private GameObject _rocketStartParticle;
        [SerializeField] private Transform _startRocketPosition;
        
        [SerializeField] private GameObject _rocketPrefab;
        
        [SerializeField] private Transform _crosshair;

        private Vector3 _minSize = new Vector3(0.02f, 0.02f, 0.02f);
        private Vector3 _maxSize = new Vector3(1, 1, 1);
        
        private Transform _lastTarget;

        protected override void UpgradeCharacteristics()
        {
            base.UpgradeCharacteristics();
            _maxReloadTime /= UpgradeValueByType(WeaponLevel);
        }

        protected override void Init()
        {
            base.Init();
            ReloadTime = _maxReloadTime;
        }
        
        protected override bool CheckOnAttackAvailable()
        {
            RaycastHit hit;
            if (!RaycastCar(out hit))
            {
                if (OwnerCarController.CurrentCarType == ActiveCarType.LocalPlayer)
                    _crosshair.gameObject.SetActive(false);
                return false;
            }

            if (_lastTarget == CurrentTargetCar)
            {
                ReloadTime -= Time.deltaTime;
            }
            else
            {
                ReloadTime = _maxReloadTime;
                _lastTarget = CurrentTargetCar;
            }

            if (OwnerCarController.CurrentCarType == ActiveCarType.LocalPlayer)
            {
                _crosshair.gameObject.SetActive(true);
                _crosshair.localScale = Vector3.Lerp(_minSize, _maxSize, ReloadTime/_maxReloadTime);
                _crosshair.position = hit.point;
            }
            
            Attack();
            
            return true;
        }

        public override void ThrowWeapon()
        {
            base.ThrowWeapon();
            _crosshair.gameObject.SetActive(false);
            if(Source.isPlaying)
                Source.Stop();
        }
        
        protected void Attack()
        {
            if(ReloadTime > 0)
                return;
            
            if (OwnerCarController.CurrentCarType == ActiveCarType.LocalAi ||
                OwnerCarController.CurrentCarType == ActiveCarType.LocalPlayer)
            {
                Bullets -= 1;
            }
            ReloadTime = _maxReloadTime;

            Source.PlayOneShot(AttackSound);
            Destroy(Instantiate(_rocketStartParticle, _startRocketPosition.position, _startRocketPosition.rotation), 0.3f);
            Instantiate(_rocketPrefab, _startRocketPosition.position, _startRocketPosition.rotation).GetComponent<RocketController>().SetTarget(_lastTarget, WeaponLevel, OwnerCarController);
        }
    }
}
