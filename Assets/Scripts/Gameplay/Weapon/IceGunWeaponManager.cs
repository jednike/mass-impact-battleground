﻿using UnityEngine;

namespace CarsBattleRoyal
{
    public class IceGunWeaponManager : RotatingWeaponManager
    {
        [SerializeField] private ParticleSystem[] _gunParticles;

        protected override bool CheckOnAttackAvailable()
        {
            if (!CurrentTargetCar)
            {
                Utils.EnableAttacksElements(_gunParticles, Source, false);
                return false;
            }
            
            RaycastHit hit;
            if (!RaycastCar(out hit))
            {
                Utils.EnableAttacksElements(_gunParticles, Source, false);
                return false;
            }
            
            Attack();
            
            return true;
        }
        
        public override void ThrowWeapon()
        {
            base.ThrowWeapon();
            Utils.EnableAttacksElements(_gunParticles, Source, false);
        }

        protected void Attack()
        {
            var enemyCar = CurrentTargetCar.GetComponent<CarController>();
            if (!enemyCar)
                return;

            ReloadTime -= Time.deltaTime;
            if (ReloadTime > 0) return;
            ReloadTime = _maxReloadTime;

            Utils.EnableAttacksElements(_gunParticles, Source, true);

            enemyCar.GetComponent<DamageCarManager>().DamageHp(DamagePerAttack, OwnerCarController.OwnerId);
            enemyCar.GetComponent<DamageSoundManager>().PlayDamageSound(CollissionSound);
            enemyCar.GetComponent<FreezeCarEffect>().FreezeEffect();
                
            if (OwnerCarController.CurrentCarType == ActiveCarType.LocalAi ||
                OwnerCarController.CurrentCarType == ActiveCarType.LocalPlayer)
            {
                Bullets -= 1;
            }
        }
    }
}
