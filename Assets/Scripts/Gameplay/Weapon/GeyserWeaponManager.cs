﻿using UnityEngine;
using UnityEngine.Serialization;

namespace CarsBattleRoyal
{
    public class GeyserWeaponManager : RotatingWeaponManager {
                
        [SerializeField] private GameObject geyserPrefab;
        [SerializeField] private ParticleSystem attackParticles;
        
        [SerializeField] private Transform crossHair;

        private readonly Vector3 _minSize = new Vector3(0.02f, 0.02f, 0.02f);
        private readonly Vector3 _maxSize = new Vector3(1, 1, 1);
        
        private Transform _lastTarget;

        protected override void UpgradeCharacteristics()
        {
            base.UpgradeCharacteristics();
            _maxReloadTime /= UpgradeValueByType(WeaponLevel);
        }

        protected override void Init()
        {
            base.Init();
            ReloadTime = _maxReloadTime;
        }
        
        protected override bool CheckOnAttackAvailable()
        {
            if (!RaycastCar(out var hit))
            {
                if (OwnerCarController.CurrentCarType == ActiveCarType.LocalPlayer)
                    crossHair.gameObject.SetActive(false);
                return false;
            }

            if (_lastTarget == CurrentTargetCar)
            {
                ReloadTime -= Time.deltaTime;
            }
            else
            {
                ReloadTime = _maxReloadTime;
                _lastTarget = CurrentTargetCar;
            }

            if (OwnerCarController.CurrentCarType == ActiveCarType.LocalPlayer)
            {
                crossHair.gameObject.SetActive(true);
                crossHair.localScale = Vector3.Lerp(_minSize, _maxSize, ReloadTime/_maxReloadTime);
                crossHair.position = hit.point;
            }
            
            Attack(hit.point);
            
            return true;
        }

        public override void ThrowWeapon()
        {
            base.ThrowWeapon();
            crossHair.gameObject.SetActive(false);
            if(Source.isPlaying)
                Source.Stop();
        }

        private void Attack(Vector3 pos)
        {
            if(ReloadTime > 0)
                return;
            
            if (OwnerCarController.CurrentCarType == ActiveCarType.LocalAi ||
                OwnerCarController.CurrentCarType == ActiveCarType.LocalPlayer)
            {
                Bullets -= 1;
            }
            ReloadTime = _maxReloadTime;

            Source.PlayOneShot(AttackSound);
            attackParticles.Play();

            var raycasts = Physics.RaycastAll(pos + 1 * Vector3.up, Vector3.down, 100);
            foreach (var raycast in raycasts)
            {
                if (!raycast.collider.CompareTag(GameConstants.TerrainTag)) continue;
                pos = raycast.point;
                break;
            }
            
            var geyzer = Instantiate(geyserPrefab, pos, Quaternion.identity).GetComponent<GeyserController>();
            geyzer.SetTarget(WeaponLevel, OwnerCarController);
        }
    }
}
