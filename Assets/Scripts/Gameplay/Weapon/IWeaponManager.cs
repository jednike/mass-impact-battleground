using System;
using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
    public interface IWeaponManager
    {
        bool ExistWeapon();
        void InstallWeapon(PickUpObject pickUpObject);
        void RemoveWeapon(PickUpObject pickUpObject);
        void ThrowWeapon();
    }

    public abstract class DefaultWeaponManager : MonoBehaviourPun, IWeaponManager
    {
        protected abstract string EventName { get; }
        
        protected Transform WeaponTransform;
        protected GunTransformManager GunTransformManager;

        private void Awake()
        {
            GunTransformManager = GetComponent<GunTransformManager>();
        }

        public abstract bool ExistWeapon();
        public abstract void InstallWeapon(PickUpObject pickUpObject);
        public abstract void RemoveWeapon(PickUpObject pickUpObject);
        public void ThrowWeapon()
        {
            if (ExistWeapon() && PhotonNetwork.IsConnected)
            {
                photonView.RPC(EventName, RpcTarget.All);
            }
        }
    }
}