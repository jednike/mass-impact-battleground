﻿using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
	public class PickUpObject : MonoBehaviourPun
	{
		protected CarController OwnerCarController;
		protected IWeaponManager OwnerWeaponManager;
		
		protected bool Used;

		private void LateUpdate()
		{
			if(Used)
				return;
			
			transform.Rotate(new Vector3(0, 60, 0) * Time.deltaTime);
		}

		protected void OnTriggerEnter(Collider other)
		{
			var carController = other.gameObject.GetComponentInParent<CarController>();
			if(Used || !carController || carController.Destroyed || (carController.CurrentCarType != ActiveCarType.LocalAi && carController.CurrentCarType != ActiveCarType.LocalPlayer))
				return;
			
			photonView.RPC("OnCarEnter", RpcTarget.All, carController.OwnerId);
		}

		[PunRPC]
		protected void OnCarEnter(int ownerId)
		{
			if(!CarsInGameController.GetCarControllerById(ownerId, out var carController))
				return;
			OnCarEnter(carController);
		}
		
		protected virtual void OnCarEnter(CarController carController)
		{
		}
	}
}
