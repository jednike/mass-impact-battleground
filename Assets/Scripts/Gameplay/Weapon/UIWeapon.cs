﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	[Serializable]
	public class WeaponUI
	{
		public WeaponType WeaponType;
		public GameObject WeaponGameObject;
	}
	public class UIWeapon : MonoBehaviour
	{
		[SerializeField] private WeaponUI[] _weapons;
		public Image BulletNumbers;
		public Image WeaponLevel;
		
		private WeaponUI _activeWeaponUi;
		private GunManager _gunManager;

		private void Awake()
		{
			CarController.OnSetLocalPlayer += SetLocalPlayer;
			
			ChangeWeapon(WeaponType.None);
		}

		private void SetLocalPlayer(GameObject localPlayer)
		{
			_gunManager = localPlayer.GetComponent<GunManager>();
			_gunManager.OnChangeWeapon += ChangeWeapon;
		}

		private void ChangeWeapon(WeaponType weaponType)
		{
			SetActiveWeapon(false, _activeWeaponUi);
			var newWeapon = GetWeaponUiByType(weaponType);
			SetActiveWeapon(true, newWeapon);
			_activeWeaponUi = newWeapon;
			if (_activeWeaponUi == null)
			{
				WeaponLevel.color = InGamePrefabs.Instance.GetWeaponLevelColor(CarsBattleRoyal.WeaponLevel.None);
				BulletNumbers.fillAmount = 0;
			}
			else
			{
				WeaponLevel.color = InGamePrefabs.Instance.GetWeaponLevelColor(_gunManager.CurrentWeapon.WeaponLevel);
			}
		}

		private void SetActiveWeapon(bool active, WeaponUI weaponUi)
		{
			if (weaponUi == null)
				return;
			weaponUi.WeaponGameObject.SetActive(active);
		}

		private WeaponUI GetWeaponUiByType(WeaponType weaponType)
		{
			return _weapons.FirstOrDefault(weapon => weapon.WeaponType == weaponType);
		}

		private void Update()
		{
			if(!_gunManager || _activeWeaponUi == null)
				return;
			BulletNumbers.fillAmount = (float)_gunManager.CurrentWeapon.Bullets/_gunManager.CurrentWeapon.MaxBullets;
/*			BulletNumbers.color = Color.Lerp(Color.red, Color.green,
				(float) _gunManager.CurrentWeapon.Bullets / _gunManager.CurrentWeapon.MaxBullets);*/
		}

		private void OnDestroy()
		{
			if(_gunManager)
				_gunManager.OnChangeWeapon -= ChangeWeapon;
			CarController.OnSetLocalPlayer -= SetLocalPlayer;
		}
	}
}
