﻿using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
	public class RocketController : MonoBehaviour
	{
		private WeaponLevel _weaponLevel;
		private CarController _carController;
		
		[SerializeField] private float speed = 30f;
		[SerializeField] private float damage = 30f;
		[SerializeField] private float lifeTime = 5f;
		[SerializeField] private float radius = 40f;
		[SerializeField] private float power = 120000f;

		[SerializeField] private AudioClip explosionSound;
		
		private Transform _target;
		private Transform _transform;

		private void Awake()
		{
			_transform = transform;
		}

		private void Update () {
			if (_target)
			{
				_transform.LookAt(_target);
				_transform.position += speed * Time.deltaTime * _transform.forward;

				if (Vector3.Distance(transform.position, _target.position) > 0.3f) return;
				DamageCar(_target);
			}
			else
			{
				Destroy(gameObject);
			}
		}

		private void DamageCar(Component target)
		{
			var enemy = target.GetComponent<CarController>();
			if (!enemy) return;
			enemy.GetComponent<DamageCarManager>().DamageHp(damage, _carController.OwnerId);
			enemy.GetComponent<DamageSoundManager>().PlayDamageSound(explosionSound);
			
			Destroy(gameObject);
		}

		public void SetTarget(Transform target, WeaponLevel weaponLevel, CarController carController)
		{
			_target = target;
			_carController = carController;
			
			var multiplier = WeaponManager.UpgradeValueByType(weaponLevel);
			speed *= multiplier;
			damage *= multiplier;
			lifeTime *= multiplier;
			
			Destroy(gameObject, lifeTime);
		}

		private void OnCollisionEnter(Collision other)
		{
			DamageCar(other.transform);
			Destroy(Instantiate(InGamePrefabs.Instance.particles.GetRandomExplosion(), _transform.position, _transform.rotation), 5);
		}

		private void OnDestroy()
		{
			var explosionPos = transform.position;
			var list = new List<Rigidbody>();
			foreach (var player in GameObject.FindGameObjectsWithTag(GameConstants.CarTag))
				if (Vector3.Distance(player.transform.position, explosionPos) < radius)
					list.Add(player.GetComponent<Rigidbody>());
			foreach (var rb in list)
				rb.AddExplosionForce(power * 300, explosionPos, radius, 3.0F);
		}
	}
}
