﻿using UnityEngine;

namespace CarsBattleRoyal
{
    public class GunWeaponManager : RotatingWeaponManager
    {
        [SerializeField] private GameObject _attackParticles;
        [SerializeField] private ParticleSystem[] _gunParticles;

        protected override bool CheckOnAttackAvailable()
        {
            if (!CurrentTargetCar)
            {
                Utils.EnableAttacksElements(_gunParticles, Source, false);
                return false;
            }
            
            RaycastHit hit;
            if (!RaycastCar(out hit))
            {
                Utils.EnableAttacksElements(_gunParticles, Source, false);
                return false;
            }
            
            Attack(hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal));
            
            return true;
        }

        public override void ThrowWeapon()
        {
            base.ThrowWeapon();
            Utils.EnableAttacksElements(_gunParticles, Source, false);
        }

        protected void Attack(Vector3 attackPosition, Quaternion attackRotation)
        {
            var enemyCar = CurrentTargetCar.GetComponent<CarController>();
            if (!enemyCar)
                return;

            ReloadTime -= Time.deltaTime;
            if (ReloadTime > 0) return;
            
            ReloadTime = _maxReloadTime;
            
            Utils.EnableAttacksElements(_gunParticles, Source, true);

            enemyCar.GetComponent<DamageCarManager>().DamageHp(DamagePerAttack, OwnerCarController.OwnerId);
            enemyCar.GetComponent<DamageSoundManager>().PlayDamageSound(CollissionSound);
            enemyCar.GetComponent<Rigidbody>().AddExplosionForce(1000, attackPosition, 7, 3.0f);
            Destroy(Instantiate(_attackParticles, attackPosition, attackRotation), 0.3f);

            if (OwnerCarController.CurrentCarType == ActiveCarType.LocalAi ||
                OwnerCarController.CurrentCarType == ActiveCarType.LocalPlayer)
            {
                Bullets -= 1;
            }
        }
    }
}
