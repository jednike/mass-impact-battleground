﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace CarsBattleRoyal
{
	public class GeyserController : MonoBehaviour
	{
		private WeaponLevel _weaponLevel;
		private CarController _carController;
		
		[SerializeField] private float damage = 10f;
		[SerializeField] private float lifeTime = 10f;
		[SerializeField] private float radius = 20f;
		[SerializeField] private float power = 12000f;

		[SerializeField] private AudioClip explosionSound;
		[SerializeField] private ParticleSystem volcano;
		[SerializeField] private ParticleSystem tube;
		[SerializeField] private CapsuleCollider myCollider;

		private readonly List<GeyserCarEffect> _targets = new List<GeyserCarEffect>();

		public void SetTarget(WeaponLevel weaponLevel, CarController carController)
		{
			_carController = carController;

			var multiplier = WeaponManager.UpgradeValueByType(weaponLevel);
			damage *= multiplier;
			lifeTime *= multiplier;
			
			radius *= multiplier;
			myCollider.radius = radius;
			
			var mainModule = tube.main;
			mainModule.startSizeX = mainModule.startSizeX.constant * multiplier;
			mainModule.startSizeZ = mainModule.startSizeZ.constant * multiplier;
			
			var shapeModule = volcano.shape;
			shapeModule.radius *= multiplier;


			Destroy(gameObject, lifeTime);
		}

		private void Update()
		{
			if(!_carController)
				return;
			foreach (var target in _targets)
			{
				target.UpdateEffect();
			}
		}

		private void OnTriggerEnter(Collider other)
		{
			var effect = other.GetComponentInParent<GeyserCarEffect>();
			effect.SetInfo(damage, _carController.OwnerId, explosionSound, power);
			_targets.Add(effect);
		}
		private void OnTriggerExit(Collider other)
		{
			_targets.Remove(other.GetComponentInParent<GeyserCarEffect>());
		}
	}
}
