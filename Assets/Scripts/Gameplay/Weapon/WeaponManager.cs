﻿using System.Collections;
using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
	public enum WeaponLevel
	{
		Green,
		Blue,
		Purple,
		Gold,
		None
	}
	public enum ObjectType
	{
		Weapon,
		Object
	}
	public enum ObjectsType
	{
		Mines,
		Armor,
		None
	}
	public enum WeaponType
	{
		None,
		Gun,
		IceGun,
		RocketLauncher,
		TeslaGun,
		GeyserGun,
	}

	[RequireComponent(typeof(PhotonView))]
	[RequireComponent(typeof(AudioSource))]
	public class WeaponManager: PickUpObject
	{
		[Header("Default Weapon Info")]
		public string WeaponName;

		private GameObject _weaponLevelGameObject;

		public WeaponLevel WeaponLevel;
		public WeaponType WeaponType;

		public int MaxBullets;
		[HideInInspector] public int Bullets;
		
		[SerializeField] protected float _maxReloadTime;
		protected float ReloadTime;

		[SerializeField] protected float DamagePerAttack;
		[SerializeField] public float MaxFindDistance = 30f;
		
		[SerializeField] protected Transform AttackPoint;
		
		[SerializeField] protected AudioClip AttackSound;
		[SerializeField] protected AudioClip CollissionSound;

		protected AudioSource Source;
		public bool InAttack;
		

		#region Preparation

		private void Awake()
		{
			Source = GetComponent<AudioSource>();
			if (photonView && photonView.InstantiationData != null && photonView.InstantiationData[0] != null)
			{
				WeaponLevel = (WeaponLevel) photonView.InstantiationData[0];
			}
			else
			{
				Debug.LogError("Not exist photon view or empty data");
			}

			_weaponLevelGameObject = InGamePrefabs.Instance.SpawnParticleByLevel(WeaponLevel, transform);

			UpgradeCharacteristics();
		}

		protected bool RaycastCar(out RaycastHit hit)
		{
			if (!Physics.Raycast(AttackPoint.position, transform.forward, out hit, MaxFindDistance)) return false;
			if (hit.collider.gameObject.layer != GameConstants.CarLayer) return false;
			
			var car = hit.collider.GetComponentInParent<CarController>();
			return car && car.OwnerId != OwnerCarController.OwnerId;
		}

		protected virtual void UpgradeCharacteristics()
		{
			MaxBullets = Mathf.CeilToInt(MaxBullets * UpgradeValueByType(WeaponLevel));
			MaxFindDistance *= UpgradeValueByType(WeaponLevel);
			DamagePerAttack *= UpgradeValueByType(WeaponLevel);
		}

		public static float UpgradeValueByType(WeaponLevel weaponLevel)
		{
			switch (weaponLevel)
			{
				case WeaponLevel.Green:
					return 1f;
				case WeaponLevel.Blue:
					return 1.1f;
				case WeaponLevel.Purple:
					return 1.3f;
				case WeaponLevel.Gold:
					return 1.6f;
				default:
					return 1f;
			}
		}

		private void Start()
		{
			Init();
		}

		protected virtual void Init()
		{
			Bullets = MaxBullets;
		}

		#endregion

		#region Attack

		private void Update()
		{
			if (!OwnerCarController)
				return;

			RotateOnTarget();
			InAttack = CheckOnAttackAvailable();

			if (NeedDestroy())
				DestroyWeapon();
		}

		protected virtual void RotateOnTarget()
		{
			
		}
		
		protected virtual bool CheckOnAttackAvailable()
		{
			return true;
		}

		#endregion

		#region Destroy

		private bool NeedDestroy()
		{
			return Bullets <= 0;
		}

		public void DestroyWeapon()
		{
			OwnerWeaponManager.RemoveWeapon(this);
			Destroy(gameObject);
		}

		public virtual void ThrowWeapon()
		{
			tag = GameConstants.WeaponTag;
			
			OwnerWeaponManager.RemoveWeapon(this);
			transform.SetParent(null, true);
			_weaponLevelGameObject.SetActive(true);
			OwnerCarController = null;
			StartCoroutine(UnusedAfter());
		}

		private IEnumerator UnusedAfter(float time = 5f)
		{
			yield return new WaitForSecondsRealtime(time);
			Used = false;
		}

		#endregion

		protected override void OnCarEnter(CarController carController)
		{
			if (OwnerCarController)
				return;

			if (carController.GetComponent<GunManager>().ExistWeapon()) return;
			Used = true;

			tag = "Untagged";
			OwnerCarController = carController;
			OwnerWeaponManager = OwnerCarController.GetComponent<GunManager>();
			OwnerWeaponManager.InstallWeapon(this);
			_weaponLevelGameObject.SetActive(false);
		}
	}
}