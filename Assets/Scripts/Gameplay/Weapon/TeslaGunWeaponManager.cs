﻿using UnityEngine;
using UnityEngine.Serialization;

namespace CarsBattleRoyal
{
    public class TeslaGunWeaponManager : WeaponManager
    {
        [SerializeField] private ParticleSystem[] gunParticles;
        [SerializeField] private ParticleSystem alwaysActiveLights;

        private Transform[] _closestCars;
        private readonly Collider[] _results = new Collider[5];
        
        protected override void UpgradeCharacteristics()
        {
            base.UpgradeCharacteristics();
            var mainModule = gunParticles[0].main;
            mainModule.startSizeMultiplier *= UpgradeValueByType(WeaponLevel);
        }
        
        protected override bool CheckOnAttackAvailable()
        {
            _closestCars = CarsInGameController.FindAllClosest(OwnerCarController, MaxFindDistance,
                OwnerCarController.transform.position);
            
            if (_closestCars.Length == 0)
            {
                Utils.EnableAttacksElements(gunParticles, Source, false);
                return false;
            }

            var attack = Attack();
            if (!attack)
            {
                Utils.EnableAttacksElements(gunParticles, Source, false);
            }
            
            return attack;
        }

        public override void ThrowWeapon()
        {
            base.ThrowWeapon();
            alwaysActiveLights.Stop();
            Utils.EnableAttacksElements(gunParticles, Source, false);
        }

        private bool Attack()
        {
            ReloadTime -= Time.deltaTime;
            if (ReloadTime > 0) return false;
            
            ReloadTime = _maxReloadTime;
            Utils.EnableAttacksElements(gunParticles, Source, true);

            var attack = false;

            var size = Physics.OverlapSphereNonAlloc(AttackPoint.position, MaxFindDistance, _results,
                1 << GameConstants.CarLayer);
            if (size == 0)
                return false;
            
            for (var i = 0; i < size; i++)
            {
                var enemyCar = _results[i].GetComponentInParent<CarController>();
                if(!enemyCar || enemyCar.OwnerId == OwnerCarController.OwnerId)
                    continue;
                
                attack = true;
                enemyCar.GetComponent<DamageCarManager>().DamageHp(DamagePerAttack, OwnerCarController.OwnerId);
            }

            if (!attack) return false;
            
            if (OwnerCarController.CurrentCarType == ActiveCarType.LocalAi ||
                OwnerCarController.CurrentCarType == ActiveCarType.LocalPlayer)
            {
                Bullets--;
            }
            
            return true;
        }

        protected override void OnCarEnter(CarController carController)
        {
            base.OnCarEnter(carController);
            alwaysActiveLights.Play();
        }
    }
}
