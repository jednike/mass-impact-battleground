﻿using System;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class RotatingWeaponManager : WeaponManager
    {
        protected Transform CurrentTargetCar;
        [SerializeField] private int _xRotateLimit = 40;
        [SerializeField] private int _yRotateLimit = 15;

        protected override void RotateOnTarget()
        {
            if (OwnerCarController.SelectedTarget)
                CurrentTargetCar = CarsInGameController.FindClosest(OwnerCarController, MaxFindDistance,
                    OwnerCarController.transform.position, _xRotateLimit);
            else
                CurrentTargetCar = CarsInGameController.FindClosest(OwnerCarController, MaxFindDistance,
                    OwnerCarController.transform.position);

            if (OwnerCarController.CurrentCarType == ActiveCarType.LocalPlayer && !Scores.Instance.SelectedTarget)
                CurrentTargetCar = CarsInGameController.FindClosest(OwnerCarController, MaxFindDistance,
                    OwnerCarController.transform.position, _xRotateLimit);
            else
                CurrentTargetCar = CarsInGameController.FindClosest(OwnerCarController, MaxFindDistance,
                    OwnerCarController.transform.position);

            if (!CurrentTargetCar)
                return;

            var tmpRotation = transform.localRotation;
            var leadTargetPosition = CurrentTargetCar.position;
            var targetPointTurret = (leadTargetPosition - transform.position).normalized;
            var targetRotationTurret = Quaternion.LookRotation(targetPointTurret, transform.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotationTurret, Time.deltaTime * 10);
            transform.localRotation =
                Quaternion.Euler(
                    Utils.ClampAngle(transform.localRotation.eulerAngles.x, -_xRotateLimit, _xRotateLimit),
                    Utils.ClampAngle(transform.localRotation.eulerAngles.y, -_yRotateLimit, _yRotateLimit),
                    tmpRotation.eulerAngles.z);
        }
    }
}
