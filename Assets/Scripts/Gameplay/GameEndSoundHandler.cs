﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class GameEndSoundHandler : MonoBehaviour
    {
        private AudioSource _source;

        private void Awake()
        {
            _source = GetComponent<AudioSource>();
        }

        private void OnEnable()
        {
            GameControllers.GameEndEvent += OnGameEndEvent;
        }

        private void OnGameEndEvent(int position, int kills, int earnedMoney)
        {
            if (position == 0)
            {
                _source.PlayOneShot(InGamePrefabs.Instance.sounds.firstPlaceSound);
            }
            else if(position <= 19)
            {
                _source.PlayOneShot(InGamePrefabs.Instance.sounds.secondPlaceSound);
            }
            else
            {
                _source.PlayOneShot(InGamePrefabs.Instance.sounds.thirdPlaceSound);
            }
        }

        private void OnDisable()
        {
            GameControllers.GameEndEvent -= OnGameEndEvent;
        }
    }
}
