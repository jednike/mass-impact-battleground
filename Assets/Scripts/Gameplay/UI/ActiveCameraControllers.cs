﻿using EVP;
using UnityEngine;

namespace CarsBattleRoyal
{
	public class ActiveCameraControllers : MonoBehaviour {
		[SerializeField] private VehicleCameraController _vehicleFollowCamera;
		[SerializeField] private FollowForTarget _minimapCamera;
		
		private void Awake()
		{
			CarController.OnSetLocalPlayer += SetLocalPlayer;
		}
		private void OnDestroy()
		{
			CarController.OnSetLocalPlayer -= SetLocalPlayer;
		}

		private void SetLocalPlayer(GameObject localPlayer)
		{
			_vehicleFollowCamera.target = localPlayer.transform;
			_minimapCamera.SetTarget(localPlayer.transform);
		}
	}
}
