﻿using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class DamageArrowController : MonoBehaviour
	{
		[SerializeField] private CanvasScaler _canvasScaler;
		private Transform _carObjectTransform;
	
		private GameObject _arrowOnScreen;
		private Image _arrowOnScreenSprite;
	
		private bool _visible;

		private Camera _camera;
	
		private float _realBorder;
		private bool _onScreen;
	
		private int _indicatorSize = 70;
		private const int Border = 10;
	
		private float _screenScaleX;
		private float _screenScaleY;
		private bool _screenScaled;
	
		private Vector2 _referenceResolution;

		private const float DisableTime = 2f;
		private float _timeToDisable = 0;

		public bool IsActive;
		public int TargetId;

		private void Awake ()
		{
			_arrowOnScreen = gameObject;
			_arrowOnScreen.layer = 0;
			_arrowOnScreenSprite = _arrowOnScreen.GetComponent<Image>();

			if(_canvasScaler.uiScaleMode == CanvasScaler.ScaleMode.ScaleWithScreenSize)
			{
				_referenceResolution = _canvasScaler.referenceResolution;
				var screenResolution = new Vector2(Screen.width, Screen.height);
				_screenScaleX = screenResolution.x / _referenceResolution.x;
				_screenScaleY = screenResolution.y / _referenceResolution.y;
				_screenScaled = true;
			} else {
				_screenScaled = false;
			}

			if(_screenScaled){
				_indicatorSize = Mathf.RoundToInt(_indicatorSize * _screenScaleX);
			}
			_realBorder = (_indicatorSize/2f) + Border;
		
			_camera = Camera.main;
		}

		public void SetNewTarget(int newTarget)
		{
			if (IsActive && TargetId == newTarget)
			{
				_timeToDisable = 0;
				return;
			}
			
			CarController car;
			CarsInGameController.GetCarControllerById(newTarget, out car);
			if(!car)
				return;

			_carObjectTransform = car.transform;
			TargetId = newTarget;
			_timeToDisable = 0;
			IsActive = true;
			gameObject.SetActive(true);
		}

		private void Update ()
		{
			_timeToDisable += Time.deltaTime;
			if (_timeToDisable > DisableTime)
			{
				IsActive = false;
				gameObject.SetActive(false);
			}
			
			var v2DPos = _camera.WorldToScreenPoint(_carObjectTransform.localPosition + Vector3.zero);
			float angle;

			var heading = _carObjectTransform.position - Camera.main.transform.position;
			var behindCamera = (Vector3.Dot(Camera.main.transform.forward, heading) < 0);

			if (v2DPos.x > Screen.width - _realBorder || v2DPos.x < _realBorder || v2DPos.y > Screen.height - _realBorder ||
			    v2DPos.y < _realBorder || behindCamera)
			{
				_onScreen = false;
				angle = Mathf.Atan2(v2DPos.y - (Screen.height / 2f), v2DPos.x - (Screen.width / 2f));
				float xCut, yCut;
				if (v2DPos.x - Screen.width / 2f > 0)
				{
					xCut = Screen.width / 2f - _realBorder;
					yCut = xCut * Mathf.Tan(angle);
				}
				else
				{
					xCut = -Screen.width / 2f + _realBorder;
					yCut = xCut * Mathf.Tan(angle);
				}
				if (yCut > Screen.height / 2f - _realBorder)
				{
					yCut = Screen.height / 2f - _realBorder;
					xCut = yCut / Mathf.Tan(angle);
				}
				if (yCut < -Screen.height / 2f + _realBorder)
				{
					yCut = -Screen.height / 2f + _realBorder;
					xCut = yCut / Mathf.Tan(angle);
				}
				if (behindCamera)
				{
					xCut = -xCut;
					yCut = -yCut;
				}
				if (_screenScaled)
				{
					xCut /= _screenScaleX;
					yCut /= _screenScaleY;
				}
			
				_arrowOnScreen.transform.localPosition = new Vector3(xCut, yCut, 0);
			}
			else
			{
				_onScreen = true;
				var xScaled = v2DPos.x - (Screen.width / 2f);
				var yScaled = v2DPos.y - (Screen.height / 2f);
				if (_screenScaled)
				{
					xScaled /= _screenScaleX;
					yScaled /= _screenScaleY;
				}
				_arrowOnScreen.transform.localPosition = new Vector3(xScaled, yScaled, 0);
			}

			if (!_onScreen)
			{
				angle = behindCamera ? Mathf.Atan2(-(v2DPos.y - (Screen.height / 2f)), -(v2DPos.x - (Screen.width / 2f))) : Mathf.Atan2(v2DPos.y - (Screen.height / 2f), v2DPos.x - (Screen.width / 2f));
			}
			else
			{
				angle = 90 * Mathf.Deg2Rad;
			}
			_arrowOnScreenSprite.enabled = !_onScreen;
			_arrowOnScreen.transform.localEulerAngles = new Vector3(0, 0, angle * Mathf.Rad2Deg - 90);
		}
	}
}
