﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
    public class ConnectionPanelManager : MonoBehaviour
    {
        [SerializeField] private Text connectionStateText;
        [SerializeField] private Image clockImage;
        [SerializeField] private GameObject cancelButton;

        private void Awake()
        {
            NetworkManager.CountdownText = connectionStateText;
            NetworkManager.TotalTimeImage = clockImage;
            OnGameStateChanged(GameControllers.LastState, GameControllers.GameState);
        }

        private void OnEnable()
        {
            GameControllers.OnGameStateChanged += OnGameStateChanged;
        }

        private void OnGameStateChanged(GameState lastState, GameState newState)
        {
            switch (newState)
            {
                case GameState.ConnectingToServer:
                    cancelButton.SetActive(true);
                    connectionStateText.text =
                        MultiLanguageManager.GetTranslateString("Connection to server", "Connect to server", connectionStateText);
                    break;
                case GameState.ConnectingToRoom:
                    cancelButton.SetActive(true);
                    connectionStateText.text =
                        MultiLanguageManager.GetTranslateString("Connection to room", "Game finding", connectionStateText);
                    break;
                case GameState.WaitingPlayers:
                    cancelButton.SetActive(true);
                    connectionStateText.text =
                        MultiLanguageManager.GetTranslateString("Waiting for players", "Waiting all players...", connectionStateText);
                    break;
                case GameState.WaitingStartGame:
                    cancelButton.SetActive(false);
                    break;
                case GameState.BeforeStart:
                    cancelButton.SetActive(false);
                    connectionStateText.text =
                        MultiLanguageManager.GetTranslateString("Lets go", "Lets go!", connectionStateText);
                    break;
            }
        }

        private void OnDisable()
        {
            GameControllers.OnGameStateChanged -= OnGameStateChanged;
        }
    }
}
