﻿using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class HealthInUI : MonoBehaviour
	{
		[SerializeField] private Image _carHealth;
		private HealthManager _healthManager;
		
		private void Awake()
		{
			CarController.OnSetLocalPlayer += SetLocalPlayer;
		}
		private void OnDestroy()
		{
			CarController.OnSetLocalPlayer -= SetLocalPlayer;
		}

		private void SetLocalPlayer(GameObject localPlayer)
		{
			_healthManager = localPlayer.GetComponent<HealthManager>();
		}
		
		private void Update()
		{
			if(!_healthManager)
				return;
			_carHealth.fillAmount = _healthManager.Hp / _healthManager.MaxHp;
		}
	}
}
