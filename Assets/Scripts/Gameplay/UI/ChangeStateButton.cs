﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	[RequireComponent(typeof(Button))]
	public class ChangeStateButton : MonoBehaviour
	{
		[HideInInspector][SerializeField] private GameState _nextState;
		[SerializeField] private bool _toLastState;

		private void Awake()
		{
			if(_nextState != GameState.None || _toLastState)
				GetComponent<Button>().onClick.AddListener(ChangeState);
		}
		private void ChangeState()
		{
			if(_toLastState)
				ChangeStateToLast();
			else
				ChangeStateTo(_nextState);
		}

		public void ChangeStateTo(GameState newState)
		{
			GameControllers.GameState = newState;
		}
		public void ChangeStateToLast()
		{
		}
		public void ChangeStateTo(string newState)
		{
			var state = (GameState) Enum.Parse(typeof(GameState), newState, true);
			ChangeStateTo(state);
		}
	}
}