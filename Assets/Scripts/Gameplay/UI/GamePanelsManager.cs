﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class GamePanelsManager: MonoBehaviour
	{
		[Header("Game UI")]
		[SerializeField] private GameObject gamePanel;
		
		[Header("Desant UI")]
		[SerializeField] private GameObject desantPanel;
		
		[Header("Pause Panel")]
		[SerializeField] private GameObject pausePanel;
		
		[Header("Connecting Panel")]
		[SerializeField] private GameObject connectingPanel;

		[Header("End Game Panel")]
		[SerializeField] private GameObject endGamePanel;

		public void Awake()
		{			
			if (!PhotonNetwork.IsConnected)
			{
				PhotonNetwork.OfflineMode = true;
				PhotonNetwork.LeaveRoom();
				PhotonNetwork.JoinRandomRoom();
			}
			Application.targetFrameRate = 30;
			
			GameControllers.OnGameStateChanged += OnGameStateChanged;
			GameControllers.GameState = GameState.PreDesant;
		}

		private void Start()
		{
			PhotonNetwork.IsMessageQueueRunning = true;
		}

		private void OnGameStateChanged(GameState lastState, GameState newState)
		{
			switch (lastState)
			{
				case GameState.GamePlay:
					gamePanel.SetActive(false);
					break;
				case GameState.Pause:
					pausePanel.SetActive(false);
					break;
				case GameState.PreDesant:
					break;
				case GameState.Desant:
					desantPanel.SetActive(false);
					break;
				case GameState.EndGame:
					endGamePanel.SetActive(false);
					break;
				case GameState.ConnectingToServer:
				case GameState.ConnectingToRoom:
				case GameState.WaitingPlayers:
				case GameState.WaitingStartGame:
				case GameState.BeforeStart:
					connectingPanel.SetActive(false);
					break;
			}
			switch (newState)
			{
				case GameState.GamePlay:
					gamePanel.SetActive(true);
					break;
				case GameState.Pause:
					AnalyticsManager.LogEvent("ClickPause");
					pausePanel.SetActive(true);
					break;
				case GameState.PreDesant:
					break;
				case GameState.Desant:
					desantPanel.SetActive(true);
					break;
				case GameState.LoadMenu:
					if(CarController.Local)
						CarController.Local.GetComponent<CarDeathHandler>().DestroyCar(999);
					SceneManager.LoadScene("MenuScene");
					break;
				case GameState.EndGame:
					endGamePanel.SetActive(true);
					break;
				case GameState.ConnectingToServer:
				case GameState.ConnectingToRoom:
				case GameState.WaitingPlayers:
				case GameState.WaitingStartGame:
				case GameState.BeforeStart:
					connectingPanel.SetActive(true);
					break;
			}
		}

		private void OnDestroy()
		{
			GameControllers.OnGameStateChanged -= OnGameStateChanged;
		}
	}
}
