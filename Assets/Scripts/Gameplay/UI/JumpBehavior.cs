﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class JumpBehavior : MonoBehaviour
	{
		private Image _image;
		private Button _button;

		[SerializeField] private Color _activeColor;
		[SerializeField] private Color _notActiveColor;

		private CarJumper _carJumper;
		
		private void Start ()
		{
			_image = GetComponent<Image>();
			_button = GetComponent<Button>();

			_carJumper = CarController.Local.GetComponent<CarJumper>();
		}
	
		private void Update ()
		{
			_image.color = _carJumper.LastJumpTime <= 0 ? _activeColor : _notActiveColor;
			_button.interactable = _carJumper.LastJumpTime <= 0;
		}
	}
}
