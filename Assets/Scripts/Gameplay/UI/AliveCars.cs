﻿using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class AliveCars : MonoBehaviour
	{
		[SerializeField] private Text _text;
		[SerializeField] private Text _killsText;

		private BestPlayerManager _bestPlayerManager;
		private KilledCarsManager _killedCarsManager;
	
		private void Update ()
		{
			if (_bestPlayerManager == null)
			{
				_bestPlayerManager = CarController.Local.GetComponent<BestPlayerManager>();
				_killedCarsManager = CarController.Local.GetComponent<KilledCarsManager>();
				return;
			}
			_text.text = CarCount.AliveCars.ToString();
			_killsText.text = _killedCarsManager.KilledCars.ToString();
			_killsText.color = _bestPlayerManager.IsBest ? Color.yellow : Color.white;
		}
	}
}
