﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class DesantManager : MonoBehaviour
    {
        public void DesantCar()
        {
            GameControllers.GameState = GameState.GamePlay;
            CarSpawner.SpawnCar();
        }
    }
}