﻿using UnityEngine;

namespace CarsBattleRoyal
{
    public class GamePanelManager : MonoBehaviour
    {
        [SerializeField] private GameObject defaultGameButton;
        [SerializeField] private GameObject raceGameButton;
        [SerializeField] private CanvasGroup canvasGroup;
        private CarController _carController;
        
        [SerializeField] private GameObject maximapPanel;

        private void Awake()
        {
            defaultGameButton.SetActive(!Scores.Instance.AutoAccelerate);
            raceGameButton.SetActive(Scores.Instance.AutoAccelerate);
            
            CarController.OnSetLocalPlayer += SetLocalPlayer;
        }

        public void ThrowWeapon()
        {
            _carController.GetComponent<GunManager>().ThrowWeapon();
        }

        private void SetLocalPlayer(GameObject carObject)
        {
            _carController = carObject.GetComponent<CarController>();
        }
		
        public void OpenMaximap(bool open)
        {
            canvasGroup.alpha = open ? 0 : 1;
            canvasGroup.interactable = !open;
            maximapPanel.SetActive(open);
        }
        
        private void OnDestroy()
        {
            CarController.OnSetLocalPlayer -= SetLocalPlayer;
        }
    }
}