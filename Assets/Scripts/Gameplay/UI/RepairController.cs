﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	[Serializable]
	public class ObjectUI
	{
		public ObjectsType ObjectsType;
		public GameObject ObjectGameObject;
		public Text AmountText;
		public Image ReloadImage;
	}
	public class RepairController : MonoBehaviour
	{
		[SerializeField] private ObjectUI[] _objects;
		private WearObjectManager _gunManager;
		
		private ObjectUI _activeWeaponUi;

		private void Awake()
		{
			CarController.OnSetLocalPlayer += SetLocalPlayer;
		}

		private void SetLocalPlayer(GameObject player)
		{
			_gunManager = player.GetComponent<WearObjectManager>();
			_gunManager.OnChangeWeapon += OnChangeWeapon;
		}

		private void Update()
		{
			if(_activeWeaponUi == null)
				return;
			switch (_activeWeaponUi.ObjectsType)
			{
				case ObjectsType.Mines:
					var mine = (MinesObjectManager) _gunManager.CurrentWeapon;
					_activeWeaponUi.AmountText.text = mine.CurrentMines.ToString();
					_activeWeaponUi.ReloadImage.fillAmount = (mine.maxReloadTime - mine.CurrentTime) / mine.maxReloadTime;
					break;
				case ObjectsType.Armor:
					break;
				case ObjectsType.None:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void OnChangeWeapon(ObjectsType obj)
		{
			SetActiveWeapon(false, _activeWeaponUi);
			_activeWeaponUi = GetWeaponUiByType(obj);
			SetActiveWeapon(true, _activeWeaponUi);
		}

		private void SetActiveWeapon(bool active, ObjectUI weaponUi)
		{
			if (weaponUi == null)
				return;
			weaponUi.ObjectGameObject.SetActive(active);
		}

		public void UseDopWeapon()
		{
			_gunManager.UseWeapon();
		}

		private ObjectUI GetWeaponUiByType(ObjectsType weaponType)
		{
			return _objects.FirstOrDefault(weapon => weapon.ObjectsType == weaponType);
		}
		
		private void OnDestroy()
		{
			if(_gunManager)
				_gunManager.OnChangeWeapon -= OnChangeWeapon;
			CarController.OnSetLocalPlayer -= SetLocalPlayer;
		}
	}
}
