﻿using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class DamagePointController : MonoBehaviour {
		private Text _damageText;

		private void Awake()
		{
			Destroy(gameObject, 1f);
			_damageText = GetComponentInChildren<Text>();
		}

		public void SetText(string text)
		{
			_damageText.text = text;
		}
	}
}
