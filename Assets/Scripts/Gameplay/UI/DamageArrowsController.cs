﻿using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class DamageArrowsController : MonoBehaviour
	{
		[SerializeField] private DamageArrowController[] _arrows;

		private void Awake()
		{
			DamageCarManager.OnDamageLocalPlayer += OnDamageLocalPlayer;
		}

		private void OnDamageLocalPlayer(int damageID)
		{
			var arrow = GetActiveWithThisId(damageID);
			if(arrow)
				arrow.SetNewTarget(damageID);
		}

		public DamageArrowController GetActiveWithThisId(int damageID)
		{
			DamageArrowController notActive = null;
			foreach (var arrow in _arrows)
			{
				if (arrow.IsActive)
				{
					if (arrow.TargetId == damageID)
						return arrow;
				}
				else
				{
					notActive = arrow;
				}
			}
			return notActive;
		}

		private void OnDestroy()
		{
			DamageCarManager.OnDamageLocalPlayer -= OnDamageLocalPlayer;
		}
	}
}
