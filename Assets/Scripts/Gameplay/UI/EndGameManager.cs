﻿using System;
using System.Collections;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
    public class EndGameManager : MonoBehaviour
    {
        [SerializeField] private Text resultText;
        [SerializeField] private Text earnedMoneyText;

        private void Awake()
        {
            GameControllers.GameEndEvent += OnGameEndEvent;
            
            var carController = CarController.Local;
            
            var earnedMoney = 0;
            if(carController)
                earnedMoney += carController.GetComponent<KilledCarsManager>().KilledCars * 2;
            if(CarCount.AliveCars <= 21)
                earnedMoney += 51 - CarCount.AliveCars;
            if (!PhotonNetwork.OfflineMode)
                earnedMoney *= 2;

            if (CarCount.AliveCars <= 1 && !carController.Destroyed)
            {
                AnalyticsManager.LogEvent("Win");
                Scores.Instance.Victories++;
                GameControllers.GameEndEvent(0, carController.GetComponent<KilledCarsManager>().KilledCars, earnedMoney);
            }
            else
            {
                AnalyticsManager.LogEvent("Lose");
                Scores.Instance.Defeats++;
                GameControllers.GameEndEvent(CarCount.AliveCars, carController.GetComponent<KilledCarsManager>().KilledCars, earnedMoney);
            }
            Scores.Instance.Money += earnedMoney;
					
            StartCoroutine(AdController.ShowInterAds("after_endgame", 2f));
            
            NetworkManager.ChangeMaster();
        }

        public void NewGame()
        {
            StartCoroutine(DisconnectFromServer(success =>
            {
                if (!success) return;
                AnalyticsManager.LogEvent("SuccessRestart");
                if(!PhotonNetwork.IsConnected)
                    SceneManager.LoadScene("LoadScene");
                else
                    GameControllers.GameState = GameState.ConnectingToServer;
            }));
            AnalyticsManager.LogEvent("ClickRestart");
        }

        private IEnumerator DisconnectFromServer(Action<bool> action)
        {
            while (PhotonNetwork.IsConnected)
            {
                PhotonNetwork.Disconnect();
                yield return new WaitForSeconds(Time.deltaTime);
            }

            action(true);
        }

        private void OnGameEndEvent(int position, int kills, int earnedMoney)
        {
            if (position == 0)
            {
                resultText.text = "#1 " + MultiLanguageManager.GetTranslateString("Champion", "Champion!", resultText);
            }
            else
            {
                resultText.text = MultiLanguageManager.GetTranslateString("Lose", "You placed ", resultText) +
                                   ("#" + (CarCount.AliveCars + 1));
            }
            earnedMoneyText.text = "+" + earnedMoney;
        }

        private void OnDestroy()
        {
            GameControllers.GameEndEvent -= OnGameEndEvent;
        }
    }
}