﻿using UnityEngine;

namespace CarsBattleRoyal
{
	public class DamagePointsController : MonoBehaviour {

		[SerializeField] private GameObject _popupText;

		private static DamagePointsController _instance;
		
		private void Awake()
		{
			_instance = this;
		}

		public static void CreateFloatingText(string text, Transform location)
		{
			var damageText = Instantiate(_instance._popupText).GetComponent<DamagePointController>();

			damageText.transform.position = location.position;
			damageText.transform.LookAt(Camera.main.transform);
			damageText.transform.rotation *= Quaternion.Euler(0, 180, 0);
			
			damageText.SetText(text);
		}
	}
}
