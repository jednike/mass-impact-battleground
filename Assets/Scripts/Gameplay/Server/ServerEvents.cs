﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    [Serializable]
    public class DefaultMessage
    {
        public bool Success = true;
        public string Message;
    }
    [Serializable]
    public class UserTypeMessage: DefaultMessage
    {
        public int UserType = 0;
    }
    [Serializable]
    public class GameSession: DefaultMessage
    {
        public int SessionId;
    }
    [Serializable]
    public class LootBoxInfo: DefaultMessage
    {
        public int Kills;
        public int TargetKills;
		
        public bool Opened;
        public long TimeBeforeOpen;
    }

    [Serializable]
    public class Leader
    {
        public int Number;
        public string Nickname;
        public int Points;
    }

    [Serializable]
    public class Leaders: DefaultMessage
    {
        public int Number;
        public int TotalNumber;
        public int Points;
		
        public string SeasonName;
        public int Days;
		
        public Leader[] First;
        public Leader[] Second;
    }
    [Serializable]
    public class WinsMessage: DefaultMessage
    {
        public int[] Wins;
    }
}