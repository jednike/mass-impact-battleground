﻿using UnityEngine;
using UnityEngine.Serialization;

namespace CarsBattleRoyal
{
	public class FollowForTarget : MonoBehaviour
	{
		[SerializeField] private Transform target;

		[Header("Position")]
		[SerializeField] private bool xAxis;
		[SerializeField] private bool yAxis;
		[SerializeField] private bool zAxis;

		[Header("Rotation")]
		[SerializeField] private bool xRotate;
		[SerializeField] private bool yRotate;
		[SerializeField] private bool zRotate;

		public void SetTarget(Transform targetTransform)
		{
			target = targetTransform;
		}

		private void LateUpdate()
		{
			if(!target)
				return;

			var myTransform = transform;
			var position = myTransform.position;
			var targetPosition = target.position;
			
			position = new Vector3(xAxis ? targetPosition.x : position.x, yAxis ? targetPosition.y : position.y, zAxis ? targetPosition.z : position.z);
			myTransform.position = position;

			var angles = myTransform.eulerAngles;
			var targetAngles = target.eulerAngles;
			angles = new Vector3(xRotate ? targetAngles.x : angles.x,
				yRotate ? targetAngles.y : angles.y,
				zRotate ? targetAngles.z : angles.z);
			myTransform.eulerAngles = angles;
		}
	}
}
