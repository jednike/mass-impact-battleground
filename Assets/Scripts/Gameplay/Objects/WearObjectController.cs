﻿
using Photon.Pun;

namespace CarsBattleRoyal
{
    public abstract class WearObjectController : PickUpObject {
        public ObjectsType WeaponType;

        public abstract void UseObject();

        protected override void OnCarEnter(CarController carController)
        {
            tag = "Untagged";
            Used = true;
            
            OwnerCarController = carController;
            OwnerWeaponManager = OwnerCarController.GetComponent<WearObjectManager>();
            OwnerWeaponManager.InstallWeapon(this);
        }
    }
}
