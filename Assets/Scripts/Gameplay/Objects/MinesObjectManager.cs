﻿using System;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Serialization;

namespace CarsBattleRoyal
{
	public class MinesObjectManager : WearObjectController {
		[SerializeField] private GameObject minePrefab;

		public int maxMines = 5;
		[NonSerialized] public int CurrentMines;

		public float maxReloadTime = 1f;
		[NonSerialized] public float CurrentTime;
		
		private readonly RaycastHit[] _results = new RaycastHit[3];

		private void Start()
		{
			CurrentMines = maxMines;
			CurrentTime = maxReloadTime;
		}

		public void Update()
		{
			CurrentTime += Time.deltaTime;
		}
		
		
		public override void UseObject()
		{
			if(CurrentTime < maxReloadTime)
				return;

			if (Physics.RaycastNonAlloc(OwnerCarController.transform.position + 1 * Vector3.up, Vector3.down, _results,
				    10, 1 << GameConstants.TerrainLayer) == 0) return;

			Vector3? position = null;
			foreach (var rayCast in _results)
			{
				if (!rayCast.collider.CompareTag(GameConstants.TerrainTag)) continue;
				position = rayCast.point;
				break;
			}
			if(!position.HasValue)
				return;
			
			photonView.RPC("InstantiateMine", RpcTarget.All, position.Value);
			CurrentTime = 0;
		}

		protected override void OnCarEnter(CarController carController)
		{
			var gunManager = carController.GetComponent<WearObjectManager>();
			if (!gunManager) return;
			if (gunManager.ExistWeapon() && gunManager.CurrentWeapon.WeaponType != ObjectsType.Mines) return;

			if (gunManager.ExistWeapon())
			{
				((MinesObjectManager) gunManager.CurrentWeapon).CurrentMines += CurrentMines;
				
				if(!PhotonNetwork.IsMasterClient)
					return;
				PhotonNetwork.Destroy(gameObject);
			}
			else
			{
				base.OnCarEnter(carController);
			}
		}

		[PunRPC]
		public void InstantiateMine(Vector3 point)
		{
			if(!OwnerCarController)
				return;
			
			var mine = Instantiate(minePrefab, point, Quaternion.identity).GetComponent<MineController>();
			mine.SetTarget(OwnerCarController);
			
			CurrentMines--;
			if (CurrentMines <= 0)
			{
				OwnerWeaponManager.ThrowWeapon();
			}
		}
	}
}
