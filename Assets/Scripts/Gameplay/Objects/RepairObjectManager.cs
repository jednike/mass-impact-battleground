﻿namespace CarsBattleRoyal
{
    public class RepairObjectManager : WearObjectController {
        public override void UseObject()
        {
            OwnerCarController.GetComponent<HealthManager>().RestoreHp(0.4f);
            OwnerWeaponManager.ThrowWeapon();
        }

        protected override void OnCarEnter(CarController carController)
        {
            if (carController.GetComponent<WearObjectManager>().ExistWeapon()) return;
            base.OnCarEnter(carController);
        }
    }
}
