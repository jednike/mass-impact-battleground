﻿using System.Linq;
using UnityEngine;

namespace CarsBattleRoyal
{
	public class MineController : MonoBehaviour {
		private CarController _carController;
		
		[SerializeField] private float damage = 25f;
		[SerializeField] private float initTime = 3f;
		[SerializeField] private float radius = 40f;
		[SerializeField] private float power = 120000f;

		[SerializeField] private AudioClip explosionSound;

		private Transform _transform;
		private float _currentTime;

		private void Awake()
		{
			_transform = transform;
		}

		private void Update ()
		{
			_currentTime += Time.deltaTime;
		}

		public void SetTarget(CarController carController)
		{
			_carController = carController;
		}

		private void OnTriggerStay(Collider other)
		{
			if(_currentTime < initTime)
				return;
			
			var enemy = other.gameObject.GetComponentInParent<CarController>();
			if (enemy)
			{
				enemy.GetComponent<DamageCarManager>().DamageHp(damage, _carController.OwnerId);
				enemy.GetComponent<DamageSoundManager>().PlayDamageSound(explosionSound);
			}
			Destroy(Instantiate(InGamePrefabs.Instance.particles.GetRandomExplosion(), _transform.position, _transform.rotation), 5);
			Destroy(gameObject);
		}

		private void OnDestroy()
		{
			var explosionPos = transform.position;
			var list = (from player in GameObject.FindGameObjectsWithTag(GameConstants.CarTag) where Vector3.Distance(player.transform.position, explosionPos) < radius select player.GetComponent<Rigidbody>()).ToList();
			foreach (var rb in list)
				rb.AddExplosionForce(power * 300, explosionPos, radius, 3.0F);
		}
	}
}
