﻿using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class CheckOnOnline : MonoBehaviour
	{
		private Button _button;

		private bool _isActive = true;

		private void Awake()
		{
			_button = GetComponent<Button>();
		}

		private void Update () {
			_button.interactable = Application.internetReachability != NetworkReachability.NotReachable;
		}
	}
}
