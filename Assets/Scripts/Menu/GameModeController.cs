﻿using UnityEngine;

namespace CarsBattleRoyal
{
	public class GameModeController : MonoBehaviour
	{
		[SerializeField] private Animator _gameModeAnimator;

		public void ShowOrHide()
		{
			_gameModeAnimator.SetBool("Showed", !_gameModeAnimator.GetBool("Showed"));
			if(!_gameModeAnimator.GetBool("Showed"))
				_gameModeAnimator.Play("Hide");
			else
				_gameModeAnimator.Play("Show");
		}
	}
}
