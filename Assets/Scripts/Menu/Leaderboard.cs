﻿using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class Leaderboard : MonoBehaviour
	{
		[SerializeField] private Text _number;
		[SerializeField] private Text _nickname;
		[SerializeField] private Text _points;

		private static readonly Color PlayerColor = Color.red;

		public void SetInfo(int number, string nickname, int points, bool isPlayer)
		{
			_number.text = number.ToString();
			_nickname.text = nickname;
			_points.text = points.ToString();
			if(!isPlayer)
				return;
			_number.color = PlayerColor;
			_nickname.color = PlayerColor;
			_points.color = PlayerColor;
		}

		public void Empty()
		{
			_number.text = "";
			_nickname.text = "";
			_points.text = "";
		}
	}
}
