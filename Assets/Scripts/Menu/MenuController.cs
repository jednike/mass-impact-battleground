﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class MenuController : MonoBehaviour
	{
		[Header("Nickname")]
		[SerializeField] private GameObject connectPanel;
	
		[Header("Connecting")]
		[SerializeField] private GameObject connectingPanel;

		[Header("Error")]
		[SerializeField] private GameObject errorConnectMessage;

		[Header("Leaderboard")]
		[SerializeField] private GameObject leaderboardPanel;

		[Header("Settings")]
		[SerializeField] private GameObject settingsPanel;

		private void Awake()
		{
			GameControllers.OnGameStateChanged += OnGameStateChanged;
			OnNotificationActionChanged();
			
			AdController.Cache(AdType.Interstitial);
			GameControllers.GameState = GameState.MainMenu;

			if (Scores.Instance.Nickname == "" || Scores.Instance.Nickname == "Default")
				Scores.Instance.Nickname = "User" + Random.Range(0, 99999);
		}

		private void OnDestroy()
		{
			GameControllers.OnGameStateChanged -= OnGameStateChanged;
		}

		private void OnNotificationActionChanged()
		{
			switch (NotificationController.NotificationAction)
			{
				case NotificationAction.OpenGarage:
					NotificationController.NotificationAction = NotificationAction.None;
					GameControllers.GameState = GameState.Garage;
					break;
			}
		}

		private void OnGameStateChanged(GameState lastState, GameState newState)
		{
			switch (lastState)
			{
				case GameState.MainMenu:
					connectPanel.SetActive(false);
					break;
				case GameState.ConnectingToServer:
				case GameState.ConnectingToRoom:
				case GameState.WaitingPlayers:
				case GameState.WaitingStartGame:
				case GameState.BeforeStart:
					connectingPanel.SetActive(false);
					break;
				case GameState.GamePlay:
					break;
				case GameState.Pause:
					break;
				case GameState.Settings:
					settingsPanel.SetActive(false);
					break;
				case GameState.LoadGame:
					break;
				case GameState.LoadMenu:
					break;
				case GameState.None:
					break;
				case GameState.ConnectError:
					errorConnectMessage.SetActive(false);
					break;
				case GameState.Leaderboard:
					leaderboardPanel.SetActive(false);
					break;
				default:
					break;
			}
			switch (newState)
			{
				case GameState.MainMenu:
					connectPanel.SetActive(true);
					break;
				case GameState.ConnectingToServer:
				case GameState.ConnectingToRoom:
				case GameState.WaitingPlayers:
				case GameState.WaitingStartGame:
				case GameState.BeforeStart:
					connectingPanel.SetActive(true);
					break;
				case GameState.GamePlay:
					break;
				case GameState.Pause:
					break;
				case GameState.Settings:
					settingsPanel.SetActive(true);
					break;
				case GameState.LoadGame:
					break;
				case GameState.LoadMenu:
					break;
				case GameState.Garage:
					SceneManager.LoadScene("GarageScene");
					AnalyticsManager.LogEvent("OpenGarage");
					break;
				case GameState.Safe:
					SceneManager.LoadScene("LootBoxScene");
					AnalyticsManager.LogEvent("OpenSafe");
					break;
				case GameState.None:
					break;
				case GameState.ConnectError:
					errorConnectMessage.SetActive(true);
					break;
				case GameState.Leaderboard:
					leaderboardPanel.SetActive(true);
					break;
				default:
					break;
			}
		}
	}
}
