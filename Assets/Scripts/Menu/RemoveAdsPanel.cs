﻿using UnityEngine;

namespace CarsBattleRoyal
{
    public class RemoveAdsPanel : MonoBehaviour
    {
        private void OnEnable()
        {
            BuyManager.BuyRemoveAdsComplete += ShowRemoveAdsWindow;
        }

        private void ShowRemoveAdsWindow(bool show)
        {
            gameObject.SetActive(show);
        }

        private void OnDisable()
        {
            BuyManager.BuyRemoveAdsComplete -= ShowRemoveAdsWindow;
        }
        
        public void TryBuy()
        {
            BuyManager.BuyProduct(GameConstants.RemoveAdsProductId);
        }

        public void TryRestore()
        {
            BuyManager.RestoreProducts();
        }

        public void ClosePanel()
        {
            gameObject.SetActive(false);
        }
    }
}