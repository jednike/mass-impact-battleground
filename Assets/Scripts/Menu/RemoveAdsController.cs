﻿using System;
using UnityEngine;

namespace CarsBattleRoyal
{
	public class RemoveAdsController : MonoBehaviour
	{
		[SerializeField] private GameObject _removeAdsWindow;

		private void Awake()
		{
			OnNotificationActionChanged();
		}

		private void OnNotificationActionChanged()
		{
			switch (NotificationController.NotificationAction)
			{
				case NotificationAction.OpenVipWindow:
					NotificationController.NotificationAction = NotificationAction.None;
					ShowRemoveAdsWindow(true);
					break;
			}
		}

		private int _totalClicks;
		
		private void OnEnable()
		{
			BuyManager.BuyRemoveAdsComplete += ShowRemoveAdsWindow;
			if(Scores.Instance.VipBuyed)
				gameObject.SetActive(false);
		}

		public void ShowRemoveAdsWindow(bool show)
		{
			_totalClicks = 0;
			gameObject.SetActive(show);
			_removeAdsWindow.SetActive(show);
		}

		private void OnDisable()
		{
			BuyManager.BuyRemoveAdsComplete -= ShowRemoveAdsWindow;
		}

		public void TryDisable()
		{
			_totalClicks++;
			if (_totalClicks < 1001) return;

			Scores.Instance.VipBuyed = true;
			ShowRemoveAdsWindow(false);
			gameObject.SetActive(false);
		}
	}
}
