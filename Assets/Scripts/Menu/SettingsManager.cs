﻿using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class SettingsManager : MonoBehaviour
	{
		[Header("SoftLanding")]
		[SerializeField] private Button _onSoftLanding;
		[SerializeField] private Button _offSoftLanding;

		[Header("Auto Accelerate")]
		[SerializeField] private Button _onAutoAccelLanding;
		[SerializeField] private Button _offAutoAccelLanding;

		[Header("Selected Target")]
		[SerializeField] private Button _onSelectedTarget;
		[SerializeField] private Button _offSelectedTarget;

		[Header("Languages")]
		[SerializeField] private Button _english;
		[SerializeField] private Button _russian;

		private void Awake ()
		{
			Init();
		}

		private void Init()
		{
			_onSoftLanding.interactable = !Scores.Instance.SoftLanding;
			_offSoftLanding.interactable = Scores.Instance.SoftLanding;
			
			_onAutoAccelLanding.interactable = !Scores.Instance.AutoAccelerate;
			_offAutoAccelLanding.interactable = Scores.Instance.AutoAccelerate;
			
			_onSelectedTarget.interactable = !Scores.Instance.SelectedTarget;
			_offSelectedTarget.interactable = Scores.Instance.SelectedTarget;

			_english.interactable = Scores.Instance.CurrentLanguage != "English";
			_russian.interactable = Scores.Instance.CurrentLanguage != "Russian";
		}

		public void ChangeSofLanding()
		{
			Scores.Instance.SoftLanding = !Scores.Instance.SoftLanding;
			Init();
		}
		public void ChangeAutoAccelerate()
		{
			Scores.Instance.AutoAccelerate = !Scores.Instance.AutoAccelerate;
			Init();
		}
		public void ChangeSelectedTarget()
		{
			Scores.Instance.SelectedTarget = !Scores.Instance.SelectedTarget;
			Init();
		}
		public void ChangeLanguage()
		{
			Scores.Instance.CurrentLanguage = Scores.Instance.CurrentLanguage != "English" ? "English": "Russian";
			Init();
		}

		public void Back()
		{
			GameControllers.GameState = GameState.MainMenu;
		}
	}
}
