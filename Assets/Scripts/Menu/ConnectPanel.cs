﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
    public class ConnectPanel : MonoBehaviour
    {
        [SerializeField] private InputField nicknameText;
        [SerializeField] private Text prefixText;
        
        private void Start()
        {
            switch (Scores.Instance.UserType)
            {
                case UserType.User:
                    prefixText.gameObject.SetActive(false);
                    break;
                case UserType.Tester:
                    prefixText.gameObject.SetActive(true);
                    prefixText.text = "[T]";
                    break;
                case UserType.Developer:
                    prefixText.gameObject.SetActive(true);
                    prefixText.text = "[D]";
                    break;
            }
        }

        private void OnEnable()
        {
            nicknameText.text = Scores.Instance.Nickname;
        }
        
        public void StartGame()
        {
            AdController.ShowInterAds("start_game");
            GameControllers.GameState = GameState.ConnectingToServer;
            AnalyticsManager.LogEvent("ClickOnline");
        }
        public void StartOfflineGame()
        {
            AdController.ShowInterAds("start_game");
            SceneManager.LoadScene("LoadScene");
            AnalyticsManager.LogEvent("ClickOffline");
        }
        
        public void OnEndEditNickname(string nickname)
        {
            Scores.Instance.Nickname = nickname;
            AnalyticsManager.LogEvent("NicknameEdit");
            NetworkRequests.Instance.SetNickname();
        }

        public void OpenSocNetwork(bool fb)
        {
            if (fb)
            {
                Application.OpenURL("https://fb.me/massimpactb");
                AnalyticsManager.LogEvent("ClickFB");
            }
            else
            {
                Application.OpenURL("https://vk.com/mass_impact");
                AnalyticsManager.LogEvent("ClickVK");
            }
        }
    }
}
