﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class SafeController : MonoBehaviour
	{
		private Button _button;
		
		[SerializeField] private Sprite _enabledSafeImage;
		[SerializeField] private Sprite _disabledSafeImage;
		[SerializeField] private Image _safeImage;

		[SerializeField] private Text _targetText;
		[SerializeField] private Image _targetImage;
		[SerializeField] private Text _timeText;

		private LootBoxInfo _lootBoxInfo;
		private DateTime OpenTime;
		
		private IEnumerator Start()
		{
			_button = GetComponent<Button>();
								
			_button.interactable = false;
			_safeImage.sprite = _disabledSafeImage;
			
			_targetText.text = string.Format("{0}/{1}", 0, 0);
			_targetImage.fillAmount = 0;
			
			if(string.IsNullOrEmpty(Scores.Instance.UniqueId))
				yield break;
			
			var form = new WWWForm();
			form.AddField("UniqueId", Scores.Instance.UniqueId);
			
			StartCoroutine(NetworkRequests.SendRequest("get_lootbox_info", form, (success, response) =>
			{
				if (!success)
				{
					Debug.LogError(response);
					return;
				}
				
				_lootBoxInfo = JsonUtility.FromJson<LootBoxInfo>(response);
				if (!_lootBoxInfo.Success)
				{
					Debug.LogError(_lootBoxInfo.Message);
					return;
				}

				_targetText.gameObject.SetActive(!_lootBoxInfo.Opened);
				_targetImage.gameObject.SetActive(!_lootBoxInfo.Opened);
				_timeText.gameObject.SetActive(_lootBoxInfo.Opened);

				var safeExist = _lootBoxInfo.Kills >= _lootBoxInfo.TargetKills && !_lootBoxInfo.Opened;
			
				_button.interactable = safeExist;
				_safeImage.sprite = safeExist ? _enabledSafeImage: _disabledSafeImage;
				
				if (!_lootBoxInfo.Opened)
				{
					_targetText.text = string.Format("{0}/{1}", Mathf.Clamp(_lootBoxInfo.Kills, 0, _lootBoxInfo.TargetKills), _lootBoxInfo.TargetKills);
					_targetImage.fillAmount = (float) _lootBoxInfo.Kills / _lootBoxInfo.TargetKills;
				}
				else
				{
					OpenTime = DateTime.Now.AddSeconds(_lootBoxInfo.TimeBeforeOpen);
				}
			}));
		}

		private void Update()
		{
			if(_lootBoxInfo == null || !_lootBoxInfo.Opened)
				return;
			var time = OpenTime - DateTime.Now;
			_timeText.text = string.Format("{00:00}:{01:00}:{02:00}", time.Hours, time.Minutes, time.Seconds);

			if (DateTime.Now <= OpenTime) return;
			
			_lootBoxInfo = null;
			_timeText.text = string.Format("{00:00}:{01:00}:{02:00}", 0, 0, 0);
			StartCoroutine(Start());
		}
	}
}
