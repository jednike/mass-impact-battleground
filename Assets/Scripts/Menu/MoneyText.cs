﻿using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class MoneyText : MonoBehaviour
	{
		private Text _text;
		
		private void Awake()
		{
			_text = GetComponent<Text>();
			_text.text = Scores.Instance.Money.ToString();
		}

		private void Update()
		{
			_text.text = Scores.Instance.Money.ToString();
		}
	}
}
