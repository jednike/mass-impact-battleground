﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{

	public class LeaderboardManager : MonoBehaviour
	{
		[SerializeField] private Text _seasonText;
		[SerializeField] private Text _numberText;

		[SerializeField] private Leaderboard[] _first;
		[SerializeField] private Leaderboard[] _second;
		
		private Leaders _leaders;

		private void Start()
		{
			CheckState();
			CheckWins();
		}

		private void CheckState()
		{
			if (_first != null && _second != null)
			{
				foreach (var lead in _first)
					lead.Empty();
				foreach (var lead in _second)
					lead.Empty();
			}

			_seasonText.text = string.Format("{0} {1}\n{2}{3} {4}",
				MultiLanguageManager.GetTranslateString("Season", "Season", _seasonText), "0",
				MultiLanguageManager.GetTranslateString("Ends in", "Ends in:", _seasonText), 0,
				MultiLanguageManager.GetTranslateString("Days", "days", _seasonText));
			_numberText.text = string.Format("#{0}/{1}", 0, 0);

			if(string.IsNullOrEmpty(Scores.Instance.UniqueId))
				return;

			var form = new WWWForm();
			form.AddField("UniqueId", Scores.Instance.UniqueId);
			
			StartCoroutine(NetworkRequests.SendRequest("get_rank", form, (success, response) =>
			{
				if (!success)
				{
					Debug.LogError(response);
					return;
				}

				_leaders = JsonUtility.FromJson<Leaders>(response);

				if (!_leaders.Success)
				{
					Debug.LogError(_leaders.Message);
					return;
				}

				_seasonText.text = string.Format("{0} {1}\n{2}{3} {4}",
					MultiLanguageManager.GetTranslateString("Season", "Season", _seasonText), _leaders.SeasonName,
					MultiLanguageManager.GetTranslateString("Ends in", "Ends in:", _seasonText), _leaders.Days,
					MultiLanguageManager.GetTranslateString("Days", "days", _seasonText));
				_numberText.text = string.Format("#{0}/{1}", _leaders.Number == -1 ? _leaders.TotalNumber : _leaders.Number + 1, _leaders.TotalNumber);

				if (_first == null || _first.Length == 0 || _second == null || _second.Length == 0) return;
				
				for (var i = 0; i < _leaders.First.Length; i++)
				{
					var lead = _leaders.First[i];
					_first[i].SetInfo(lead.Number + 1, lead.Nickname, lead.Points, lead.Number == _leaders.Number);
				}

				for (var i = 0; i < _leaders.Second.Length; i++)
				{
					var lead = _leaders.Second[i];
					_second[i].SetInfo(lead.Number + 1, lead.Nickname, lead.Points, lead.Number == _leaders.Number);
				}
			}));
		}

		private void CheckWins()
		{
			if(string.IsNullOrEmpty(Scores.Instance.UniqueId))
				return;
			
			var form = new WWWForm();
			form.AddField("UniqueId", Scores.Instance.UniqueId);
			
			StartCoroutine(NetworkRequests.SendRequest("i_am_winner", form, (success, response) =>
			{
				if (!success)
				{
					Debug.LogError(response);
					return;
				}

				var winsMessage = JsonUtility.FromJson<WinsMessage>(response);
				if (!winsMessage.Success)
				{
					return;
				}

				foreach (var season in winsMessage.Wins)
				{
					var carName = "Inferno";
					foreach (var car in GameConstants.SeasonCars)
					{
						if (car.Value != season) continue;
						carName = car.Key;
						break;
					}
                
					var carModelByName = InGamePrefabs.Instance.cars.GetCarModelByName(carName);
					if(carModelByName.Buyed)
						continue;
					carModelByName.Buyed = true;
					Scores.Instance.CurrentCar = carName;
					NotificationController.NotificationAction = NotificationAction.OpenGarage;
				}
			}));
		}

		public void ToBack()
		{
			GameControllers.GameState = GameState.MainMenu;
		}
	}
}
