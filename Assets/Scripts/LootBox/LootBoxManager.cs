﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace CarsBattleRoyal
{
	public enum AwardType
	{
		Skin,
		Car,
		Coins
	}

	public class LootBoxManager : MonoBehaviour
	{
		[SerializeField] private Animator lootBoxGameObject;

		[Header("UI")]
		[SerializeField] private GameObject awardUi;
		[SerializeField] private Image awardImage;
		[SerializeField] private Text awardTypeText;

		private AssetBundle _bundle;

		private void Awake()
		{
			_bundle = AssetBundle.LoadFromFile($"{Application.streamingAssetsPath}/ui_loot");
		}

		private void Start()
		{
			var form = new WWWForm();
			form.AddField("UniqueId", Scores.Instance.UniqueId);
			
			StartCoroutine(NetworkRequests.SendRequest("open_lootbox", form, (success, response) =>
			{
				if (!success)
				{
					Debug.LogError(response);
					return;
				}
			
				var message = JsonUtility.FromJson<DefaultMessage>(response);
				if (message.Success)
				{
					StartCoroutine(OpenLootBox());
					return;
				}
				
				Debug.LogError(message.Message);
				ToMenu();
			}));
		}

		private void OnEnable()
		{
			GameControllers.OnGameStateChanged += OnGameStateChanged;
		}
		private void OnDisable()
		{
			GameControllers.OnGameStateChanged -= OnGameStateChanged;
		}

		private void OnGameStateChanged(GameState lastState, GameState newState)
		{
			switch (newState)
			{
				case GameState.LoadMenu:
					SceneManager.LoadScene("MenuScene");
					break;
			}
		}

		private IEnumerator OpenLootBox()
		{
			var startSkinsCount = GetAvailableByType(AwardType.Skin).Length;
			var skins = GetAvailableByType(AwardType.Skin, true);
			var cars = GetAvailableByType(AwardType.Car, true);

			AwardFromLootBox randomLoot;

			var random = Random.Range(0f, 1f);
			if (cars.Length > 0 && random <= 0.02f * cars.Length)
				randomLoot = cars[Random.Range(0, cars.Length)];
			else if (skins.Length > 0 && random <= 0.6f * ((float) skins.Length / startSkinsCount))
				randomLoot = skins[Random.Range(0, skins.Length)];
			else
				randomLoot = new AwardFromLootBox {ResourceName = "LootCoins", AwardType = AwardType.Coins};


			if (randomLoot.AwardType != AwardType.Coins)
			{
				var earnedLoot =
					new List<string>(Scores.Instance.EarnedLoot == null ? new[] {""} : Scores.Instance.EarnedLoot);
				earnedLoot.Add(randomLoot.ResourceName);
				Scores.Instance.EarnedLoot = earnedLoot.ToArray();
			}

			awardImage.sprite = _bundle.LoadAsset<Sprite>(randomLoot.ResourceName);
			awardTypeText.text = MultiLanguageManager.GetTranslateString(randomLoot.AwardType.ToString(),
				randomLoot.AwardType.ToString(), awardTypeText);

			while (lootBoxGameObject.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.8f)
			{
				yield return null;
			}

			switch (randomLoot.AwardType)
			{
				case AwardType.Car:
					Scores.Instance.GetCarByName(randomLoot.CarName).Buyed = true;
					break;
				case AwardType.Coins:
					Scores.Instance.Money += Random.Range(200, 250);
					break;
			}

			awardUi.SetActive(true);
		}

		public static AwardFromLootBox[] GetAvailableByType(AwardType type, bool needCheck = false)
		{
			var earnedLoot = new List<string>(Scores.Instance.EarnedLoot == null ? new []{""}: Scores.Instance.EarnedLoot);
			if (needCheck)
				return InGamePrefabs.Instance.loot.Where(loot => loot.AwardType == type && !earnedLoot.Contains(loot.ResourceName)).ToArray();
			else
				return InGamePrefabs.Instance.loot.Where(loot => loot.AwardType == type).ToArray();
		}

		public void ToMenu()
		{
			GameControllers.GameState = GameState.LoadMenu;
		}

		private void OnDestroy()
		{
			_bundle.Unload(true);
		}
	}
}
