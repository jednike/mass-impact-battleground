﻿using System.Collections;
using CarsBattleRoyal;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadManager : MonoBehaviour
{
	[SerializeField] private GameObject _gdprWindow;
	[SerializeField] private GameObject _notification;
	private float _time = 0;
	
	private bool _needLoad;
	private bool _loading;

	private void OnEnable()
	{
		LoadSaveManager.OnSaveLoadFromCloud += OnLoadFromCloud;
	}

	private void OnLoadFromCloud()
	{
		Scores.MaySave = true;
		_needLoad = true;
	}

	private void OnDisable()
	{
		LoadSaveManager.OnSaveLoadFromCloud -= OnLoadFromCloud;
	}

	private void Awake()
	{
		if (Scores.Instance.LicenseApplied) return;
		_gdprWindow.SetActive(true);
	}
	
	private IEnumerator StartLoadGame ()
	{
		yield return CheckUser();
		yield return null;
		
		_notification.SetActive(true);
		AdController.ShowInterAds("first_screen");
		SceneManager.LoadSceneAsync("MenuScene");
	}
	
	private IEnumerator CheckUser()
	{
		var nickname = Scores.Instance.Nickname;
		var uniqueId = Scores.Instance.UniqueId;
		if (string.IsNullOrEmpty(uniqueId))
			uniqueId = Utils.RandomString();
				
		var form = new WWWForm();
		form.AddField("UniqueId", uniqueId);
		form.AddField("Nickname", nickname);
		
		yield return StartCoroutine(NetworkRequests.SendRequest("check_or_add_user", form, (success, response) =>
		{
			if (!success)
			{
				Debug.LogError(response);
				return;
			}
			
			var message = JsonUtility.FromJson<DefaultMessage>(response);
			if (!message.Success)
			{
				Debug.LogError(message.Message);
			}
			if (string.IsNullOrEmpty(Scores.Instance.UniqueId))
				Scores.Instance.UniqueId = uniqueId;
			NetworkRequests.Instance.SetNickname();
		}));
		yield return StartCoroutine(NetworkRequests.SendRequest("get_user_type", form, (success, response) =>
		{
			if (!success)
			{
				Debug.LogError(response);
				return;
			}
			
			var message = JsonUtility.FromJson<UserTypeMessage>(response);
			if (!message.Success)
			{
				Debug.LogError(message.Message);
			}

			Scores.Instance.UserType = (UserType) message.UserType;
		}));
	}

	private void Update()
	{
		_time += Time.deltaTime;
		if (_time > 30)
			OnLoadFromCloud();
		
		if (!_needLoad || _loading || !Scores.Instance.LicenseApplied)
			return;
		_loading = true;
		
		StartCoroutine(StartLoadGame());
	}
}
