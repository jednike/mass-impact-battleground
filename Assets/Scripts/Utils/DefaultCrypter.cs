using System;
using System.Security.Cryptography;
using System.Text;

namespace CarsBattleRoyal
{
    public class DefaultCrypter: ICrypter
    {
        private static readonly string CryptoKey = "Ahiu32sdj";

        public string Encrypt(string toEncrypt)
        {
            var keyArray = Encoding.UTF8.GetBytes(CryptoKey);
            var toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);
            var rDel = new RijndaelManaged
            {
                Key = keyArray,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            var cTransform = rDel.CreateEncryptor();
            var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public string Decrypt(string toDecrypt)
        {
            var keyArray = Encoding.UTF8.GetBytes(CryptoKey);
            var toEncryptArray = Convert.FromBase64String(toDecrypt);
            var rDel = new RijndaelManaged
            {
                Key = keyArray,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            var cTransform = rDel.CreateDecryptor();
            var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return Encoding.UTF8.GetString(resultArray);
        }
    }
}