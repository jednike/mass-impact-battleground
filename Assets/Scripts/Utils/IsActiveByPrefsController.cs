﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
	public class IsActiveByPrefsController : MonoBehaviour
	{
		[SerializeField] private AudioMixer _masterMixer;
		[SerializeField] private Sprite _activeSprite;
		[SerializeField] private Sprite _inActiveSprite;

		[SerializeField] private bool _isMusic;
		
		private Image _image;
		private Button _button;

		private void Awake()
		{
			_image = GetComponent<Image>();
			_button = GetComponent<Button>();
			SetChange();
		}

		public void OnClick()
		{
			if(_isMusic)
				Scores.Instance.Music = !Scores.Instance.Music;
			else
				Scores.Instance.Sounds = !Scores.Instance.Sounds;
			
			SetChange();
		}

		private void SetChange()
		{
			var currentState = _isMusic ? Scores.Instance.Music : Scores.Instance.Sounds;
			
			_image.sprite = currentState ? _activeSprite : _inActiveSprite;
			_masterMixer.SetFloat(_isMusic ? "MusicVolume" : "SoundsVolume", currentState ? 0 : -80);
		}
	}
}
