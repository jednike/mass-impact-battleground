﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing.MiniJSON;

namespace CarsBattleRoyal
{
    public enum NotificationAction
    {
        None,
        OpenGarage,
        OpenVipWindow,
    }
    public class NotificationController : GlobalSingletonBehaviour<NotificationController>
    {
        public static NotificationAction NotificationAction = NotificationAction.None;

        public override void DoAwake()
        {
            OneSignal.StartInit("74b9d030-4d6b-49fa-9b9c-f7ec6ffd2b36")
                .HandleNotificationOpened(HandleNotificationOpened)
                .HandleNotificationReceived(HandleNotificationReceived)
                .EndInit();
            OneSignal.SendTag("UniqueId", Scores.Instance.UniqueId);
  
            OneSignal.inFocusDisplayType = OneSignal.OSInFocusDisplayOption.Notification;
        }

        private void Start () {  
            if (!Scores.Instance.VipBuyShowed)
            {
                Scores.Instance.VipBuyShowed = true;
                if(!Scores.Instance.VipBuyed)
                    VipMessage();
            }

            if (LoadSaveManager.Instance.IsLogin())
            {
                OneSignal.SendTag("nickname", Social.localUser.userName);
            }
        }

        public void VipMessage()
        {
            OneSignal.IdsAvailable((userId, pushToken) =>
            {
                if (pushToken != null)
                {
                    var notification = new Dictionary<string, object>();

                    notification["include_player_ids"] = new List<string> {userId};
                    notification["contents"] = new Dictionary<string, string>
                    {
                        {"en", "Do you want a premium car, 1000 coins and disable ADS? We have a special offer for you!"},
                        {"ru", "Хочешь премиум-тачки, 1000 монет и отключение рекламы? У нас есть предложение для тебя!"}
                    };
                    notification["headings"] = new Dictionary<string, string> {{"en", "The special offer for you!"}, {"ru", "Специальное предложение для тебя!"}};
                    notification["send_after"] = DateTime.Now.ToUniversalTime().AddDays(1).ToString("U");
                    notification["data"] = new Dictionary<string, object>{{"buy_vip", ""}};

                    OneSignal.PostNotification(notification,
                        responseSuccess => { },
                        responseFailure => { });
                }
                else
                {
                    Debug.Log("ERROR: Device is not registered.");
                }
            });
        }
        public void ManyNotPlayed()
        {
            OneSignal.IdsAvailable((userId, pushToken) =>
            {
                if (pushToken != null)
                {
                    var notification = new Dictionary<string, object>();

                    notification["include_player_ids"] = new List<string> {userId};
                    notification["contents"] = new Dictionary<string, string>
                    {
                        {"en", "You have not played for a long time"},
                        {"ru", "Ты уже долго не играл"}
                    };
                    notification["headings"] = new Dictionary<string, string> {{"en", "Long time no see"}, {"ru", "Давно не виделись"}};
                    notification["send_after"] = DateTime.Now.ToUniversalTime().AddDays(10).ToString("U");

                    OneSignal.PostNotification(notification,
                        responseSuccess => { },
                        responseFailure => { });
                }
                else
                {
                    Debug.Log("ERROR: Device is not registered.");
                }
            });
        }

        public void PresentCar()
        {
            OneSignal.IdsAvailable((userId, pushToken) =>
            {
                if (pushToken != null)
                {
                    var notification = new Dictionary<string, object>();

                    notification["include_player_ids"] = new List<string> {userId};
                    notification["contents"] = new Dictionary<string, string>
                    {
                        {"en", "Especially for you! Car as a gift! Launch the game!"},
                        {"ru", "Специально для тебя! Тачка в подарок! Заходи в игру!"}
                    };
                    notification["headings"] = new Dictionary<string, string> {{"en", "Gifts for you!"}, {"ru", "Подарок для тебя!"}};
                    notification["send_after"] = DateTime.Now.ToUniversalTime().AddSeconds(30).ToString("U");
                    notification["data"] = new Dictionary<string, object>{{"present_car", "Amalgam"}};

                    OneSignal.PostNotification(notification,
                        responseSuccess => { },
                        responseFailure => { });
                }
                else
                {
                    Debug.Log("ERROR: Device is not registered.");
                }
            });
        }
        public void PresentVIP()
        {
            OneSignal.IdsAvailable((userId, pushToken) =>
            {
                if (pushToken != null)
                {
                    var notification = new Dictionary<string, object>();

                    notification["include_player_ids"] = new List<string> {userId};
                    notification["contents"] = new Dictionary<string, string>
                    {
                        {"en", "We give you VIP! Launch the game and ride new cars!"},
                        {"ru", "Мы дарим тебе VIP! Заходи в игру и катайся на новых тачках!"}
                    };
                    notification["headings"] = new Dictionary<string, string> {{"en", "Gifts for you!"}, {"ru", "Подарок для тебя!"}};
                    notification["send_after"] = DateTime.Now.ToUniversalTime().AddSeconds(35).ToString("U");
                    notification["data"] = new Dictionary<string, object>{{"present_vip", "true"}};

                    OneSignal.PostNotification(notification,
                        responseSuccess => { },
                        responseFailure => { });
                }
                else
                {
                    Debug.Log("ERROR: Device is not registered.");
                }
            });
        }

        private static void HandleNotificationOpened(OSNotificationOpenedResult result)
        {
            var payload = result.notification.payload;
            var additionalData = payload.additionalData;

            Debug.Log("additionalData " + additionalData.toJson());
            if (additionalData == null) return;

            var cars = InGamePrefabs.Instance.cars;
            if (additionalData.ContainsKey("present_car"))
            {
                var carModelByName = cars.GetCarModelByName((string) additionalData["present_car"]);
                carModelByName.Buyed = true;
                Scores.Instance.CurrentCar = (string) additionalData["present_car"];
                NotificationAction = NotificationAction.OpenGarage;
            }
            if (additionalData.ContainsKey("SeasonId"))
            {
                var season = (int) additionalData["SeasonId"];
                var carName = "Inferno";
                foreach (var car in GameConstants.SeasonCars)
                {
                    if (car.Value != season) continue;
                    carName = car.Key;
                    break;
                }
                
                var carModelByName = cars.GetCarModelByName(carName);
                carModelByName.Buyed = true;
                Scores.Instance.CurrentCar = carName;
                NotificationAction = NotificationAction.OpenGarage;
            }

            if (additionalData.ContainsKey("present_vip"))
            {
                Scores.Instance.Money += 1000;
                Scores.Instance.VipBuyed = true;
                BuyManager.BuyRemoveAds(false);
            }

            if (additionalData.ContainsKey("buy_vip"))
            {
                NotificationAction = NotificationAction.OpenVipWindow;
            }
        }

        private void HandleNotificationReceived(OSNotification notification)
        {
            
        }
    }
}
