﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    public static class GameObjectExtensions
    {
        public static void ChangeLayer(this GameObject parentObject, int layer)
        {
            var children = parentObject.GetComponentsInChildren<Transform>(true);
            foreach (var go in children)
            {
                go.gameObject.layer = layer;
            }
        }
    }
}