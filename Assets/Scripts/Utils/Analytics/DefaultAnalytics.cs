using System.Collections.Generic;
using UnityEngine.Analytics;

namespace CarsBattleRoyal
{
    public class DefaultAnalytics: IAnalytics
    {
        public void LogEvent(string eventName)
        {
            Analytics.CustomEvent(eventName);
        }

        public void LogEvent(string eventName, string parameterName, int parameterValue)
        {
            Analytics.CustomEvent(eventName, new Dictionary<string, object>{{parameterName, parameterValue}});
        }

        public void LogEvent(string eventName, string parameterName, string parameterValue)
        {
            Analytics.CustomEvent(eventName, new Dictionary<string, object>{{parameterName, parameterValue}});
        }
    }
}