#if Firebase
using Firebase;
using Firebase.Analytics;

namespace CarsBattleRoyal
{
    public class FirebaseAnalytic: IAnalytics
    {
        public void LogEvent(string eventName)
        {
            FirebaseAnalytics.LogEvent(eventName);
        }

        public void LogEvent(string eventName, string parameterName, int parameterValue)
        {
            FirebaseAnalytics.LogEvent(eventName, parameterName, parameterValue);
        }

        public void LogEvent(string eventName, string parameterName, string parameterValue)
        {
            FirebaseAnalytics.LogEvent(eventName, parameterName, parameterValue);
        }
    }
}
#endif