namespace CarsBattleRoyal
{
    public interface IAnalytics
    {
        void LogEvent(string eventName);
        void LogEvent(string eventName, string parameterName, int parameterValue);
        void LogEvent(string eventName, string parameterName, string parameterValue);
    }
}