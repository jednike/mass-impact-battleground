﻿using System.Collections.Generic;
using CarsBattleRoyal;
using UnityEngine;

public class AnalyticsManager : MonoBehaviour
{
	private static IEnumerable<IAnalytics> _analytics = new[] {new DefaultAnalytics()};
	public static void LogEvent(string eventName)
	{
		foreach (var analytic in _analytics)
		{
			analytic.LogEvent(eventName);
		}
	}
	
	public static void LogEvent(string eventName, string parameterName, int parameterValue)
	{
		foreach (var analytic in _analytics)
		{
			analytic.LogEvent(eventName, parameterName, parameterValue);
		}
	}
	
	public static void LogEvent(string eventName, string parameterName, string parameterValue)
	{
		foreach (var analytic in _analytics)
		{
			analytic.LogEvent(eventName, parameterName, parameterValue);
		}
	}
}
