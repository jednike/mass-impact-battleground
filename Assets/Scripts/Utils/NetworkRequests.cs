﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace CarsBattleRoyal
{
	public class NetworkRequests : GlobalSingletonBehaviour<NetworkRequests> {

		public static IEnumerator SendRequest(string methodName, WWWForm form, Action<bool, string> action)
		{
			var www = UnityWebRequest.Post(string.Format("{0}post/{1}.php", GameConstants.LeaderBoardUrl, methodName), form);
		
			www.chunkedTransfer = false; 
			www.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		
			yield return www.SendWebRequest();

			if (www.isNetworkError || www.isHttpError)
				action(false, www.error);
			else
				action(true, www.downloadHandler.text);
		}
	
		// Часто используемые запросы
		public void SetNickname()
		{
			var form = new WWWForm();
			form.AddField("UniqueId", Scores.Instance.UniqueId);
			form.AddField("Nickname", Scores.Instance.Nickname);

			StartCoroutine(NetworkRequests.SendRequest("set_nickname", form, (success, response) =>
			{
				if (!success)
				{
					Debug.LogError(response);
					return;
				}

				var message = JsonUtility.FromJson<DefaultMessage>(response);
				if (!message.Success) Debug.LogError(message.Message);
			}));
		}
	}
}
