using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CarsBattleRoyal
{
    public class AssetBundleGameSceneLoader: MonoBehaviour, IGameSceneLoader
    {
        private AsyncOperation _async;
        public void LoadScene(string sceneName)
        {
            StartCoroutine(LoadSceneAsset(sceneName));
        }

        private IEnumerator LoadSceneAsset(string sceneName)
        {
            var async = AssetBundle.LoadFromFileAsync(Application.streamingAssetsPath + "/" + sceneName.ToLower());
            while (!async.isDone)
            {
                yield return null;
            }
            var scenePath = async.assetBundle.GetAllScenePaths();
            _async = SceneManager.LoadSceneAsync(scenePath[0]);
        }

        public bool SceneLoading()
        {
            return _async == null || !_async.isDone;
        }
    }
}