﻿using System.Collections.Generic;

namespace CarsBattleRoyal
{
	public enum UserType
	{
		User,
		Tester,
		Developer
	}

	public class GameConstants
	{
		public const string GameVersion = "2.1";
		public static Dictionary<string, int> SeasonCars = new Dictionary<string, int>{{"Inferno", 1}, {"Gyro", 2}};

		public const string AndroidAppodealKey = "77b9f55cdb6cf1dbe76bb593874699d1b5b8c7a254ac10b9";
		public const string IosAppodealKey = "b0bff6ae218dde0cafad6ca6f8d73b82f7fb2202cd62b07e";
		
		public const string TerrainTag = "Terrain";
		public const string CarTag = "Vehicle";
		public const string WeaponTag = "Weapon";
		public const string ColliderTag = "Terrain";

		public const string CarLayerString = "Car";
		public const int CarLayer = 10;
		public const string NotTouchCarLayerString = "NotTouchCar";
		public const int NotTouchCarLayer = 11;

		public const byte MaxAiPlayers = 20;
		public const byte MaxRealPlayers = 40;

		public const float CircleRadius = 2300f;
		public const float CircleRisizeKoef = 2f;
		
		public const float DamageInDangerZone = 8f;
		public const float SynchronizeTime = 0.05f;
		public const int ActiveDistance = 4;
		
		public const string GameSceneName = "GameSceneTerrain";
		public const string RemoveAdsProductId = "massimpact.remove_ads";
		
		public const string LeaderBoardUrl = "https://jednike.info/";
		public static int TerrainLayer = 15;
	}
}
