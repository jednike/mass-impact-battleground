﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    public interface IScoreSaver
    {
        Scores Load(string path);
        void Save(string path, Scores scores);
    }
}
