﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using Random = System.Random;

namespace CarsBattleRoyal
{
    [Serializable]
    public class CarInfo
    {
        public string CarName;
        public int CurrentSkin;
        public bool Buyed;
    }

    [Serializable]
    public class Scores
    {
        public static bool MaySave;
        private static readonly string Path = Application.persistentDataPath + "/save6.dat";

        public static Scores Instance
        {
            get
            {
                if (_instance == null)
                    Load();
                return _instance;
            }
            set => _instance = value;
        }

        private static Scores _instance;

        public bool VipBuyed;
        public string Nickname;

        public string CurrentCar;

        public string CurrentSkin
        {
            get
            {
                var car = InGamePrefabs.Instance.cars.GetCarModelByName(CurrentCar);
                return car != null ? car.CarSkins[car.CurrentSkin] : "Random";
            }
        }

        public int Defeats;
        public int Victories;

        public int Killed
        {
            get => Kills;
            set => Kills = value;
        }

        public bool SoftLanding;
        public bool AutoAccelerate;
        public bool SelectedTarget;
        
        public string CurrentLanguage
        {
            get { return Language; }
            set
            {
                if (Language == value)
                    return;
                Language = value;
                OnLanguageChange();
            }
        }
        public static event Action OnLanguageChange = delegate { };

        public string Language;

        public int Kills;

        public int KillsInBlock;
        public int TargetKillsInBlock;

        public bool Sounds = true;
        public bool Music = true;
        public int Money;
        public double LastSaveDate;
        public double TimeInGame;

        public bool LicenseApplied;
        public bool VipBuyShowed;

        public CarInfo[] Cars;
        public UserType UserType;

        public string[] EarnedLoot;

        public string UniqueId;

        private IScoreSaver _scoreSaver;

        private static void Load()
        {
            if (!File.Exists(Path))
            {
                Instance = TransferFromPrefsToJson();
                Save();
            }
            else
            {
                Instance = GetSaver().Load(Path);

                if (Instance != null) return;
                Instance = TransferFromPrefsToJson();
                Save();
            }
        }

        public static void Save()
        {
#if !UNITY_EDITOR
			if(!MaySave)
				return;
#endif
            Instance.LastSaveDate = (DateTime.Now - new DateTime()).TotalSeconds;
            GetSaver().Save(Path, Instance);
        }

        private static IScoreSaver GetSaver()
        {
#if NEED_CRYPTO
            return new CryptScoreSaver();
#else
            return new DefaultScoreSaver();
#endif
        }

        private static Scores TransferFromPrefsToJson()
        {
            var instance = new Scores
            {
                VipBuyed = PlayerPrefs.GetInt("ads_removed", 0) == 1,
                Nickname = PlayerPrefs.GetString("Nickname", "Default"),
                CurrentCar = PlayerPrefs.GetString("CurrentCar", "Random"),
                Defeats = PlayerPrefs.GetInt("Defeats", 0),
                Victories = PlayerPrefs.GetInt("Victories", 0),
                Killed = PlayerPrefs.GetInt("Kills", 0),
                Sounds = PlayerPrefs.GetInt("SoundsVolume", 1) == 1,
                Music = PlayerPrefs.GetInt("MusicVolume", 1) == 1,
                Money = PlayerPrefs.GetInt("Money", 150),
                SoftLanding = true,
                AutoAccelerate = false,
                SelectedTarget = false,
                LastSaveDate = 0,
                TimeInGame = PlayerPrefs.GetInt("TimeInGame", 0),
                UserType = (UserType) PlayerPrefs.GetInt("UserType", 0),
                LicenseApplied = PlayerPrefs.GetInt("LicenseRead", 0) == 1,
                VipBuyShowed = PlayerPrefs.GetInt("VipBuyShowed", 0) == 1,
                Cars = new CarInfo[InGamePrefabs.Instance.cars.Cars.Length]
            };

            var i = 0;
            foreach (var car in InGamePrefabs.Instance.cars.Cars)
            {
                instance.Cars[i] = new CarInfo
                {
                    Buyed = PlayerPrefs.GetInt(car.CarName + " Buyed", 0) == 1,
                    CarName = car.CarName,
                    CurrentSkin = PlayerPrefs.GetInt(car.CarName + " Skin", 0)
                };
                i++;
            }

            return instance;
        }

        public static Scores StringToData(string strData)
        {
            try
            {
                var data = JsonUtility.FromJson<Scores>(strData);
                return data;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public CarInfo GetCarByName(string carName)
        {
            foreach (var car in Cars)
            {
                if (car.CarName == carName)
                    return car;
            }

            var newCar = new CarInfo
            {
                CarName = carName,
                Buyed = false,
                CurrentSkin = 0
            };
            var tempCars = Cars;
            Cars = new CarInfo[tempCars.Length + 1];

            var i = 0;
            foreach (var car in tempCars)
            {
                Cars[i] = car;
                i++;
            }

            Cars[i] = newCar;

            return newCar;
        }
    }
}