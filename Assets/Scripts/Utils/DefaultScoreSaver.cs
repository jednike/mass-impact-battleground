using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class DefaultScoreSaver: IScoreSaver
    {
        public Scores Load(string path)
        {
            Scores scores;
            using (var file = File.OpenRead(path))
            {
                var bf = new BinaryFormatter();
                scores = (Scores) bf.Deserialize(file);
                file.Close();
            }
            return scores;
        }

        public void Save(string path, Scores scores)
        {
            LoadSaveManager.Instance.SaveGame(JsonUtility.ToJson(scores));
            var file = File.Exists(path) ? File.OpenWrite(path) : File.Create(path);
            var bf = new BinaryFormatter();
            bf.Serialize(file, scores);
            file.Close();
        }
    }
}