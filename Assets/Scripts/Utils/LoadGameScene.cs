﻿using System;
using System.Collections;
using UnityEngine;

namespace CarsBattleRoyal
{
	public interface IGameSceneLoader
	{
		void LoadScene(string sceneName);
		bool SceneLoading();
	}

	public class LoadGameScene : MonoBehaviour
	{
		private IGameSceneLoader _sceneLoader;

		private void Awake()
		{
			_sceneLoader = gameObject.AddComponent<DefaultGameSceneLoader>();
		}

		private IEnumerator Start () {
			_sceneLoader.LoadScene(GameConstants.GameSceneName);
			while (_sceneLoader.SceneLoading())
			{
				yield return null;
			}
		}
	}
}
