namespace CarsBattleRoyal
{
    public interface ICrypter
    {
        string Encrypt(string toEncrypt);
        string Decrypt(string toDecrypt);
    }
}