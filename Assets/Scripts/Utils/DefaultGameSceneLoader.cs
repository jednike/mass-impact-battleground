using UnityEngine;
using UnityEngine.SceneManagement;

namespace CarsBattleRoyal
{
    public class DefaultGameSceneLoader: MonoBehaviour, IGameSceneLoader
    {
        private AsyncOperation _async;
        public void LoadScene(string sceneName)
        {
            _async = SceneManager.LoadSceneAsync(sceneName);
        }

        public bool SceneLoading()
        {
            return !_async.isDone;
        }
    }
}