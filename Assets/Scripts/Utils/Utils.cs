using System.Linq;
using Photon.Pun;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class Utils
    {
        public static bool IsMobile()
        {
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE)
				return true;
#else
            return false;
#endif
        }

        public static float ClampAngle(float angle, float min, float max)
        {
            if (min < 0 && max > 0 && (angle > max || angle < min))
            {
                angle -= 360;
                if (angle > max || angle < min)
                {
                    return Mathf.Abs(Mathf.DeltaAngle(angle, min)) < Mathf.Abs(Mathf.DeltaAngle(angle, max)) ? min : max;
                }
            }
            else if(min > 0 && (angle > max || angle < min))
            {
                angle += 360;
                if (angle > max || angle < min)
                {
                    return Mathf.Abs(Mathf.DeltaAngle(angle, min)) < Mathf.Abs(Mathf.DeltaAngle(angle, max)) ? min : max;
                }
            }
 
            if (angle < min) return min;
            return angle > max ? max : angle;
        }
        public static string RandomString()
        {
            var random = new System.Random();
            const string pool = "abcdefghijklmnopqrstuvwxyz0123456789";
            var chars = Enumerable.Range(0, 20)
                .Select(x => pool[random.Next(0, pool.Length)]);
            return new string(chars.ToArray());
        }

        public static void EnableAttacksElements(ParticleSystem[] particles, AudioSource source, bool enable)
        {
            if(enable && !source.isPlaying)
                source.Play();
            else if(!enable && source.isPlaying)
                source.Pause();
			
            foreach (var particle in particles)
            {
                if(enable)
                    particle.Play();
                else
                    particle.Stop();
            }
        }

        public static bool NowInRoom()
        {
            return !PhotonNetwork.OfflineMode && PhotonNetwork.InRoom && PhotonNetwork.CurrentRoom.CustomProperties != null;
        }
    }
}