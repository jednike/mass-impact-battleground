using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class CryptScoreSaver: IScoreSaver
    {
        private ICrypter _crypter;
        
        public CryptScoreSaver()
        {
            _crypter = new DefaultCrypter();
        }
        public Scores Load(string path)
        {
            Scores scores;
            using (var reader = new FileStream(path, FileMode.Open))
            {
                var str = new StreamReader(reader, Encoding.UTF8).ReadToEnd();
                if (!string.IsNullOrEmpty(str))
                {
                    using (var file = File.OpenRead(path))
                    {
                        var bf = new BinaryFormatter();
                        scores = (Scores) bf.Deserialize(file);
                        file.Close();
                    }
                }
                else
                {
                    str = _crypter.Decrypt(str);
                    scores = JsonUtility.FromJson<Scores>(new StringReader(str).ReadToEnd());
                }
            }
            return scores;
        }

        public void Save(string path, Scores scores)
        {
            var save = _crypter.Encrypt(JsonUtility.ToJson(scores));
            LoadSaveManager.Instance.SaveGame(save);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                var array = Encoding.UTF8.GetBytes(save);
                stream.Write(array, 0, array.Length);
            }
        }
    }
}