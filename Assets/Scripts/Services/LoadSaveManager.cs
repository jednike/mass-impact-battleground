﻿using System;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class LoadSaveManager : GlobalSingletonBehaviour<LoadSaveManager>
    {
        public static event Action OnSaveLoadFromCloud = delegate { };
        private SaveManager _saveManager;

        public override void DoAwake()
        {
#if UNITY_ANDROID
            _saveManager = new AndroidSaveManager();
#elif UNITY_IOS
            _saveManager = new iOSSaveManager();
#else
            _saveManager = new DefaultLoadManager();
#endif
            _saveManager.Init();
            _saveManager.OnLoadSaveGame = OnLoadSaveGame;
        }

        private void Start()
        {
            if (IsLogin())
            {
                OnSaveLoadFromCloud();
                return;
            }
            _saveManager.Login(success => {
                if (!success)
                {
                    Debug.Log("Not Success Login GameCenter");
                    OnSaveLoadFromCloud();
                    return;
                }

                LoadSaveGame();
                RequestNickname();
            });
        }

        private static void OnLoadSaveGame(bool success, string saveString)
        {
            if (success)
            {
                if (string.IsNullOrEmpty(saveString))
                {
                    OnSaveLoadFromCloud();
                    return;
                }

                var cloudScores = Scores.StringToData(saveString);
                if (cloudScores != null && cloudScores.LastSaveDate > Scores.Instance.LastSaveDate)
                {
                    Scores.Instance = cloudScores;
                }
                OnSaveLoadFromCloud();
            }
            else
            {
                OnSaveLoadFromCloud();
            }
        }

        private void RequestNickname()
        {
            if (!IsLogin())
                return;
            
            if (Scores.Instance.Nickname != "Default") return;
            Scores.Instance.Nickname = _saveManager.RequestNickname();
        }
        public void SaveGame(string data)
        {
            if (!IsLogin())
                return;
            _saveManager.SaveGame(data);
        }
        private void LoadSaveGame()
        {
            if (!IsLogin())
            {
                OnSaveLoadFromCloud();
                return;
            }
            _saveManager.LoadSaveGame();
        }
        public bool IsLogin()
        {
            return _saveManager.IsLogin();
        }

        public override void DoDestroy()
        {
            _saveManager.UnLogin();
            _saveManager = null;
        }
    }
}
