﻿#if UNITY_IOS

using System;
using FSG.iOSKeychain;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class iOSSaveManager : SaveManager
    {
        public override void Login(Action<bool> login)
        {
            Social.localUser.Authenticate(login);
        }

        public override void Init()
        {
        }

        public override string RequestNickname()
        {
            return Social.localUser.userName;
        }

        public override bool IsLogin()
        {
            return Social.localUser != null && Social.localUser.authenticated;
        }

        public override void SaveGame(string data)
        {
            Keychain.SetValue(SaveName, data);
        }

        public override void LoadSaveGame()
        {
            var save = Keychain.GetValue(SaveName);
            OnLoadSaveGame(true, save);
        }

        public override void UnLogin()
        {
            
        }
    }
}
#endif