﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class DefaultLoadManager : SaveManager
    {
        public override void Login(Action<bool> login)
        {
            login(true);
        }

        public override void Init()
        {
            Debug.Log("Init successful");
        }

        public override string RequestNickname()
        {
            return "DefaultNickName";
        }

        public override bool IsLogin()
        {
            return true;
        }

        public override void SaveGame(string data)
        {
            Debug.Log($"Data saved: {data}");
        }

        public override void LoadSaveGame()
        {
            Debug.Log("Save loaded");
        }

        public override void UnLogin()
        {
            Debug.Log("Unlogin");
        }
    }
}
