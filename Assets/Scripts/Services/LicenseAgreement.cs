﻿using UnityEngine;

namespace CarsBattleRoyal
{
	public class LicenseAgreement : MonoBehaviour
	{
		public void ApplyLicense()
		{
			Scores.Instance.LicenseApplied = true;
			CloseWindow();
		}

		private void CloseWindow()
		{
			gameObject.SetActive(false);
		}
	}
}
