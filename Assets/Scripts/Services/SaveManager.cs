﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    public abstract class SaveManager
    {
        public Action<bool, string> OnLoadSaveGame = delegate(bool success, string data) {  };
        
        protected const string SaveName = "CloudSaveMassImpact";

        public abstract void Login(Action<bool> login);
        public abstract void Init();
        public abstract string RequestNickname();
        public abstract bool IsLogin();
        public abstract void SaveGame(string data);
        public abstract void LoadSaveGame();
        public abstract void UnLogin();
    }
}