﻿#if UNITY_ANDROID
using System;
using System.Text;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class AndroidSaveManager: SaveManager
    {
        public override void Login(Action<bool> login)
        {
            Social.localUser.Authenticate(login);
        }

        public override void Init()
        {
            var config = new PlayGamesClientConfiguration.Builder()
                .EnableSavedGames()
                .Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true;
            PlayGamesPlatform.Activate();
        }
        
        public override string RequestNickname()
        {
            return Social.localUser.userName;
        }

        public override bool IsLogin()
        {
            return PlayGamesPlatform.Instance.IsAuthenticated();
        }

        public override void SaveGame(string data)
        {
            var savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            var builder = new SavedGameMetadataUpdate.Builder();
            builder = builder
                .WithUpdatedDescription("Saved game at " + DateTime.Now);
            var updatedMetadata = builder.Build();
        
            savedGameClient.OpenWithAutomaticConflictResolution(SaveName, DataSource.ReadNetworkOnly,
                ConflictResolutionStrategy.UseMostRecentlySaved, (status, metadata) =>
                {
                    if(status != SavedGameRequestStatus.Success)
                        return;
                    savedGameClient.CommitUpdate(metadata, updatedMetadata, Encoding.UTF8.GetBytes(data), (status1, metadata1) =>
                    {
                        Debug.Log("Save " + status1);
                    });
                });
        }

        public override void LoadSaveGame()
        {
            var savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.OpenWithAutomaticConflictResolution(SaveName, DataSource.ReadNetworkOnly,
                ConflictResolutionStrategy.UseMostRecentlySaved, (status, metadata) =>
                {
                    if(status != SavedGameRequestStatus.Success)
                    {
                        OnLoadSaveGame(false, "");
                        return;
                    }
                
                    savedGameClient.ReadBinaryData(metadata, (requestStatus, bytes) =>
                    {
                        OnLoadSaveGame(status == SavedGameRequestStatus.Success, Encoding.UTF8.GetString(bytes));
                    });
                });
        }

        public override void UnLogin()
        {
            
        }
    }
}
#endif