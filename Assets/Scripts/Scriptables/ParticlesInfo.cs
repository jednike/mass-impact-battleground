﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    [CreateAssetMenu(menuName = "Mass Impact/Particles Info", fileName = "ParticlesInfo")]
    public class ParticlesInfo : ScriptableObject
    {
        public GameObject stopFlyParticle;
        public GameObject[] explosions;

        public GameObject GetRandomExplosion()
        {
            return explosions[Random.Range(0, explosions.Length)];
        }
    }
}
