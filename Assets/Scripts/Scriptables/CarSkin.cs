using UnityEngine;

namespace CarsBattleRoyal
{
    [CreateAssetMenu(menuName = "Mass Impact/Car Skin", fileName = "CarSkin")]
    public class CarSkin : ScriptableObject
    {
        public string CarName;
        public string CarNameString;
        public string[] CarSkins;
		
        public int Cost
        {
            get
            {
                switch (AvailableType)
                {
                    case AvailableType.Simple:
                        return 100;
                    case AvailableType.Epic:
                        return 1000;
                    case AvailableType.OnlyBox:
                    case AvailableType.OnlyVIP:
                    case AvailableType.OnlySeason:
                        return -1;
                    default:
                        return 0;
                }
            }
        }
        public AvailableType AvailableType;
        public GameObject CarModel;
		
        public int CurrentSkin
        {
            get => Scores.Instance.GetCarByName(CarName).CurrentSkin;
            set => Scores.Instance.GetCarByName(CarName).CurrentSkin = value;
        }
        public bool Buyed
        {
            get => Scores.Instance.GetCarByName(CarName).Buyed;
            set => Scores.Instance.GetCarByName(CarName).Buyed = value;
        }
    }
}