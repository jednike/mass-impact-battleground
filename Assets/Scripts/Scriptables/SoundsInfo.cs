﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace CarsBattleRoyal
{
    [CreateAssetMenu(menuName = "Mass Impact/Sounds Info", fileName = "SoundsInfo")]
    public class SoundsInfo : ScriptableObject
    {
        public AudioMixerGroup soundMixerGroup;
        public AudioMixerGroup musicMixerGroup;

        public AudioClip firstPlaceSound;
        public AudioClip secondPlaceSound;
        public AudioClip thirdPlaceSound;

        public AudioClip destroyCarSound;
    }
}
