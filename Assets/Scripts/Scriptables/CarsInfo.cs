﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CarsBattleRoyal
{
    [CreateAssetMenu(menuName = "Mass Impact/Cars Info", fileName = "CarsInfo")]
    public class CarsInfo : ScriptableObject
    {
        public CarSkin[] Cars => cars;
        [SerializeField] private CarSkin[] cars;

        public CarSkin GetCarModelByName(string carName)
        {
            return cars.FirstOrDefault(car => car.CarName == carName);
        }

        public void GetRandomCarAndSkin(out string carName, out string skinName, AvailableType needType)
        {
            var car = cars[Random.Range(0, cars.Length)];
            while (car.AvailableType != needType)
            {
                car = cars[Random.Range(0, cars.Length)];
            }

            carName = car.CarName;
            skinName = car.CarSkins[Random.Range(0, car.CarSkins.Length)];
        }
    }
}
