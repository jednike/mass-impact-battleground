﻿using System;
using System.Collections;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class AdController : GlobalSingletonBehaviour<AdController>
    {
        private AdManager _adManager;
        public override void DoAwake()
        {
            base.DoAwake();
            #if (UNITY_IOS || UNITY_ANDROID) && APPODEAL
                _adManager = new AppodealManager();
            #else
                _adManager = new DefaultAdManager();
            #endif
            _adManager.InitAds();
        }

        public static void Cache(AdType type)
        {
			Instance._adManager.Cache(type);
        }
        
        public static IEnumerator ShowInterAds(string placement, float waitingTime)
        {
            yield return new WaitForSeconds(waitingTime);
            ShowInterAds(placement);
        }

        public static void ShowInterAds(string placement)
        {
            if (Scores.Instance.VipBuyed)
                return;
            Instance._adManager.ShowInterAds(placement);
        }
        public static void ShowVideoAds(string placement)
        {
            if (Scores.Instance.VipBuyed)
                return;
            Instance._adManager.ShowVideoAds(placement);
        }
        public static void ShowRewardedAds(string placement, Action<bool> rewardedAction)
        {
            Instance._adManager.ShowRewardedAds(placement, rewardedAction);
        }
    }
}
