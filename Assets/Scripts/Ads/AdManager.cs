﻿using System;

namespace CarsBattleRoyal
{
	public enum AdType
	{
		Interstitial,
		NonSkipVideo,
		Rewarded
	}
	public abstract class AdManager
	{
		protected Action<bool> OnRewardedShowed;
		public abstract void InitAds();
		public abstract void Cache(AdType type);
		public abstract void ShowInterAds(string placement);
		public abstract void ShowVideoAds(string placement);
		public abstract void ShowRewardedAds(string placement, Action<bool> rewardedAction);
	}
}
