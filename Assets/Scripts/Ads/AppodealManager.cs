﻿using System;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

#if APPODEAL
namespace CarsBattleRoyal
{
    public class AppodealManager : AdManager, IInterstitialAdListener, IRewardedVideoAdListener,
        INonSkippableVideoAdListener
    {
	    public override void InitAds()
        {
	        Appodeal.disableLocationPermissionCheck();
	        Appodeal.disableWriteExternalStoragePermissionCheck();
	        
#if UNITY_ANDROID
			Appodeal.initialize(GameConstants.AndroidAppodealKey, Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO, true);
#elif UNITY_IOS
			Appodeal.initialize(GameConstants.IosAppodealKey, Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO, true);
			#endif

			Appodeal.setAutoCache(Appodeal.REWARDED_VIDEO, true);
			
			Appodeal.setInterstitialCallbacks(this);
			Appodeal.setRewardedVideoCallbacks(this);
			Appodeal.setNonSkippableVideoCallbacks(this);
        }
        
        public override void Cache(AdType type)
        {
			switch (type)
			{
				case AdType.Interstitial:
					Appodeal.cache(Appodeal.INTERSTITIAL);
					break;
				case AdType.NonSkipVideo:
					#if !UNITY_STANDALONE
					Appodeal.cache(Appodeal.NON_SKIPPABLE_VIDEO);
					#endif
					break;
				case AdType.Rewarded:
					Appodeal.cache(Appodeal.REWARDED_VIDEO);
					break;
			}
        }

        public override void ShowInterAds(string placement)
        {
	        if (!Appodeal.canShow(Appodeal.INTERSTITIAL, placement) || !Appodeal.isLoaded(Appodeal.INTERSTITIAL))
	        {
		        Appodeal.cache(Appodeal.INTERSTITIAL);
		        return;
	        }
	        Appodeal.show(Appodeal.INTERSTITIAL, placement);
        }

        public override void ShowVideoAds(string placement)
        {
	        if (!Appodeal.canShow(Appodeal.NON_SKIPPABLE_VIDEO, placement) || !Appodeal.isLoaded(Appodeal.NON_SKIPPABLE_VIDEO))
	        {
		        Appodeal.cache(Appodeal.NON_SKIPPABLE_VIDEO);
		        return;
	        }
		
	        Appodeal.show(Appodeal.NON_SKIPPABLE_VIDEO, placement);
        }

        public override void ShowRewardedAds(string placement, Action<bool> rewardedAction)
        {
	        if (!Appodeal.canShow(Appodeal.REWARDED_VIDEO, placement) || !Appodeal.isLoaded(Appodeal.REWARDED_VIDEO))
	        {
		        Appodeal.cache(Appodeal.REWARDED_VIDEO);
		        return;
	        }
	        OnRewardedShowed = rewardedAction;
	        Appodeal.show(Appodeal.REWARDED_VIDEO, placement);
        }

        #region Interstitial
		
        private void OnInterShowedNull()
        {
	        InGamePrefabs.SetVolume();
        }

        public void onInterstitialLoaded(bool isPrecache)
        {
        }

        public void onInterstitialFailedToLoad()
        {
        }

        public void onInterstitialShown()
        {
	        InGamePrefabs.SetVolume(-80);
        }

        public void onInterstitialClosed()
        {
	        OnInterShowedNull();
        }

        public void onInterstitialClicked()
        {
        }

        public void onInterstitialExpired()
        {
        }

        #endregion

        #region Rewarded

        private void OnRewardedShowedNull(bool success)
        {
	        InGamePrefabs.SetVolume();
			
	        if (OnRewardedShowed == null) return;
	        OnRewardedShowed(success);
	        OnRewardedShowed = null;
        }

        public void onRewardedVideoLoaded(bool precache)
        {
        }

        public void onRewardedVideoFailedToLoad()
        {
        }

        public void onRewardedVideoShown()
        {
	        InGamePrefabs.SetVolume(-80);
        }

        public void onRewardedVideoFinished(double amount, string sName)
        {
        }

        public void onRewardedVideoClosed(bool finished)
        {
	        OnRewardedShowedNull(finished);
        }

        public void onRewardedVideoExpired()
        {
	        
        }

        public void onRewardedVideoClicked()
        {
	        
        }

        #endregion

        #region Video

        private void OnVideoShowedNull()
        {
	        InGamePrefabs.SetVolume();
        }

        public void onNonSkippableVideoLoaded(bool isPrecache)
        {
        }

        public void onNonSkippableVideoFailedToLoad()
        {
			
        }

        public void onNonSkippableVideoShown()
        {
	        InGamePrefabs.SetVolume(-80);
        }

        public void onNonSkippableVideoFinished()
        {
        }

        public void onNonSkippableVideoClosed(bool finished)
        {
	        OnVideoShowedNull();
        }

        public void onNonSkippableVideoExpired()
        {
        }

        #endregion
    }
}
#endif