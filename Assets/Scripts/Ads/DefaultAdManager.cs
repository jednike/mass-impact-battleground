﻿using System;

namespace CarsBattleRoyal
{
    public class DefaultAdManager : AdManager
    {
        public override void InitAds()
        {
            
        }

        public override void Cache(AdType type)
        {
        }

        public override void ShowInterAds(string placement)
        {
        }

        public override void ShowVideoAds(string placement)
        {
        }

        public override void ShowRewardedAds(string placement, Action<bool> rewardedAction)
        {
            rewardedAction(true);
        }
    }
}