﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;

namespace CarsBattleRoyal
{
	public class BuyManager: GlobalSingletonBehaviour<BuyManager>, IStoreListener  {
		public static event Action<bool> BuyRemoveAdsComplete = delegate {  };
		
		private IStoreController _controller;
		private IExtensionProvider _extensions;

		public override void DoAwake ()
		{
			var module = StandardPurchasingModule.Instance();
			var builder = ConfigurationBuilder.Instance(module);
			builder.AddProduct(GameConstants.RemoveAdsProductId, ProductType.NonConsumable);

			UnityPurchasing.Initialize (this, builder);
		}

		public void OnInitialized (IStoreController controller, IExtensionProvider extensions)
		{
			_controller = controller;
			_extensions = extensions;
		}
		
		public void OnInitializeFailed (InitializationFailureReason error)
		{
			Debug.Log("Billing failed to initialize!");
			switch (error)
			{
				case InitializationFailureReason.AppNotKnown:
					Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
					break;
				case InitializationFailureReason.PurchasingUnavailable:
					Debug.Log("Billing disabled!");
					break;
				case InitializationFailureReason.NoProductsAvailable:
					Debug.Log("No products available for purchase!");
					break;
			}
		}
		
		public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs e)
		{
			switch (e.purchasedProduct.definition.id)
			{
				case GameConstants.RemoveAdsProductId:
					if (Scores.Instance.VipBuyed)
						return PurchaseProcessingResult.Complete;

					Scores.Instance.VipBuyed = true;
					Scores.Instance.Money += 1000;
					BuyRemoveAdsComplete(false);
					break;
			}
			
			return PurchaseProcessingResult.Complete;
		}

		public static void BuyProduct(string productId)
		{
			Instance._controller.InitiatePurchase(productId);
		}

		public static void RestoreProducts()
		{
			Instance._extensions.GetExtension<IAppleExtensions>().RestoreTransactions(OnTransactionsRestored);
		}
		private static void OnTransactionsRestored(bool success)
		{
			Debug.Log($"Transactions restored: {success}");
		}
	
		public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
		{
			Debug.Log(reason);
		}

		public static void BuyRemoveAds(bool state)
		{
			BuyRemoveAdsComplete(state);
		}
	}
}
