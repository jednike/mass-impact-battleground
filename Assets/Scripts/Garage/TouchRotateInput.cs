using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CarsBattleRoyal
{
    public class TouchRotateInput: IRotateInput
    {
        public bool StartRotate()
        {
            return Input.touches.Any(touch => touch.phase == TouchPhase.Began);
        }

        public float Rotate()
        {
            return (from touch in Input.touches where touch.phase == TouchPhase.Moved select touch.deltaPosition.x).FirstOrDefault();
        }

        public bool EndRotate()
        {
            return Input.touches.Any(touch => touch.phase == TouchPhase.Ended);
        }
    }
}