using UnityEngine;
using UnityEngine.EventSystems;

namespace CarsBattleRoyal
{
    public class MouseRotateInput: IRotateInput
    {
        public bool StartRotate()
        {
            return Input.GetMouseButtonDown(0);
        }

        public float Rotate()
        {
            return Input.GetAxis("Mouse X");
        }

        public bool EndRotate()
        {
            return Input.GetMouseButtonUp(0);
        }
    }
}