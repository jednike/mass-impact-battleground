namespace CarsBattleRoyal
{
    public enum CarState
    {
        NotBuyed,
        Buyed,
        OnlyVip,
        Selected,
        FromLootBox,
        ForSeason
    }
}