﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
    public class GarageSkinsManager : MonoBehaviour
    {
        [SerializeField] private Transform skinsTransform;
        [SerializeField] private GameObject skinsPrefab;
        
        private GarageController _garageController;
        private AwardFromLootBox[] _lootSkins;
        private AssetBundle _bundle;

        private void Awake()
        {
            _garageController = GetComponent<GarageController>();
            
            _lootSkins = LootBoxManager.GetAvailableByType(AwardType.Skin, true);
            
            _garageController.SetNewSkinAction += OnSetNewSkin;
            _garageController.SetNewCarAction += OnSetNewCar;
            
            _bundle = AssetBundle.LoadFromFile($"{Application.streamingAssetsPath}/ui_skins");
        }

        private void OnSetNewSkin(int newSkin)
        {
            var i = 0;
            foreach (Transform carPlace in skinsTransform)
            {
                carPlace.GetChild(0).gameObject.SetActive(i == newSkin);
                i++;
            }
        }

        private void OnSetNewCar(CarSkin currentCar)
        {
            var carSkins = currentCar.CarSkins;
            for (var i = 0; i < currentCar.CarSkins.Length; i++)
            {
                GameObject skinObject;
                if (i >= skinsTransform.childCount)
                {
                    var index = i;
                    skinObject = Instantiate(skinsPrefab, skinsTransform);
                    skinObject.GetComponent<Button>().onClick.AddListener(() => _garageController.SetNewSkin(index));
                }
                else
                {
                    skinObject = skinsTransform.GetChild(i).gameObject;
                }

                skinObject.SetActive(true);
                skinObject.GetComponent<Image>().sprite = _bundle.LoadAsset<Sprite>(carSkins[i]);

                var blockedSkin = _lootSkins.Any(skin =>
                    currentCar.CarName == skin.CarName && carSkins[i] == skin.SkinName);
                skinObject.GetComponent<Image>().color =
                    blockedSkin ? new Color(1, 1, 1, 0.5f) : Color.white;

                skinObject.transform.GetChild(0).gameObject.SetActive(i == currentCar.CurrentSkin);
            }

            for (var i = currentCar.CarSkins.Length; i < skinsTransform.childCount; i++)
            {
                skinsTransform.GetChild(i).gameObject.SetActive(false);
            }
        }

        private void OnDestroy()
        {
            _garageController.SetNewSkinAction -= OnSetNewSkin;
            _garageController.SetNewCarAction -= OnSetNewCar;
            
            _bundle.Unload(true);
        }
    }
}