﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace CarsBattleRoyal
{
    public class GarageAdsManager : MonoBehaviour
    {
        [SerializeField] private GameObject vipWindow;
        private CarStateManager _carState;

        private void Awake()
        {
            _carState = GetComponent<CarStateManager>();
        }
        
        private void OnEnable()
        {
            BuyManager.BuyRemoveAdsComplete += ShowRemoveAdsWindow;
        }
        
        private void ShowRemoveAdsWindow(bool buyed)
        {
            if (!buyed) return;
			
            vipWindow.SetActive(false);
            if(_carState.CurrentCarState == CarState.OnlyVip)
                _carState.CurrentCarState = CarState.Buyed;
        }

        public void AddCoinsForAds()
        {
            AnalyticsManager.LogEvent("Add Money Start");
            AdController.ShowRewardedAds("add_coins", success =>
            {
                if (!success) return;
                AnalyticsManager.LogEvent("Add Money Success");
                Scores.Instance.Money += 200;
            });
        }
        
        private void OnDisable()
        {
            BuyManager.BuyRemoveAdsComplete -= ShowRemoveAdsWindow;
        }
    }
}
