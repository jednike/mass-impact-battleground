﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
    public class CarInfoView : MonoBehaviour
    {
        [SerializeField] private Text cost;
        [SerializeField] private Text carName;

        private GarageController _garage;

        private void Awake()
        {
            _garage = GetComponent<GarageController>();
            _garage.SetNewCarAction += OnSetNewCar;
        }

        private void OnSetNewCar(CarSkin car)
        {
            cost.text = car.Cost.ToString();
            carName.text = car.CarNameString;
        }

        private void OnDestroy()
        {
            _garage.SetNewCarAction -= OnSetNewCar;
        }
    }
}
