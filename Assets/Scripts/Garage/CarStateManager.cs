﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace CarsBattleRoyal
{
    public class CarStateManager : MonoBehaviour
    {
        private GarageController _garage;

        [SerializeField] private GameObject selectButton;
        [SerializeField] private GameObject selectedButton;
        [SerializeField] private GameObject unlockButton;
        [SerializeField] private GameObject buyButton;
        [SerializeField] private GameObject notAvailable;
        [SerializeField] private Text notAvailableText;

        public CarState CurrentCarState
        {
            get => _currentCarState;
            set
            {
                switch (_currentCarState)
                {
                    case CarState.NotBuyed:
                        unlockButton.SetActive(false);
                        break;
                    case CarState.Buyed:
                        selectButton.SetActive(false);
                        break;
                    case CarState.OnlyVip:
                        buyButton.SetActive(false);
                        break;
                    case CarState.Selected:
                        selectedButton.SetActive(false);
                        break;
                    case CarState.FromLootBox:
                    case CarState.ForSeason:
                        notAvailable.SetActive(false);
                        break;
                }

                _lastState = _currentCarState;
                _currentCarState = value;
                switch (_currentCarState)
                {
                    case CarState.NotBuyed:
                        unlockButton.SetActive(true);
                        break;
                    case CarState.Buyed:
                        selectButton.SetActive(true);
                        break;
                    case CarState.OnlyVip:
                        buyButton.SetActive(true);
                        break;
                    case CarState.Selected:
                        selectedButton.SetActive(true);
                        break;
                    case CarState.ForSeason:
                        notAvailableText.text = MultiLanguageManager.GetTranslateString("For season", "For season", notAvailableText).Replace("%", GameConstants.SeasonCars[_garage.CurrentCarInfo.CarName].ToString());
                        notAvailable.SetActive(true);
                        break;
                    case CarState.FromLootBox:
                        notAvailableText.text = MultiLanguageManager.GetTranslateString("From lootbox", "From lootbox", notAvailableText);
                        notAvailable.SetActive(true);
                        break;
                }
            }
        }
        private CarState _currentCarState;
        private CarState _lastState;

        private void Awake()
        {
            _garage = GetComponent<GarageController>();
            _garage.SetNewCarAction += CheckCarState;
        }

        private void OnEnable()
        {
            BuyManager.BuyRemoveAdsComplete += OnBuyRemoveAdsComplete;
        }

        private void OnBuyRemoveAdsComplete(bool notBuyed)
        {
            if (CurrentCarState == CarState.OnlyVip && !notBuyed)
            {
                CurrentCarState = CarState.Buyed;
            }
        }

        private void OnDisable()
        {
            BuyManager.BuyRemoveAdsComplete -= OnBuyRemoveAdsComplete;
        }

        public void CheckCarState(CarSkin currentCar)
        {
            if(CurrentCarState == CarState.FromLootBox ||
               CurrentCarState == CarState.ForSeason)
                CurrentCarState = _lastState;

            if (Scores.Instance.CurrentCar == currentCar.CarName)
            {
                CurrentCarState = CarState.Selected;
            }
            else
            {
                switch (currentCar.AvailableType)
                {
                    case AvailableType.Random:
                        CurrentCarState = CarState.Buyed;
                        break;
                    case AvailableType.Simple:
                    case AvailableType.Epic:
                        CurrentCarState = !currentCar.Buyed ? CarState.NotBuyed : CarState.Buyed;
                        break;
                    case AvailableType.OnlyVIP:
                        CurrentCarState = Scores.Instance.VipBuyed ? CarState.Buyed : CarState.OnlyVip;
                        break;
                    case AvailableType.OnlyBox:
                        CurrentCarState = !currentCar.Buyed ? CarState.FromLootBox : CarState.Buyed;
                        break;
                    case AvailableType.OnlySeason:
                        CurrentCarState = !currentCar.Buyed ? CarState.ForSeason : CarState.Buyed;
                        break;
                }
            }
        }

        private void OnDestroy()
        {
            _garage.SetNewCarAction -= CheckCarState;
        }
    }
}
