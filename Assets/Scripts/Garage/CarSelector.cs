﻿using UnityEngine;

namespace CarsBattleRoyal
{
    public class CarSelector : MonoBehaviour
    {
        private CarStateManager _carState;
        private GarageController _garage;
        
        private void Awake()
        {
            _carState = GetComponent<CarStateManager>();
            _garage = GetComponent<GarageController>();
        }
        
        public void SelectCar()
        {
            _carState.CurrentCarState = CarState.Selected;
            Scores.Instance.CurrentCar = _garage.CurrentCarInfo.CarName;
            AnalyticsManager.LogEvent("Select Car", "Name", _garage.CurrentCarInfo.CarName);
        }
    }
}
