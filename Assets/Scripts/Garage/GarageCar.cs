using System;
using UnityEngine;

namespace CarsBattleRoyal
{
    [Serializable]
    public class GarageCar
    {
        public CarSkin CarInfo;
        public GameObject GameObject;
        public GameObject CarObject;
    }
}