﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarsBattleRoyal
{
    public interface IRotateInput
    {
        bool StartRotate();
        float Rotate();
        bool EndRotate();
    }
}