﻿using System;
using System.Collections;
using System.Collections.Generic;
using CarsBattleRoyal;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace CarsBattleRoyal
{
	public class RotateCarManager : MonoBehaviour {
		[SerializeField] private float rotationRate = 0.4f;
		private bool _rotating;
		private float _lastRotateForce;

		private IRotateInput _rotateInput;
		private Transform _currentPlatform;

		private void Awake()
		{
			if (Utils.IsMobile())
			{
				_rotateInput = new TouchRotateInput();
			}
			else
			{		
				_rotateInput = new MouseRotateInput();
			}
		}

		private void Update (){
			if(!_currentPlatform)
				return;
			
			if (_rotateInput.StartRotate())
			{
				if (EventSystem.current.IsPointerOverGameObject() ||
				    EventSystem.current.currentSelectedGameObject != null)
					return;
				_rotating = true;
			}
			if (_rotating)
			{
				_lastRotateForce = _rotateInput.Rotate();
				if (Math.Abs(_lastRotateForce) > 0.01f)
				{
					_lastRotateForce *= -rotationRate;
					_currentPlatform.Rotate (0, _lastRotateForce, 0, Space.World);
				}
			}
			if (_rotateInput.EndRotate())
			{
				if (_rotating)
				{
					_rotating = false;
					StartCoroutine(Smooth());
				}
			}
		}
	
		private IEnumerator Smooth()
		{
			var time = 0f;
			while (time < 0.3f)
			{
				time += Time.deltaTime;
				_currentPlatform.Rotate (0, 
					Mathf.Lerp(_lastRotateForce, 0, time/0.3f), 0, Space.World);
				yield return new WaitForSeconds(Time.deltaTime);
			}
		}

		public void SetTarget(GameObject platform)
		{
			_currentPlatform = platform.transform;
		}
	}
}
