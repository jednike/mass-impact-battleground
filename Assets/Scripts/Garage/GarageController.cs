﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

 namespace CarsBattleRoyal
{
	public class GarageController : MonoBehaviour
	{
		[SerializeField] private GameObject carPlacePrefab;
		[SerializeField] private Transform carPlaces;

		[SerializeField] private List<GarageCar> cars = new List<GarageCar>();
		
		private int _lastCar;
		private int _currentCar;

		private Vector3 _lastPosition;
		private Vector3 _newPosition;

		private const float TimeToChangePos = 1f;
		private float _currentTime;
		private AwardFromLootBox[] _lootSkins;

		public event Action<int> SetNewSkinAction = delegate {  };
		public event Action<CarSkin> SetNewCarAction = delegate {  };
		private CarStateManager _stateManager;
		
		public CarSkin CurrentCarInfo { get; private set; }

		private RotateCarManager _rotateManager;

		private void OnEnable()
		{
			GameControllers.OnGameStateChanged += OnGameStateChanged;
		}

		private void OnGameStateChanged(GameState lastState, GameState newState)
		{
			switch (newState)
			{
				case GameState.LoadMenu:
					SceneManager.LoadScene("MenuScene");
					break;
			}
		}

		private void OnDisable()
		{
			GameControllers.OnGameStateChanged -= OnGameStateChanged;
		}

		private void Awake ()
		{			
			_lootSkins = LootBoxManager.GetAvailableByType(AwardType.Skin, true);
			_stateManager = GetComponent<CarStateManager>();
			_rotateManager = GetComponent<RotateCarManager>();
			
			StartCoroutine(ChangeRandomCar());
	
			var i = 1;
			foreach (var car in InGamePrefabs.Instance.cars.Cars)
			{
				var carPlace = Instantiate(carPlacePrefab, carPlaces);
				carPlace.transform.position = new Vector3(i * -12, 0, 0);

				var carObject = Instantiate(car.CarModel, carPlace.transform);
				carObject.AddComponent<CarSkinManager>().ApplySkin(car.CarName, car.CarSkins[car.CurrentSkin], new GarageSkinSwitcher());

				cars.Add(new GarageCar
				{
					CarInfo = car,
					GameObject = carPlace,
					CarObject = carObject
				});
				i++;
			}

			for (i = 0; i < cars.Count; i++)
			{
				if (Scores.Instance.CurrentCar != cars[i].CarInfo.CarName) continue;
				SetNewCar(i);
				return;
			}
		}

		private IEnumerator ChangeRandomCar()
		{
			while (enabled)
			{
				var cars = InGamePrefabs.Instance.cars;
				cars.GetRandomCarAndSkin(out var carName, out var carSkin, AvailableType.Simple);
				var car = Instantiate(cars.GetCarModelByName(carName).CarModel, this.cars[0].GameObject.transform);
				car.AddComponent<CarSkinManager>().ApplySkin(carName, carSkin, new GarageSkinSwitcher());
				
				yield return new WaitForSecondsRealtime(2f);
				Destroy(car);
			}
		}

		public void NextCar(int direction)
		{
			_lastCar = _currentCar;
			SetNewCar((int) Mathf.Repeat(_lastCar + direction, cars.Count));
		}

		public void SetNewSkin(int newSkin)
		{
			var blockedSkin = _lootSkins.Any(skin => CurrentCarInfo.CarName == skin.CarName && CurrentCarInfo.CarSkins[newSkin] == skin.SkinName);
			if (!blockedSkin)
			{
				if(_stateManager.CurrentCarState == CarState.FromLootBox ||
				   _stateManager.CurrentCarState == CarState.ForSeason)
					_stateManager.CheckCarState(CurrentCarInfo);
				cars[_currentCar].CarObject.GetComponent<CarSkinManager>().ApplySkin(CurrentCarInfo.CarName, CurrentCarInfo.CarSkins[newSkin], new GarageSkinSwitcher());
				CurrentCarInfo.CurrentSkin = newSkin;
				SetNewSkinAction(newSkin);
			}
			else 
				_stateManager.CurrentCarState = CarState.FromLootBox;
		}

		private void SetNewCar(int newCar)
		{
			_currentCar = newCar;
			CurrentCarInfo = cars[_currentCar].CarInfo;
			_rotateManager.SetTarget(cars[_currentCar].GameObject);
			
			_lastPosition = carPlaces.transform.position;
			_newPosition = new Vector3(_currentCar * 12, 0, 0);

			SetNewCarAction(CurrentCarInfo);

			_currentTime = 0;
		}
		
		private void Update()
		{
			if(_currentTime >= TimeToChangePos)
				return;

			_currentTime += Time.deltaTime;
			carPlaces.position = Vector3.Lerp(_lastPosition, _newPosition, _currentTime / TimeToChangePos);
		}
	}
}
