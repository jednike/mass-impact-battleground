﻿using System;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class CarBuyer : MonoBehaviour
    {
        private CarStateManager _carState;
        private GarageController _garage;

        private void Awake()
        {
            _carState = GetComponent<CarStateManager>();
            _garage = GetComponent<GarageController>();
        }

        public void UnlockCurrentCar()
        {
            if(Scores.Instance.Money < _garage.CurrentCarInfo.Cost)
                return;
            Scores.Instance.Money -= _garage.CurrentCarInfo.Cost;
            _garage.CurrentCarInfo.Buyed = true;
            _carState.CurrentCarState = CarState.Buyed;
            AnalyticsManager.LogEvent("Buy Car", "Name", _garage.CurrentCarInfo.CarName);
        }
    }
}
