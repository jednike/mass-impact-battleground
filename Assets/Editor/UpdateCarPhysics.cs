﻿using System.Collections.Generic;
using EVP;
using UnityEditor;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class UpdateCarPhysics : EditorWindow {
        private VehicleController _playerPrefab;
        private List<VehicleController> _playerModels;

        private bool _done;
        private string _errorLine;

        private void OnGUI()
        {
            if (!_done)
            {
                if (string.IsNullOrEmpty(_errorLine))
                {
                    EditorGUILayout.BeginVertical("box");
                    EditorGUILayout.LabelField("Update car", EditorStyles.boldLabel);
                    EditorGUILayout.EndVertical();
                }
                else
                {
                    EditorGUILayout.HelpBox(_errorLine, MessageType.Info);
                    if (GUILayout.Button("Retry", GUILayout.Height(25)))
                    {
                        _errorLine = "";
                        _done = false;
                        _playerPrefab = null;
                        _playerModels = null;
                    }
                }
                EditorGUILayout.BeginVertical("box");
                GUILayout.Label("Base Settings", EditorStyles.boldLabel);
                _playerPrefab = Resources.Load<GameObject>("CarPrefab").GetComponent<VehicleController>();
                var playerModels = Resources.LoadAll<GameObject>("Cars");
                _playerModels = new List<VehicleController>();
                foreach (var model in playerModels)
                {
                    _playerModels.Add(model.GetComponent<VehicleController>());
                }
                EditorGUILayout.EndVertical();

                bool enb = (_playerPrefab != null && _playerModels != null && /*RightHand != null &&*/ !_done);
                GUI.enabled = enb;
                if (GUILayout.Button("Replace", GUILayout.Height(40)))
                {
                    Replace();
                }
                GUI.enabled = true;
            }
            else
            {
                if (GUILayout.Button("Done!", GUILayout.Height(25)))
                {
                    Close();
                }
            }
        }

        private void Replace()
        {
            foreach (var model in _playerModels)
            {
                model.maxSpeedForward       = _playerPrefab.maxSpeedForward;
                model.maxSpeedReverse       = _playerPrefab.maxSpeedReverse;
                
                model.tireFriction          = _playerPrefab.tireFriction;
                model.antiRoll              = _playerPrefab.antiRoll;
                model.maxSteerAngle         = _playerPrefab.maxSteerAngle;
                model.aeroDrag              = _playerPrefab.aeroDrag;
                model.aeroDownforce         = _playerPrefab.aeroDownforce;
                model.rollingResistance     = _playerPrefab.rollingResistance;
                
                model.maxDriveForce         = _playerPrefab.maxDriveForce;
                model.maxDriveSlip          = _playerPrefab.maxDriveSlip;
                model.driveForceToMaxSlip   = _playerPrefab.driveForceToMaxSlip;
                model.forceCurveShape       = _playerPrefab.forceCurveShape;
                
                model.maxBrakeForce         = _playerPrefab.maxBrakeForce;
                model.brakeForceToMaxSlip   = _playerPrefab.brakeForceToMaxSlip;
                
                model.brakeMode             = _playerPrefab.brakeMode;
                model.maxBrakeRatio         = _playerPrefab.maxBrakeRatio;
                
                model.handbrakeMode         = _playerPrefab.brakeMode;
                model.maxHandbrakeRatio     = _playerPrefab.maxHandbrakeRatio;
                
                model.driveBalance          = _playerPrefab.driveBalance;
                model.brakeBalance          = _playerPrefab.brakeBalance;
                model.tireFriction          = _playerPrefab.tireFriction;
                model.aeroBalance           = _playerPrefab.aeroBalance;
                model.handlingBias          = _playerPrefab.handlingBias;

                model.tractionControl       = _playerPrefab.tractionControl;
                model.tractionControlRatio  = _playerPrefab.tractionControlRatio;

                model.brakeAssist           = _playerPrefab.brakeAssist;
                model.brakeAssistRatio      = _playerPrefab.brakeAssistRatio;

                model.steeringLimit         = _playerPrefab.steeringLimit;
                model.steeringLimitRatio    = _playerPrefab.steeringLimitRatio;

                model.steeringAssist        = _playerPrefab.steeringAssist;
                model.steeringAssistRatio   = _playerPrefab.steeringAssistRatio;
            }
            
            _done = true;
            maxSize = new Vector2(400, 50);
        }

        [MenuItem("Tools/Updade Car Physics")]
        public static void ShowWindow()
        {
            GetWindowWithRect(typeof(UpdateCarPhysics), new Rect(400, 300, 400, 200));
        }
    }
}