﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class BundleBuilder : Editor
    {
        [MenuItem("Assets/AssetBundles/Build Android Bundles")]
        public static void BuildAndroidAssetBundles()
        {
            BuildAllAssetBundles(BuildTarget.Android);
        }
        [MenuItem("Assets/AssetBundles/Build iOS Bundles")]
        public static void BuildIoSAssetBundles()
        {
            BuildAllAssetBundles(BuildTarget.iOS);
        }
        private static void BuildAllAssetBundles(BuildTarget buildTarget)
        {
            var assetBundleDirectory = $"AssetBundles/{buildTarget.ToString()}";
            if(!Directory.Exists(assetBundleDirectory))
            {
                Directory.CreateDirectory(assetBundleDirectory);
            }
            BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.ChunkBasedCompression,
                buildTarget);
        }
    }
}
