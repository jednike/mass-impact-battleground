﻿using System.Linq;
using EVP;
using UnityEditor;
using UnityEngine;

namespace CarsBattleRoyal
{
    public class AddNewCarWithLODS : EditorWindow {
        private GameObject _playerPrefab;
        private GameObject _playerModel;

        private bool _done;
        private string _errorLine;

        private void OnGUI()
        {
            if (!_done)
            {
                if (string.IsNullOrEmpty(_errorLine))
                {
                    EditorGUILayout.BeginVertical("box");
                    EditorGUILayout.LabelField("Add car", EditorStyles.boldLabel);
                    EditorGUILayout.EndVertical();
                }
                else
                {
                    EditorGUILayout.HelpBox(_errorLine, MessageType.Info);
                    if (GUILayout.Button("Retry", GUILayout.Height(25)))
                    {
                        _errorLine = "";
                        _done = false;
                        _playerPrefab = null;
                        _playerModel = null;
                    }
                }
                EditorGUILayout.BeginVertical("box");
                GUILayout.Label("Base Settings", EditorStyles.boldLabel);
                _playerPrefab = Resources.Load<GameObject>("CarPrefab");
                _playerModel = EditorGUILayout.ObjectField("New Car Model", _playerModel, typeof(GameObject), false) as GameObject;
                EditorGUILayout.EndVertical();

                bool enb = (_playerPrefab != null && _playerModel != null && /*RightHand != null &&*/ !_done);
                GUI.enabled = enb;
                if (GUILayout.Button("Replace", GUILayout.Height(40)))
                {
                    Replace();
                }
                GUI.enabled = true;
            }
            else
            {
                if (GUILayout.Button("Done!", GUILayout.Height(25)))
                {
                    Close();
                }
            }
        }

        private void Replace()
        {
            var tempPlayerPrefab = PrefabUtility.InstantiatePrefab(_playerPrefab.gameObject) as GameObject;
            var tempPlayerModel = PrefabUtility.InstantiatePrefab(_playerModel.gameObject) as GameObject;
            
            var body0 = tempPlayerModel.transform.Find("body_LOD0");
            var body1 = tempPlayerModel.transform.Find("body_LOD1");
            body1.SetParent(body0);

            var prefabBody = tempPlayerPrefab.transform.Find("Body");
            var bodyChilds = prefabBody.Cast<Transform>().ToList();
            foreach (var child in bodyChilds)
            {
                DestroyImmediate(child.gameObject);
            }
            body0.SetParent(prefabBody);
            
            var flWheel0 = tempPlayerModel.transform.Find("wheel_FL_LOD0");
            var flWheel1 = tempPlayerModel.transform.Find("wheel_FL_LOD1");
            flWheel1.SetParent(flWheel0);

            var frWheel0 = tempPlayerModel.transform.Find("wheel_FR_LOD0");
            var frWheel1 = tempPlayerModel.transform.Find("wheel_FR_LOD1");
            frWheel1.SetParent(frWheel0);

            var rlWheel0 = tempPlayerModel.transform.Find("wheel_RL_LOD0");
            var rlWheel1 = tempPlayerModel.transform.Find("wheel_RL_LOD1");
            rlWheel1.SetParent(rlWheel0);

            var rrWheel0 = tempPlayerModel.transform.Find("wheel_RR_LOD0");
            var rrWheel1 = tempPlayerModel.transform.Find("wheel_RR_LOD1");
            rrWheel1.SetParent(rrWheel0);

            var prefabWheels = tempPlayerPrefab.transform.Find("Wheels");
            var wheelsChilds = prefabWheels.Cast<Transform>().ToList();
            foreach (var child in wheelsChilds)
            {
                DestroyImmediate(child.gameObject);
            }
            
            flWheel0.transform.SetParent(prefabWheels);
            frWheel0.transform.SetParent(prefabWheels);
            rlWheel0.transform.SetParent(prefabWheels);
            rrWheel0.transform.SetParent(prefabWheels);

            var vehicleController = tempPlayerPrefab.GetComponent<VehicleController>();
            foreach (var wheel in vehicleController.wheels)
            {
                if (wheel.wheelCollider.name.Contains("FL"))
                    wheel.wheelTransform = flWheel0.transform;
                else if (wheel.wheelCollider.name.Contains("FR"))
                    wheel.wheelTransform = frWheel0.transform;
                else if (wheel.wheelCollider.name.Contains("RL"))
                    wheel.wheelTransform = rlWheel0.transform;
                else if (wheel.wheelCollider.name.Contains("RR"))
                    wheel.wheelTransform = rrWheel0.transform;

                wheel.wheelCollider.transform.position = wheel.wheelTransform.position;
            }

            tempPlayerPrefab.transform.Find("Colliders/Body Collider").GetComponent<MeshCollider>().sharedMesh =
                body0.GetComponent<MeshFilter>().sharedMesh;
            
            DestroyImmediate(tempPlayerModel);
            
            _done = true;
            maxSize = new Vector2(400, 50);
        }

        [MenuItem("Tools/Add New Car With LODS")]
        public static void ShowWindow()
        {
            GetWindowWithRect(typeof(AddNewCarWithLODS), new Rect(400, 300, 400, 200));
        }
    }
}